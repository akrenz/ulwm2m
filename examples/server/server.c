#include <ulwm2m.h>
#include "commandline.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <inttypes.h>
#include <poll.h>
#include <pthread.h>

// Forward Declarations
static int prv_read_id(char * buffer,uint16_t * idP);
static void prv_print_error(uint8_t status);
static void *lwm2m_task(void *arg);
static void *cmd_task(void *arg);

#define MAX_PACKET_SIZE 1024

static int g_quit = 0;

static char * prv_dump_binding(lwm2m_binding_t binding)
{
    switch (binding)
    {
    case BINDING_UNKNOWN:
        return "Not specified";
    case BINDING_U:
        return "UDP";
    case BINDING_UQ:
        return "UDP queue mode";
    case BINDING_S:
        return "SMS";
    case BINDING_SQ:
        return "SMS queue mode";
    case BINDING_US:
        return "UDP plus SMS";
    case BINDING_UQS:
        return "UDP queue mode plus SMS";
    default:
        return "";
    }
}

static void prv_dump_client(lwm2m_client_t * targetP)
{
    lwm2m_client_object_t * objectP;

    fprintf(stdout, "Client #%d:\r\n", targetP->internalID);
    fprintf(stdout, "\tname: \"%s\"\r\n", targetP->name);
    fprintf(stdout, "\tbinding: \"%s\"\r\n", prv_dump_binding(targetP->binding));
    if (targetP->msisdn) fprintf(stdout, "\tmsisdn: \"%s\"\r\n", targetP->msisdn);
    if (targetP->altPath) fprintf(stdout, "\talternative path: \"%s\"\r\n", targetP->altPath);
    fprintf(stdout, "\tlifetime: %d sec\r\n", targetP->lifetime);
    fprintf(stdout, "\tobjects: ");
    for (objectP = targetP->objectList; objectP != NULL ; objectP = objectP->next)
    {
        if (objectP->instanceList == NULL)
        {
            fprintf(stdout, "/%d, ", objectP->id);
        }
        else
        {
            lwm2m_list_t * instanceP;

            for (instanceP = objectP->instanceList; instanceP != NULL ; instanceP = instanceP->next)
            {
                fprintf(stdout, "/%d/%d, ", objectP->id, instanceP->id);
            }
        }
    }
    fprintf(stdout, "\r\n");
}

static void prv_result_callback(uint16_t clientID,
                                lwm2m_uri_t * uriP,
                                int status,
                                lwm2m_media_type_t format,
                                uint8_t * data,
                                int dataLength,
                                void * userData)
{
    fprintf(stdout, "\r\nClient #%d /%d", clientID, uriP->objectId);
    if (LWM2M_URI_IS_SET_INSTANCE(uriP))
        fprintf(stdout, "/%d", uriP->instanceId);
    else if (LWM2M_URI_IS_SET_RESOURCE(uriP))
        fprintf(stdout, "/");
    if (LWM2M_URI_IS_SET_RESOURCE(uriP))
            fprintf(stdout, "/%d", uriP->resourceId);
    fprintf(stdout, " : ");
    print_status(stdout, status);
    fprintf(stdout, "\r\n");

    if(dataLength != 0){
      lwm2m_data_t * res = NULL;
      int ret = lwm2m_data_parse(uriP, data, dataLength, format, &res);
      for(int i=0; i<ret; i++){
        char* data_type = userData;
        if(strcmp(data_type, "INTEGER") == 0){
            if(res[i].value.asBuffer.length == 1){
                printf("Resource %d: %" PRIu8 "\n", res[i].id, *res[i].value.asBuffer.buffer);
            }else if(res[i].value.asBuffer.length == 2){
                printf("Resource %d: %" PRIu16 "\n", res[i].id, ntohs(*((uint16_t*)res[i].value.asBuffer.buffer)));
            }else if(res[i].value.asBuffer.length == 4){
                printf("Resource %d: %" PRIu32 "\n", res[i].id, ntohl(*((uint32_t*)res[i].value.asBuffer.buffer)));
            }
            }else if (strcmp(data_type, "STRING") == 0){
                fprintf(stdout, "Resource %d: %.*s\n", res[i].id, (int)res[i].value.asBuffer.length, res[i].value.asBuffer.buffer);
            }else if (strcmp(data_type, "OPAQUE") == 0){
                fprintf(stdout, "Resource %d: \n", res[i].id);
                for(int j=0; j< res[i].value.asBuffer.length;j++){
                    fprintf(stdout, "0x%X ", res[i].value.asBuffer.buffer[j]);
                }
                fprintf(stdout, "\n");
            }else if (strcmp(data_type, "FLOAT") == 0){
                double val = 0;
                lwm2m_data_decode_float(&res[i], &val);
                printf("Resource %d: %f", res[i].id, val);
            }else{
            fprintf(stdout, "\"%s\" Unknown\n", data_type);
            }
        }
    }
    
    fprintf(stdout, "\r\n> ");
    fflush(stdout);
}

static void prv_monitor_callback(uint16_t clientID,
                                 lwm2m_uri_t * uriP,
                                 int status,
                                 lwm2m_media_type_t format,
                                 uint8_t * data,
                                 int dataLength,
                                 void * userData)
{
    lwm2m_context_t * lwm2mH = (lwm2m_context_t *) userData;
    lwm2m_client_t * targetP;

    switch (status)
    {
    case COAP_201_CREATED:
        fprintf(stdout, "\r\nNew client #%d registered.\r\n", clientID);

        targetP = (lwm2m_client_t *)lwm2m_list_find((lwm2m_list_t *)lwm2mH->clientList, clientID);

        prv_dump_client(targetP);
        break;

    case COAP_202_DELETED:
        fprintf(stdout, "\r\nClient #%d unregistered.\r\n", clientID);
        break;

    case COAP_204_CHANGED:
        fprintf(stdout, "\r\nClient #%d updated.\r\n", clientID);

        targetP = (lwm2m_client_t *)lwm2m_list_find((lwm2m_list_t *)lwm2mH->clientList, clientID);

        prv_dump_client(targetP);
        break;

    default:
        fprintf(stdout, "\r\nMonitor callback called with an unknown status: %d.\r\n", status);
        break;
    }

    fprintf(stdout, "\r\n> ");
    fflush(stdout);
}

static void prv_read_client(char * buffer,
                            void * user_data)
{
    lwm2m_context_t * lwm2mH = (lwm2m_context_t *) user_data;
    uint16_t clientId;
    lwm2m_uri_t uri;
    char* end = NULL;
    char* data_type = NULL;
    int result;

    result = prv_read_id(buffer, &clientId);
    if (result != 1) goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if (buffer[0] == 0) goto syntax_error;

    result = lwm2m_stringToUri(buffer, end - buffer, &uri);
    if (result == 0) goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if(buffer[0] == 0) goto syntax_error;

    data_type = strndup(buffer, end - buffer);

    if (!check_end_of_args(end)) goto syntax_error;
    int ret = 0;
    if ((ret = strcmp(data_type, "INTEGER")) != 0 &&
        (ret = strcmp(data_type, "STRING")) != 0 &&
        (ret = strcmp(data_type, "OPAQUE")) != 0 &&
        (ret = strcmp(data_type, "FLOAT")) != 0)
        goto syntax_error;
    result = lwm2m_dm_read(lwm2mH, clientId, &uri, prv_result_callback, false, data_type);

    if (result == 0)
    {
        fprintf(stdout, "OK");
    }
    else
    {
        prv_print_error(result);
    }
    return;

syntax_error:
    fprintf(stdout, "Syntax error !");
}

static void prv_read_file_client(char *buffer,
                            void *user_data)
{
    lwm2m_context_t *lwm2mH = (lwm2m_context_t *)user_data;
    uint16_t clientId;
    lwm2m_uri_t uri;
    char *end = NULL;
    int result;

    result = prv_read_id(buffer, &clientId);
    if (result != 1)
        goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if (buffer[0] == 0)
        goto syntax_error;

    result = lwm2m_stringToUri(buffer, end - buffer, &uri);
    if (result == 0)
        goto syntax_error;

    if (!check_end_of_args(end))
        goto syntax_error;

    result = lwm2m_dm_read(lwm2mH, clientId, &uri, prv_result_callback, NULL);

    if (result == 0)
    {
        fprintf(stdout, "OK");
    }
    else
    {
        prv_print_error(result);
    }
    return;

syntax_error:
    fprintf(stdout, "Syntax error !");
}

static void prv_write_client(char * buffer,
                             void * user_data)
{
    lwm2m_context_t * lwm2mH = (lwm2m_context_t *) user_data;
    uint16_t clientId;
    lwm2m_uri_t uri;
    char * end = NULL;
    int result;

    result = prv_read_id(buffer, &clientId);
    if (result != 1) goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if (buffer[0] == 0) goto syntax_error;

    result = lwm2m_stringToUri(buffer, end - buffer, &uri);
    if (result == 0) goto syntax_error;

    buffer = get_next_arg(end, &end);
    if (buffer[0] == 0) goto syntax_error;

    if (!check_end_of_args(end)) goto syntax_error;

    result = lwm2m_dm_write(lwm2mH, clientId, &uri, LWM2M_CONTENT_TEXT, (uint8_t *)buffer, end - buffer, prv_result_callback, NULL);

    if (result == 0)
    {
        fprintf(stdout, "OK");
    }
    else
    {
        prv_print_error(result);
    }
    return;

syntax_error:
    fprintf(stdout, "Syntax error !");
}

static size_t read_file(const char* path, char ** content)
{
    size_t newLen = 0;

    FILE *fp = fopen(path, "r");
    if (fp != NULL) {
        /* Go to the end of the file. */
        if (fseek(fp, 0L, SEEK_END) == 0) {
            /* Get the size of the file. */
            long bufsize = ftell(fp);
            if (bufsize == -1) { /* Error */ }

            /* Allocate our buffer to that size. */
            *content = malloc(sizeof(char) * (bufsize + 1));

            /* Go back to the start of the file. */
            if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

            /* Read the entire file into memory. */
            newLen = fread(*content, sizeof(char), bufsize, fp);
            if ( ferror( fp ) != 0 ) {
                fputs("Error reading file", stderr);
            }
        }
        fclose(fp);
    }

    return newLen;
}

static void prv_write_file_client(char * buffer, void * user_data)
{
    lwm2m_context_t * lwm2mH = (lwm2m_context_t *) user_data;
    uint16_t clientId;
    lwm2m_uri_t uri;
    char * end = NULL;
    int result;

    result = prv_read_id(buffer, &clientId);
    if (result != 1) goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if (buffer[0] == 0) goto syntax_error;

    result = lwm2m_stringToUri(buffer, end - buffer, &uri);
    if (result == 0) goto syntax_error;

    buffer = get_next_arg(end, &end);
    if (buffer[0] == 0) goto syntax_error;

    if (!check_end_of_args(end)) goto syntax_error;

    /* read content of file */
    *end = 0;
    char * file_content = NULL;
    size_t file_size = read_file(buffer, &file_content);

    result = lwm2m_dm_write(lwm2mH, clientId, &uri, LWM2M_CONTENT_TEXT, (uint8_t *)file_content, file_size, prv_result_callback, NULL);

    if (result == 0)
    {
        fprintf(stdout, "OK");
    }
    else
    {
        prv_print_error(result);
    }
    return;

syntax_error:
    fprintf(stdout, "Syntax error !");
}

static void prv_exec_client(char * buffer,
                            void * user_data)
{
    lwm2m_context_t * lwm2mH = (lwm2m_context_t *) user_data;
    uint16_t clientId;
    lwm2m_uri_t uri;
    char* end = NULL;
    int result;

    result = prv_read_id(buffer, &clientId);
    if (result != 1) goto syntax_error;

    buffer = get_next_arg(buffer, &end);
    if (buffer[0] == 0) goto syntax_error;

    result = lwm2m_stringToUri(buffer, end - buffer, &uri);
    if (result == 0) goto syntax_error;

    if (!check_end_of_args(end)) goto syntax_error;
    result = lwm2m_dm_execute(lwm2mH, clientId, &uri, LWM2M_CONTENT_TEXT, NULL, 0, prv_result_callback, NULL);

    if (result == 0)
    {
        fprintf(stdout, "OK");
    }
    else
    {
        prv_print_error(result);
    }
    return;

syntax_error:
    fprintf(stdout, "Syntax error !");
}

static void prv_exit(char * buffer, void * user_data)
{
  g_quit = 1;
}

static int prv_read_id(char * buffer, uint16_t * idP)
{
    int nb;
    int value;

    nb = sscanf(buffer, "%d", &value);
    if (nb == 1)
    {
        if (value < 0 || value > LWM2M_MAX_ID)
        {
            nb = 0;
        }
        else
        {
            *idP = value;
        }
    }

    return nb;
}

static void prv_print_error(uint8_t status)
{
    fprintf(stdout, "Error: ");
    print_status(stdout, status);
    fprintf(stdout, "\r\n");
}

void handle_sigint(int signum)
{
    g_quit = 1;
}

int main(int argc, char *argv[])
{
    lwm2m_context_t * lwm2mH = lwm2m_create_context(NULL, 0, NULL, "5683", AF_INET);
    if(lwm2mH == NULL){
      fprintf(stderr, "Could not create handle\n");
      return -1;
    }


    signal(SIGINT, handle_sigint);
    lwm2m_set_monitoring_callback(lwm2mH, prv_monitor_callback, lwm2mH);

    pthread_t thread[2];
    pthread_create(thread, NULL, lwm2m_task, lwm2mH);
    pthread_create(thread+1, NULL, cmd_task, lwm2mH);

    // wait for all threads to terminate
    for(int i=0;i<2;i++){
      pthread_join(thread[i], NULL);
    }

    lwm2m_close_context(lwm2mH);

    return 0;
}

static void *lwm2m_task(void *arg){
  lwm2m_context_t * ctx = (lwm2m_context_t*)arg;
  fprintf(stdout, "Start processing loop\n");
  while (0 == g_quit){
    lwm2m_process(ctx);
  }

  return NULL;
}

static void *cmd_task(void *arg){
  lwm2m_context_t * ctx = (lwm2m_context_t*)arg;
  command_desc_t commands[] =
      {
          {"read", "Read from a client.", " read CLIENT# URI DATA_TYPE\r\n"
                                          "   CLIENT#: client number as returned by command 'list'\r\n"
                                          "   URI: uri to read such as /3, /3/0/2, /1024/11, /1024/0/1\r\n"
                                          "   DATA: type of data for display. Available: STRING, INTEGER, FLOAT, OPAQUE\r\n"
                                          "Result will be displayed asynchronously.",
           prv_read_client, ctx},
          {"read_file", "Read large data from a client and store it into a file.", " read_file CLIENT# URI DATA_TYPE\r\n"
                                               "   CLIENT#: client number as returned by command 'list'\r\n"
                                               "   URI: uri to read such as /3, /3/0/2, /1024/11, /1024/0/1\r\n"
                                               "   PATH: path to file\r\n"
                                               "Result will be displayed asynchronously.",
           prv_read_file_client, ctx},
          {"write", "Write to a client.", " write CLIENT# URI DATA\r\n"
                                          "   CLIENT#: client number as returned by command 'list'\r\n"
                                          "   URI: uri to write to such as /3, /3/0/2, /1024/11, /1024/0/1\r\n"
                                          "   DATA: data to write\r\n"
                                          "Result will be displayed asynchronously.",
           prv_write_client, ctx},
          {"write_file", "Write content of a file to a client.", " write_file CLIENT# URI PATH\r\n"
                                                                 "   CLIENT#: client number as returned by command 'list'\r\n"
                                                                 "   URI: uri to write to such as /3, /3/0/2, /1024/11, /1024/0/1\r\n"
                                                                 "   PATH: path to file",
           prv_write_file_client, ctx},
          {"exec", "Execute a node on a client", " exec CLIENT# URI\r\n"
                                                 "   CLIENT#: client number as returned by command 'list'\r\n"
                                                 "   URI: uri to exec such as /3, /3/0/2, /1024/11, /1024/0/1",
           prv_exec_client, ctx},
          {"exit", "Quit lwm2mserver", "Quit lwm2mserver", prv_exit, NULL},
          COMMAND_END_LIST};

  struct pollfd pollfd = {
    .fd = STDIN_FILENO,
    .events = POLLIN
  };
  
  while (0 == g_quit){
    int ret = poll(&pollfd, 1, 10);

    if(ret > 0 && pollfd.revents & POLLIN){
      uint8_t buffer[MAX_PACKET_SIZE] = {};
      ssize_t numBytes = read(STDIN_FILENO, buffer, MAX_PACKET_SIZE - 1);

      if (numBytes > 1)
      {
          buffer[numBytes] = 0;
          handle_command(commands, (char*)buffer);
          fprintf(stdout, "\r\n> ");
          fflush(stdout);
      }
    }
  }

  return NULL;
}

cmake_minimum_required (VERSION 3.0)

project (lightclient C)

set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-Wall -Werror")
set(CMAKE_C_FLAGS ${CMAKE_C_FLAGS} "-Wall -Werror")

set(ULWM2M_CLIENT_LIB ON CACHE BOOL "Build wakaama Client library" FORCE)
set(ULWM2M_SERVER_LIB OFF CACHE BOOL "Do not build wakaama Server library" FORCE)
set(ULWM2M_PLATFORM "linux" CACHE STRING "Set Platform Source directory to linux" FORCE)
set(BUILD_TESTING OFF CACHE BOOL "Disable unit tests for example" FORCE)
set(ULWM2M_FLOATING_POINT ON CACHE BOOL "Enable Floating Point support for ulwm2m" FORCE)

add_subdirectory(../../ wakaama)

add_executable(${PROJECT_NAME}  lightclient.c object_test.c)

target_link_libraries(${PROJECT_NAME} ulwm2mclient)

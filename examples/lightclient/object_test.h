/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
 
#ifndef OBJECT_TEST_H_
#define OBJECT_TEST_H_

#include <ulwm2m.h>

typedef struct{
  uint8_t  test;
  double   dec;
  int16_t  sig;
} test_instance_data_t;

uint8_t prv_test_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t prv_test_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);
uint8_t prv_test_exec(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP);
uint8_t prv_test_create(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);
uint8_t prv_test_delete(uint16_t id, lwm2m_object_t * objectP);
uint8_t prv_test_discover(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);

#endif /* OBJECT_TEST_H_ */
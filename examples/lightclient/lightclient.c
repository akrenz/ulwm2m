/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "object_test.h"

#include <ulwm2m.h>

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <signal.h>

#define LOG(x) fprintf(stdout, "[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ERROR(x) fprintf(stderr, "[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ARG(FMT, ...) fprintf(stdout, "[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)
#define LOG_ARG_ERROR(FMT, ...) fprintf(stderr, "[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)

volatile sig_atomic_t g_quit = 0;

void handle_sigint(int signum){
  printf("Signal handle. g_quit: %d\n", g_quit);
  g_quit = 1;
}

void print_usage(){
  printf("Usage: ulwm2mclient [OPTION]\r\n");
  printf("Launch a LWM2M client.\r\n");
  printf("Options:\r\n");
  printf("  -6\t\tUse IPv6 connection. Default: IPv4 connection\r\n");
  printf("\r\n");
}

#define OBJ_COUNT 5

uint32_t write_firmware_callback(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more)
{
  printf("Block Num: %d\n", block_num);
  printf("Block More: %d\n", block_more);
  printf("%.*s", length, buffer);
  fflush(stdout);

  return COAP_NO_ERROR;
}

uint32_t execute_update_callback(void)
{
  printf("Execute Update\n");

  return COAP_NO_ERROR;
}

int main(int argc, char *argv[])
{
  char * name = "testlwm2mclient";
  int ret = 0;
  lwm2m_object_t objArray[OBJ_COUNT];

  int addressFamily = AF_INET;
  int opt = 1;
  while (opt < argc)
  {
    if (argv[opt] == NULL
        || argv[opt][0] != '-'
        || argv[opt][2] != 0){
        print_usage();
        return 0;
    }
    switch (argv[opt][1]){
      case '6':
        addressFamily = AF_INET6;
        break;
      default:
        print_usage();
        return 0;
    }
    opt += 1;
  }

 LOG("Options parsed");

  /* create mandatory security object */
  ret = lwm2m_init_object(&objArray[0], LWM2M_SECURITY_OBJECT_ID, lwm2m_security_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create security object\r\n");
    return -1;
  }
  lwm2m_security_instance_data_t secInstData = {.uri="coap://localhost:5683",
                                                .isBootstrap = false,
                                                .securityMode = SECURITY_MODE_NO_SECURITY,
                                                .keyOrIdentity = NULL,
                                                .serverPublicKey = NULL,
                                                .secretKey = NULL,
                                                .shortID=123,
                                                .clientHoldOffTime=10,
                                                .BootstrapServerAccountTimeout=0};
  lwm2m_instance_t * sec_inst = lwm2m_create_instance(0, &secInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[0], sec_inst);

  /* create static server object */
  ret = lwm2m_init_object(&objArray[1], LWM2M_SERVER_OBJECT_ID, lwm2m_server_read, lwm2m_server_write, lwm2m_server_execute, lwm2m_server_create, lwm2m_server_delete, lwm2m_server_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create server object\r\n");
    return -1;
  }
  lwm2m_server_instance_data_t serverInstData = { .shortServerId = 123,
                                                  .lifetime = 300,
                                                  .defaultMinObservationPeriod = 10,
                                                  .defaultMaxObservationPeriod = 60,
                                                  .storing = false,
                                                  .binding = BINDING_U
  };
  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, &serverInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[1], server_inst);

  /* create mandatory device object */
  ret = lwm2m_init_object(&objArray[2], LWM2M_DEVICE_OBJECT_ID, lwm2m_device_read, NULL, lwm2m_device_execute, NULL, NULL, lwm2m_device_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Device object\r\n");
    return -1;
  }
  lwm2m_device_instance_data_t deviceInstData = {  .manufacturer = "Albert Krenz",
                                                .firmware = "Number 1.2.3",
                                                .binding = BINDING_U
  };
  lwm2m_instance_t * device_inst = lwm2m_create_instance(0, &deviceInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[2], device_inst);

  /* create object */
  ret = lwm2m_init_object(&objArray[3], 31024, prv_test_read, prv_test_write, prv_test_exec, prv_test_create, prv_test_delete, prv_test_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Test object\r\n");
    return -1;
  }
  /* first instance of object */
  test_instance_data_t b = {.test=10, .dec=10.0, .sig=-10};
  lwm2m_instance_t * test_inst = lwm2m_create_instance(1, &b, NULL, false);
  lwm2m_object_add_instance(&objArray[3], test_inst);
  /* second instance of object */
  test_instance_data_t c = {.test=20, .dec=20.0, .sig=-20};
  test_inst = lwm2m_create_instance(2,&c, NULL, false);
  lwm2m_object_add_instance(&objArray[3], test_inst);

  /* create firmware Update Object */
  ret = lwm2m_init_object(&objArray[4], LWM2M_FIRMWARE_UPDATE_OBJECT_ID, lwm2m_firmware_update_read, NULL, lwm2m_firmware_update_execute, NULL, NULL, NULL, lwm2m_firmware_update_write, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Device object\r\n");
    return -1;
  }
  lwm2m_firmware_update_instance_data_t firmware_update_data = {.write_firmware = write_firmware_callback, .execute_update = execute_update_callback};
  lwm2m_instance_t * fwup_inst = lwm2m_create_instance(0, &firmware_update_data, NULL, false);
  lwm2m_object_add_instance(&objArray[4], fwup_inst);


  lwm2m_context_t * lwm2mH = lwm2m_create_context(objArray, OBJ_COUNT, name, "56830", addressFamily, 0);
  if(lwm2mH == NULL){
    LOG_ERROR("Could not create lwm2m context");
    return -1;
  }

  signal(SIGINT, handle_sigint);
  fprintf(stdout, "LWM2M Client \"%s\" started.\r\nUse Ctrl-C to exit.\r\n\n", name);

  while (0 == g_quit){
    // lwm2m_print_state(lwm2mH);

    lwm2m_process(lwm2mH);
  }

  lwm2m_close_context(lwm2mH);


  lwm2m_clear_object(&objArray[0]);
  lwm2m_clear_object(&objArray[1]);
  lwm2m_clear_object(&objArray[2]);
  lwm2m_clear_object(&objArray[3]);

  return 0;
}


#include <stdio.h>

#include <ulwm2m.h>
#include <sys/socket.h>

#define OBJ_COUNT 3

#define LOG(x) fprintf(stdout, "[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ERROR(x) fprintf(stderr, "[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ARG(FMT, ...) fprintf(stdout, "[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)
#define LOG_ARG_ERROR(FMT, ...) fprintf(stderr, "[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)

void app_main(void){
  printf("ulwm2m client example\n");

  char * name = "testlwm2mclient";
  int ret = 0;
  lwm2m_object_t objArray[OBJ_COUNT];

  /* create mandatory security object */
  ret = lwm2m_init_object(&objArray[0], LWM2M_SECURITY_OBJECT_ID, lwm2m_security_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create security object\r\n");
    return;
  }
  lwm2m_security_instance_data_t secInstData = {.uri="coap://localhost:5683",
                                                .isBootstrap = false,
                                                .securityMode = SECURITY_MODE_NO_SECURITY,
                                                .keyOrIdentity = NULL,
                                                .serverPublicKey = NULL,
                                                .secretKey = NULL,
                                                .shortID=123,
                                                .clientHoldOffTime=10,
                                                .BootstrapServerAccountTimeout=0};
  lwm2m_object_t * sec_inst = lwm2m_create_instance(0, &secInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[0], sec_inst);

  /* create static server object */
  ret = lwm2m_init_object(&objArray[1], LWM2M_SERVER_OBJECT_ID, lwm2m_server_read, lwm2m_server_write, lwm2m_server_execute, lwm2m_server_create, lwm2m_server_delete, lwm2m_server_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create server object\r\n");
    return;
  }
  lwm2m_server_instance_data_t serverInstData = { .shortServerId = 123,
                                                  .lifetime = 300,
                                                  .defaultMinObservationPeriod = 10,
                                                  .defaultMaxObservationPeriod = 60,
                                                  .storing = false,
                                                  .binding = BINDING_U
  };
  lwm2m_object_t * server_inst = lwm2m_create_instance(0, &serverInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[1], server_inst);

  /* create mandatory device object */
  ret = lwm2m_init_object(&objArray[2], LWM2M_DEVICE_OBJECT_ID, lwm2m_device_read, NULL, lwm2m_device_execute, NULL, NULL, lwm2m_device_discover, NULL, NULL, NULL);
  if (ret < 0){
    LOG_ERROR("Failed to create Device object\r\n");
    return;
  }

  lwm2m_device_instance_data_t deviceInstData = {  .manufacturer = "Albert Krenz",
                                                .firmware = "Number 1.2.3",
                                                .binding = BINDING_U
  };
  lwm2m_object_t * device_inst = lwm2m_create_instance(0, &deviceInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[2], device_inst);

  lwm2m_context_t * lwm2mH = lwm2m_create_context(objArray, OBJ_COUNT, name, "56830", AF_INET, 0);

  while (1){
    lwm2m_process(lwm2mH);
  }

  lwm2m_close_context(lwm2mH);


  lwm2m_clear_object(&objArray[0]);
  lwm2m_clear_object(&objArray[1]);
  lwm2m_clear_object(&objArray[2]);
}
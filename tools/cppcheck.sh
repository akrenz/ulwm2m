#! /usr/bin/env bash
# Run either 'cppcheck.sh check' to use normal analysis mode of cppcheck or 'cppcheck.sh bug-hunting'
# to use bug hunting mode to find more errors at the risk of more false positives

if [ -z "$1" ]; then
    echo "Error: No argument supplied. Use \"check\" or \"bug-hunting\""
    exit -1
fi

if [ ! -f build/compile_commands.json ];then
  echo "Error: compile_commands.json is missing. Rerun cmake with -DCMAKE_EXPORT_COMPILE_COMMANDS=ON."
  exit -1
fi

if [ $1 == "check" ];then
  cppcheck --enable=warning,performance,portability,missingInclude --std=c99 --platform=unix64 -j3 --project=build/compile_commands.json -i $(pwd)/test -i wakaama/examples -i tests -v --xml . 2>cppcheck.xml
  echo "=== Summary ==="
  ERRORS=$(xml_grep --count  "//results/errors/error[@severity='error']"  cppcheck.xml | awk '{if(/total:/) print $2}')
  echo "Errors: $ERRORS"
  WARNINGS=$(xml_grep --count  "//results/errors/error[@severity='warning']"  cppcheck.xml | awk '{if(/total:/) print $2}')
  echo "Warning: $WARNINGS"
  PERFORMANCES=$(xml_grep --count  "//results/errors/error[@severity='performance']"  cppcheck.xml | awk '{if(/total:/) print $2}')
  echo "Performance: $PERFORMANCES"
  PORTABILITIES=$(xml_grep --count  "//results/errors/error[@severity='portability']"  cppcheck.xml | awk '{if(/total:/) print $2}')
  echo "Portabilities: $PORTABILITIES"
  MISSING_INCLUDES=$(xml_grep --count  "//results/errors/error[@severity='missingInclude']"  cppcheck.xml | awk '{if(/total:/) print $2}')
  echo "Missing Includes: $MISSING_INCLUDES"
fi
if [ $1 == "bug-hunting" ];then
  cppcheck --bug-hunting -j3 --project=build/compile_commands.json -i $(pwd)/test -i wakaama/examples  -i tests --xml . 2>cppcheck_bug_hunting.xml
fi
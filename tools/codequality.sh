#! /bin/env bash
docker run --rm -it \
  --env CODECLIMATE_CODE=$(pwd) \
  --env CONTAINER_TIMEOUT_SECONDS=9000 \
  --volume $(pwd):/code \
  --volume /tmp/cc:/tmp/cc \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --env CODECLIMATE_DEBUG=1 "codeclimate/codeclimate" analyze
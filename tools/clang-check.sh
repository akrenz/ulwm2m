#!/bin/bash


###### configure & build ######
mkdir -p build
pushd build
cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && \
ninja
popd

###### clang-check ######
if [ -d "build/clangcheck" ]; then
  rm -r build/clangcheck
fi

mkdir -p build/clangcheck
pushd build/clangcheck

# collect list of files
find ../../client/    -type f -follow -print | grep "[.]cc\|[.]cpp\|[.]cxx\|[.]c++\|[.]CPP\|[.]c\|[.]C$" >  src_files.txt
find ../../examples/  -type f -follow -print | grep "[.]cc\|[.]cpp\|[.]cxx\|[.]c++\|[.]CPP\|[.]c\|[.]C$" >>  src_files.txt
find ../../platform/  -type f -follow -print | grep "[.]cc\|[.]cpp\|[.]cxx\|[.]c++\|[.]CPP\|[.]c\|[.]C$" >>  src_files.txt
find ../../server/    -type f -follow -print | grep "[.]cc\|[.]cpp\|[.]cxx\|[.]c++\|[.]CPP\|[.]c\|[.]C$" >> src_files.txt

# run clang-check on collected file names
clang-check -analyze -fix-what-you-can -p ../build/ $(cat src_files.txt) > clangcheck_all.log 2>&1
rm -f src_files.txt

# check if clang-check returned suggestions
notcorrectlist=`git status | grep "modified:" | sed 's/^.*modified: //'`
logcount=$(wc -l < clangcheck_all.log )

# oh no, something has changed!
echo "Error count is $logcount! clang-check run failed :-(.";
cat clangcheck_all.log

echo "Suggested patches:";
for f in $notcorrectlist; do
       echo $f
       git diff $f > clangcheck_$(basename $f).diff
done

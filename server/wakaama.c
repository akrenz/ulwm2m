/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
 
#include "internal.h"

#include <stdbool.h>
#include <netdb.h>
#include <sys/types.h>

#define MAX_PACKET_SIZE 1024

lwm2m_context_t * lwm2m_create_context(lwm2m_object_t objects[], size_t numObj, const char * name, const char* port, const int addressFamily){

  // if(objects == NULL || numObj == 0 || name == NULL){
  //   return NULL;
  // }

  client_data_t * data = lwm2m_malloc(sizeof(client_data_t));
  lwm2m_context_t * context = NULL;

  if(lwm2m_connect_socket(data, port, addressFamily) < 0){
    lwm2m_free(data);
    return NULL;
  }
  if(objects != NULL) data->securityObjP = &objects[0];

  context = lwm2m_init(data);
  if(context == NULL){
    lwm2m_close_socket(data);
    lwm2m_free(data);
    return NULL;
  }

#ifdef LWM2M_CLIENT_MODE
  if (lwm2m_configure(context, name, NULL, NULL, numObj, objects) != 0){
    lwm2m_close_socket(data);
    lwm2m_close(context);
    lwm2m_free(data);
    return NULL;
  }
#endif

  return context;
}

int lwm2m_close_context(lwm2m_context_t* context){
  if(context == NULL){
    return -1;
  }
  lwm2m_close_socket(context->userData);
  lwm2m_free(context->userData);
  lwm2m_close(context);

  return 0;
}

bool lwm2m_session_is_equal(void * session1, void * session2, void * userData){
return (session1 == session2);
}

// void * lwm2m_connect_server(uint16_t secObjInstID, void * userData){
//   client_data_t * data = (client_data_t*) userData;
  
//   const char * uri = get_security_uri(data->securityObjP, secObjInstID);
//   if(uri != NULL){
//     const char * hostname = getHostnameFromUri(uri);
//     const char * port = getPortFromUri(uri);
//     LOG_ARG("Connect to %s:%s", hostname, port);
//     struct addrinfo * server = hostname_to_ip(hostname, port);
//     lwm2m_free((void*)hostname);
//     lwm2m_free((void*)port);
//     if(server != NULL){
      
//       signed ret = connect(data->sock, server->ai_addr, server->ai_addrlen);
//       if(ret <0){
//         LOG_ERROR("Could not connect to server\n");
//         return NULL;  
//       }
      
//       if(server->ai_family == AF_INET){
//         data->sessionHandle = lwm2m_malloc(sizeof(struct sockaddr_in));
//       }else{
//         data->sessionHandle = lwm2m_malloc(sizeof(struct sockaddr_in6));
//       }
//       memcpy(data->sessionHandle, server->ai_addr, server->ai_addrlen);
      
//       freeaddrinfo(server);
//       lwm2m_free((void*)uri);
//       return data->sessionHandle;
//     }
//   }
//   return NULL;
// }

uint8_t lwm2m_buffer_send(void * sessionH, uint8_t * buffer, size_t length, void * userdata){
  LOG("Entering");
  client_data_t * data = (client_data_t*)userdata;

  if (lwm2m_socket_send(data, buffer, length, sessionH) < 0){
    LOG_ERROR("Could not send data");
    return COAP_503_SERVICE_UNAVAILABLE;
  }

  return COAP_NO_ERROR;
}

void lwm2m_close_connection(void * sessionH, void * userData){
  client_data_t * data = (client_data_t*) userData;
  data->sessionHandle = NULL;
  lwm2m_free(sessionH);
}

void lwm2m_print_state(lwm2m_context_t * lwm2mH){
#ifdef LWM2M_CLIENT_MODE
    lwm2m_server_t * targetP;

    LOG("State: ");
    switch(lwm2mH->state)
    {
    case STATE_INITIAL:
        LOG("STATE_INITIAL");
        break;
    case STATE_BOOTSTRAP_REQUIRED:
        LOG("STATE_BOOTSTRAP_REQUIRED");
        break;
    case STATE_BOOTSTRAPPING:
        LOG("STATE_BOOTSTRAPPING");
        break;
    case STATE_REGISTER_REQUIRED:
        LOG("STATE_REGISTER_REQUIRED");
        break;
    case STATE_REGISTERING:
        LOG("STATE_REGISTERING");
        break;
    case STATE_READY:
        LOG("STATE_READY");
        break;
    default:
        LOG("Unknown !");
        break;
    }
    LOG("\r\n");

    targetP = lwm2mH->bootstrapServerList;

    if (lwm2mH->bootstrapServerList == NULL)
    {
        LOG("No Bootstrap Server.\r\n");
    }
    else
    {
        LOG("Bootstrap Servers:\r\n");
        for (targetP = lwm2mH->bootstrapServerList ; targetP != NULL ; targetP = targetP->next)
        {
            LOG_ARG(" - Security Object ID %d", targetP->secObjInstID);
            LOG_ARG("\tHold Off Time: %lu s", (unsigned long)targetP->lifetime);
            LOG("\tstatus: ");
            switch(targetP->status)
            {
            case STATE_DEREGISTERED:
                LOG("DEREGISTERED\r\n");
                break;
            case STATE_BS_HOLD_OFF:
                LOG("CLIENT HOLD OFF\r\n");
                break;
            case STATE_BS_INITIATED:
                LOG("BOOTSTRAP INITIATED\r\n");
                break;
            case STATE_BS_PENDING:
                LOG("BOOTSTRAP PENDING\r\n");
                break;
            case STATE_BS_FINISHED:
                LOG("BOOTSTRAP FINISHED\r\n");
                break;
            case STATE_BS_FAILED:
                LOG("BOOTSTRAP FAILED\r\n");
                break;
            default:
                LOG_ARG("INVALID (%d)\r\n", (int)targetP->status);
            }
            LOG("\r\n");
        }
    }

    if (lwm2mH->serverList == NULL)
    {
        LOG("No LWM2M Server.\r\n");
    }
    else
    {
        LOG("LWM2M Servers:\r\n");
        for (targetP = lwm2mH->serverList ; targetP != NULL ; targetP = targetP->next)
        {
            LOG_ARG(" - Server ID %d", targetP->shortID);
            LOG("\tstatus: ");
            switch(targetP->status)
            {
            case STATE_DEREGISTERED:
                LOG("DEREGISTERED\r\n");
                break;
            case STATE_REG_PENDING:
                LOG("REGISTRATION PENDING\r\n");
                break;
            case STATE_REGISTERED:
                LOG_ARG("REGISTERED\tlocation: \"%s\"\tLifetime: %lus\r\n", targetP->location, (unsigned long)targetP->lifetime);
                break;
            case STATE_REG_UPDATE_PENDING:
                LOG("REGISTRATION UPDATE PENDING\r\n");
                break;
            case STATE_REG_UPDATE_NEEDED:
                LOG("REGISTRATION UPDATE REQUIRED\r\n");
                break;
            case STATE_DEREG_PENDING:
                LOG("DEREGISTRATION PENDING\r\n");
                break;
            case STATE_REG_FAILED:
                LOG("REGISTRATION FAILED\r\n");
                break;
            default:
                LOG_ARG("INVALID (%d)\r\n", (int)targetP->status);
            }
            LOG("\r\n");
        }
    }
#endif
}

void lwm2m_process(lwm2m_context_t* context){
  struct timeval tv;

  tv.tv_sec = 60;
  tv.tv_usec = 0;

  int result = lwm2m_step(context, &(tv.tv_sec));
  if (result != 0){
    LOG_ARG_ERROR("lwm2m_step() failed: 0x%X\r\n", result);
    return;
  }

  uint8_t buffer[MAX_PACKET_SIZE];
  client_data_t * data = context->userData;

  #ifdef LWM2M_CLIENT_MODE
  ssize_t numBytes = lwm2m_socket_recv(data->sock, buffer, MAX_PACKET_SIZE, data->sessionHandle);
  if (0 < numBytes){
    lwm2m_handle_packet(context, buffer, numBytes, data->sessionHandle);
  }
  #else
  struct sockaddr_storage addr;
  ssize_t numBytes = lwm2m_socket_recv(data->sock, buffer, MAX_PACKET_SIZE, (struct sockaddr*)&addr);
  if (0 < numBytes){
    lwm2m_handle_packet(context, buffer, numBytes, &addr);
  }
  #endif

}

int lwm2m_free_client(lwm2m_client_t * client){
  lwm2m_client_object_t * obj = client->objectList;
  while(obj != NULL){
    lwm2m_client_object_t * objInst = obj;
    obj = obj->next;
    lwm2m_free_client_object(objInst);
  }
  if (client->name != NULL) lwm2m_free(client->name);
  if (client->msisdn != NULL) lwm2m_free(client->msisdn);
  if (client->altPath != NULL) lwm2m_free(client->altPath);
  lwm2m_free(client);

  return 0;
}

int lwm2m_free_client_object(lwm2m_client_object_t * obj){
  if(obj == NULL){
    return -1;
  }

  lwm2m_list_t * inst = obj->instanceList;
  while(inst != NULL){
    lwm2m_list_t * tmp = inst;
    inst = inst->next;
    lwm2m_free(tmp);
  }
  lwm2m_free(obj);

  return 0;
}
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <internal.h>

#include <string.h>
#include <stdio.h>
#include <zephyr/kernel.h>
#include <zephyr/net/socket.h>
#include <version.h>
#if KERNEL_VERSION_NUMBER >= 0x30500
#include <zephyr/random/random.h>
#else
#include <zephyr/random/rand32.h>
#endif
#include <zephyr/posix/arpa/inet.h>


#define SIZE_XS 8
#define SIZE_S 16
#define SIZE_M 32
#define SIZE_L 128
#define SIZE_XL 256
#define NUM_XS ULWM2M_HEAP_NUM_XS
#define NUM_S ULWM2M_HEAP_NUM_S
#define NUM_M ULWM2M_HEAP_NUM_M
#define NUM_L ULWM2M_HEAP_NUM_L
#define NUM_XL ULWM2M_HEAP_NUM_XL
#define GUARD_XS 0x1111
#define GUARD_S  0x2222
#define GUARD_M  0x3333
#define GUARD_L  0x4444
#define GUARD_XL 0x5555

struct heap_element_xs{
  uint16_t guard;
  uint8_t buf[SIZE_XS] __attribute__((aligned));
};

struct heap_element_s{
  uint16_t guard;
  uint8_t buf[SIZE_S] __attribute__((aligned));
};

struct heap_element_m{
  uint16_t guard;
  uint8_t buf[SIZE_M] __attribute__((aligned));
};

struct heap_element_l{
  uint16_t guard;
  uint8_t buf[SIZE_L] __attribute__((aligned));
};

struct heap_element_xl{
  uint16_t guard;
  uint8_t buf[SIZE_XL] __attribute__((aligned));
};

struct heap_s{
  struct heap_element_xs xs[NUM_XS];
  struct heap_element_s s[NUM_S];
  struct heap_element_m m[NUM_M];
  struct heap_element_l l[NUM_L];
  struct heap_element_xl xl[NUM_XL];
};

static struct heap_s heap = {};

// K_HEAP_DEFINE(ulwm2m_heap, 4096);

void * lwm2m_malloc(size_t s){
  void* p= NULL;
  if(s <= SIZE_XS){
    for(size_t i=0;i<NUM_XS;i++){
      if(heap.xs[i].guard == 0x00){
        heap.xs[i].guard = GUARD_XS;
        p = heap.xs[i].buf;
        break;
      }
    }
  }else if(s <= SIZE_S){
    for(size_t i=0;i<NUM_S;i++){
      if(heap.s[i].guard == 0x00){
        heap.s[i].guard = GUARD_S;
        p = heap.s[i].buf;
        break;
      }
    }
  }else if(s<= SIZE_M){
    for(size_t i=0;i<NUM_M;i++){
      if(heap.m[i].guard == 0x00){
        heap.m[i].guard = GUARD_M;
        p = heap.m[i].buf;
        break;
      }
    }
  }else if(s<= SIZE_L){
    for(size_t i=0;i<NUM_L;i++){
      if(heap.l[i].guard == 0x00){
        heap.l[i].guard = GUARD_L;
        p = heap.l[i].buf;
        break;
      }
    }
  }else if(s<= SIZE_XL){
    for(size_t i=0;i<NUM_XL;i++){
      if(heap.xl[i].guard == 0x00){
        heap.xl[i].guard = GUARD_XL;
        p = heap.xl[i].buf;
        break;
      }
    }
  }
  return p;
}

void * lwm2m_calloc(const size_t n, const size_t s)
{
  return lwm2m_malloc(n * s);
}

void lwm2m_free(void * p){
  if(p == NULL) return;
  uint16_t* guard = (((uint16_t*)p)-sizeof(uint16_t)*2);
  *guard = 0x00;
}

char * lwm2m_strdup(const char * str){
  size_t len = strlen(str)+1;
  char *dest = lwm2m_malloc(len);
	if (dest){
		memcpy(dest, str, len);
  }

	return dest;
}

int lwm2m_strncmp(const char * s1, const char * s2, size_t n){
  return strncmp(s1, s2, n);
}

time_t lwm2m_gettime(void){
  return k_uptime_get()/1000;
}

void lwm2m_printf(const char * format, ...){
  va_list ap;

  va_start(ap, format);

  vfprintf(stderr, format, ap);
  
  va_end(ap);
}

void lwm2m_close_socket(client_data_t * data){
  close(data->sock);
}

int lwm2m_connect_socket(client_data_t * data, const char * port, const int addressFamily){
  data->sock = -1;
  data->sock = zsock_socket(addressFamily, SOCK_DGRAM, IPPROTO_UDP);
  if (data->sock >= 0)
  {
      if(addressFamily == AF_INET){
        struct sockaddr_in addr = {.sin_addr = INADDR_ANY_INIT, .sin_port = htons(strtol(port, NULL, 10)), .sin_family = addressFamily};
        if (-1 == zsock_bind(data->sock, (const struct sockaddr*)&addr, sizeof(struct sockaddr_in)))
        {
            LOG_ERROR("Could not bind socket");
            zsock_close(data->sock);
            data->sock = -1;
        }
      }else if(addressFamily == AF_INET6){
        struct sockaddr_in6 addr = {.sin6_addr = IN6ADDR_ANY_INIT, .sin6_port = htons(strtol(port, NULL, 10)), .sin6_family = addressFamily};
        if (-1 == zsock_bind(data->sock, (const struct sockaddr*)&addr, sizeof(struct sockaddr_in6)))
        {
            LOG_ERROR("Could not bind socket");
            zsock_close(data->sock);
            data->sock = -1;
        }
      }else{
        LOG_ARG_ERROR("Unknown address Family %d", addressFamily);
        zsock_close(data->sock);
        data->sock = -1;
      }
      
  }else{
    LOG_ARG_ERROR("COuld not create socket. Returned %d", data->sock);
  }

  return data->sock;
}

ssize_t lwm2m_socket_send(client_data_t * cdata, const void * data, size_t n, const struct sockaddr * addr){
  LOG("Entering");
  size_t offset = 0;
  while (offset != n){
    ssize_t nbSent = 0;
    if(cdata->addressFamily == AF_INET){
      char address[64] = {};
      inet_ntop(AF_INET, &((struct sockaddr_in *)addr)->sin_addr, address, sizeof(struct sockaddr_in));
      LOG_ARG("Send %d bytes to address %s on Port %d", n, address, ntohs(((struct sockaddr_in *)addr)->sin_port));
      nbSent = zsock_sendto(cdata->sock, (uint8_t*)data + offset, n - offset, 0, addr, sizeof(struct sockaddr_in));
    }else{
      char address[64] = {};
      inet_ntop(AF_INET6, &((struct sockaddr_in6 *)addr)->sin6_addr, address, sizeof(struct sockaddr_in6));
      LOG_ARG("Send %d bytes to address %s on Port %d", n, address, ntohs(((struct sockaddr_in6 *)addr)->sin6_port));
      nbSent = zsock_sendto(cdata->sock, (uint8_t*)data + offset, n - offset, 0, addr, sizeof(struct sockaddr_in6));
    }
    if (nbSent == -1){
        LOG_ERROR("Could not send data");
        return COAP_501_NOT_IMPLEMENTED;
    }
    offset += nbSent;
  }

  return n;
}

ssize_t lwm2m_socket_recv(int sock, void * data, size_t max_num_bytes, struct sockaddr * addr){
  socklen_t peer_addrLen = sizeof(struct addrinfo);
  ssize_t num_bytes = zsock_recvfrom(sock, data, max_num_bytes, 0, addr, &peer_addrLen);
  if(num_bytes > 0) LOG_ARG("Received %d bytes", num_bytes);
  return num_bytes;
}

bool lwm2m_socket_data_available(int sock, int timeout_ms){
  struct zsock_pollfd pollfd = {
    .fd = sock,
    .events = ZSOCK_POLLIN
  };
  int ret = zsock_poll(&pollfd, 1, timeout_ms);

  if(pollfd.revents & ZSOCK_POLLIN){
    return true;
  }else{
    return false;
  }
}

int lwm2m_rand(){
  return sys_rand32_get();
}

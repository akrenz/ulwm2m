/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <internal.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <poll.h>

void * lwm2m_malloc(size_t s){
  return malloc(s);
}

void * lwm2m_calloc(const size_t n, const size_t s)
{
  return calloc(n, s);
}

void lwm2m_free(void * p){
  return free(p);
}

char * lwm2m_strdup(const char * str){
  return strdup(str);
}

int lwm2m_strncmp(const char * s1, const char * s2, size_t n){
  return strncmp(s1, s2, n);
}

time_t lwm2m_gettime(void){
  struct timeval tv;

  if (0 != gettimeofday(&tv, NULL))
  {
      return -1;
  }

  return tv.tv_sec;
}

void lwm2m_printf(const char * format, ...){
  va_list ap;

  va_start(ap, format);

  vfprintf(stderr, format, ap);
  
  va_end(ap);
}

void lwm2m_close_socket(client_data_t * data){
  close(data->sock);
}

int lwm2m_connect_socket(client_data_t * data, const char * port, const int addressFamily){
  data->sock = -1;
  struct addrinfo hints;
  struct addrinfo *res;
  struct addrinfo *p;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = addressFamily;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;
  if (0 != getaddrinfo(NULL, port, &hints, &res))
  {
      return -1;
  }
  for(p = res ; p != NULL && data->sock == -1 ; p = p->ai_next)
  {
      data->sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
      if (data->sock >= 0)
      {
          if (-1 == bind(data->sock, p->ai_addr, p->ai_addrlen))
          {
              close(data->sock);
              data->sock = -1;
          }
      }
  }

  struct timeval tv;
  tv.tv_sec = 10;
  tv.tv_usec = 0;
  setsockopt(data->sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
  freeaddrinfo(res);

  return data->sock;
}

ssize_t lwm2m_socket_send(client_data_t * cdata, const void * data, size_t n, __CONST_SOCKADDR_ARG addr){
  LOG("Entering");
  size_t offset = 0;
  while (offset != n){
    ssize_t nbSent = 0;
    if(cdata->addressFamily == AF_INET){
      LOG_ARG("Send %d bytes to address %s on Port %d", n, inet_ntoa(((struct sockaddr_in *)addr)->sin_addr), ntohs(((struct sockaddr_in *)addr)->sin_port));
      nbSent = sendto(cdata->sock, data + offset, n - offset, 0, addr, sizeof(struct sockaddr_in));
    }else{
      char address[64] = {};
      inet_ntop(AF_INET6, &((struct sockaddr_in6 *)addr)->sin6_addr, address, sizeof(struct sockaddr_in6));
      LOG_ARG("Send %d bytes to address %s on Port %d", n, address, ntohs(((struct sockaddr_in6 *)addr)->sin6_port));
      nbSent = sendto(cdata->sock, data + offset, n - offset, 0, addr, sizeof(struct sockaddr_in6));
    }
    if (nbSent == -1){
        LOG_ERROR("Could not send data");
        return COAP_501_NOT_IMPLEMENTED;
    }
    offset += nbSent;
  }

  return n;
}

ssize_t lwm2m_socket_recv(int sock, void * data, size_t max_num_bytes, struct sockaddr * addr){
  socklen_t peer_addrLen = sizeof(struct addrinfo);
  ssize_t num_bytes = recvfrom(sock, data, max_num_bytes, 0, addr, &peer_addrLen);
  if(num_bytes > 0) LOG_ARG("Received %d bytes", num_bytes);
  return num_bytes;
}

bool lwm2m_socket_data_available(int sock, int timeout_ms){
  struct pollfd pollfd = {
    .fd = sock,
    .events = POLLIN
  };
  int ret = poll(&pollfd, 1, timeout_ms);

  if(pollfd.revents & POLLIN){
    return true;
  }else{
    return false;
  }
}

int lwm2m_rand(){
  srand((int)lwm2m_gettime());
  return rand();
}

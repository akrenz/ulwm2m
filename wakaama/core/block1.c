/*******************************************************************************
 *
 * Copyright (c) 2016 Intel Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v20.html
 * The Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Simon Bernard - initial API and implementation
 *
 *******************************************************************************/
/*
 Copyright (c) 2016 Intel Corporation

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
     * Neither the name of Intel Corporation nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "internals.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

uint8_t coap_block1_handler(lwm2m_block_data_t ** pBlockData,
                            uint16_t mid,
                            uint16_t blockSize,
                            uint32_t blockNum,
                            bool blockMore)
{
  lwm2m_block_data_t * blockData = *pBlockData;

  // manage new block1 transfer
  if (blockNum == 0){
    // we already have block1 data for this server, clear it
    if (*pBlockData == NULL){
        *pBlockData = lwm2m_malloc(sizeof(lwm2m_block_data_t));
        if (NULL == *pBlockData) return COAP_500_INTERNAL_SERVER_ERROR;
        blockData = *pBlockData;
    }

  }else { // manage already started block1 transfer

    if (blockData == NULL){
      // we never receive the first block or block is out of order
      return COAP_408_REQ_ENTITY_INCOMPLETE;
    }

    if(blockNum <= blockData->blockNum){
      //ignore retransmission
      return COAP_IGNORE;
    }
  }

  blockData->blockNum = blockNum;
  blockData->mid = mid;

  if (blockMore){
    return COAP_231_CONTINUE;
  }

  lwm2m_free(*pBlockData);
  *pBlockData = NULL;
  return NO_ERROR;
}

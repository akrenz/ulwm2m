/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef ULWM2M_SERVER_H_
#define ULWM2M_SERVER_H_

#ifndef LWM2M_SERVER_MODE
#define LWM2M_SERVER_MODE
#endif
#include "../../wakaama/core/liblwm2m.h"

#include <netinet/in.h>

/**
 * @brief Creates an lwm2m context bound to a specific receiver address
 * 
 * @param objects Array of lwm2m Objects
 * @param num Nuber of Objects in objects Array
 * @param name Name of the Client
 * @param port Port number for communication
 * @param addressFamily AF_INET or AF_INET6
 *
 * @return lwm2m_context_t* Returns a context handle for further use. NULL in case of an error.
 */
lwm2m_context_t * lwm2m_create_context(lwm2m_object_t objects[], size_t num, const char * name, const char* port, const int addressFamily);

/**
 * @brief Close a previously created context
 * 
 * @param context Pointer to a previously created context handle.
 * 
 * @return int Returns 0 if successfull, -1 in case of an error.
 */
int lwm2m_close_context(lwm2m_context_t* context);

/**
 * @brief Main process function to handle communication
 * 
 * between Handles communication between LWM2M Server and LWM2M Client. A context must be created previously via
 * lwm2m_create_context(). This function must be called regularly, e.g. within a while-loop.
 * 
 * @param context Pointer to a previously created context handle.
 */
void lwm2m_process(lwm2m_context_t* context);

/**
 * @brief Remove client object from server context 
 *  * 
 * @param client Pointer to client object
 * @return Returns 0 on success and -1 on failure
 */
int lwm2m_free_client(lwm2m_client_t * client);

/**
 * @brief Remove remote client object from a lwm2m_client_t 
 *  * 
 * @param object Pointer
 * @return Returns 0 on success and -1 on failure
 */
int lwm2m_free_client_object(lwm2m_client_object_t * object);

#endif /* ULWM2M_SERVER_H_ */
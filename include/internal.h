/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
 
#ifndef ULWM2M_INTERNAL_H_
#define ULWM2M_INTERNAL_H_

#include "ulwm2m.h"
#include <sys/types.h>

#ifdef ESP_PLATFORM
#include <lwip/sockets.h>
#elif __ZEPHYR__
#include <zephyr/kernel.h>
#include <zephyr/net/socket.h>
#elif __unix__
#include <sys/socket.h>
#include <netdb.h>
#else
 #error "unsupported Platform"
#endif

typedef struct{
  lwm2m_object_t * securityObjP;
  int sock;
  void * sessionHandle;
  int addressFamily;
} client_data_t;

#ifdef ULWM2M_WITH_LOGS
#define LOG(x) lwm2m_printf("[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ERROR(x) lwm2m_printf("[%s:%d] %s\n", __FUNCTION__, __LINE__, x);
#define LOG_ARG(FMT, ...) lwm2m_printf("[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)
#define LOG_ARG_ERROR(FMT, ...) lwm2m_printf("[%s:%d] " FMT "\r\n", __FUNCTION__ , __LINE__ , __VA_ARGS__)
#else
#define LOG(x)
#define LOG_ERROR(x)
#define LOG_ARG(FMT, ...)
#define LOG_ARG_ERROR(FMT, ...)
#endif


/* this methods must be implemented in platform specific code */
int lwm2m_connect_socket(client_data_t * data, const char* port, const int addressFamily);
void lwm2m_close_socket(client_data_t * data);

ssize_t lwm2m_socket_send(client_data_t * sock, const void * data, size_t n, const struct sockaddr * addr);
ssize_t lwm2m_socket_recv(int sock, void * data, size_t max_num_bytes, struct sockaddr * addr);
bool lwm2m_socket_data_available(int sock, int timeout_ms);


#endif /* ULWM2M_INTERNAL_H_ */
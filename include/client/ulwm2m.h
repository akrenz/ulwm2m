/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef ULWM2M_CLIENT_H_
#define ULWM2M_CLIENT_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LWM2M_CLIENT_MODE
#define LWM2M_CLIENT_MODE
#endif

#include "../../wakaama/core/liblwm2m.h"
#include "../../client/object_security.h"
#include "../../client/object_device.h"
#include "../../client/object_server.h"
#include "../../client/object_digital_output.h"
#include "../../client/object_frequency.h"
#include "../../client/object_power.h"
#include "../../client/object_firmware_update.h"

typedef enum {
    SECURITY_MODE_PSK=0,
    SECURITY_MODE_RPK,
    SECURITY_MODE_CERTIFICATE,
    SECURITY_MODE_NO_SECURITY,
    SECURITY_MODE_WITH_EST
}lwm2m_security_mode_t;

typedef uint8_t (*lwm2m_free_instance_data_callback)(void * data);

typedef struct _lwm2m_instance_t {
  struct _lwm2m_instance_t * next; // matches lwm2m_list_t::next
  uint16_t id;  // matches lwm2m_list_t::id
  void * data;  // holds data assigned to instance and passed to callback functions
  lwm2m_free_instance_data_callback free_data_callback; // callback
  bool freeDataOnClear;
} lwm2m_instance_t;

/**
 * @brief Creates an lwm2m context bound to a specific receiver address
 * 
 * @param objects Array of lwm2m Objects
 * @param num Nuber of Objects in objects Array
 * @param name Name of the Client
 * @param port Port number for communication
 * @param addressFamily AF_INET or AF_INET6
 * @param timeout Timeout time for poll in milliseconds. If 0 is specified, poll will return immediately. If -1 is specified poll will wait block until an event occurs
 *
 * @return lwm2m_context_t* Returns a context handle for further use. NULL in case of an error.
 */
lwm2m_context_t * lwm2m_create_context(lwm2m_object_t objects[], size_t num, const char * name, const char* port, const int addressFamily, int timeout);

/**
 * @brief Close a previously created context
 * 
 * @param context Pointer to a previously created context handle.
 * 
 * @return int Returns 0 if successfull, -1 in case of an error.
 */
int lwm2m_close_context(lwm2m_context_t* context);

/**
 * @brief Main process function to handle communication
 * 
 * between Handles communication between LWM2M Server and LWM2M Client. A context must be created previously via
 * lwm2m_create_context(). This function must be called regularly, e.g. within a while-loop.
 * 
 * @param context Pointer to a previously created context handle.
 */
void lwm2m_process(lwm2m_context_t* context);

/**
 * @brief Initialize an lwm2m_object_t object
 * 
 * @param obj Pointer to object to initalize
 * @param objID ID of the object in the form LWM2M_*_OBJECT_ID (e.g. LWM2M_SECURITY_OBJECT_ID)
 * @param read callback function to be called if server wants to read data of the object.
 * @param write Callback function to be called if server wants to write data.
 * @param exec Callback function which gets executed if server calls the exec endpoint of the object.
 * @param create 
 * @param delete 
 * @param discover Callback function that reports all action endpoints of the object.
 * @return Returns 0 on success and -1 on failure 
 */
int lwm2m_init_object(lwm2m_object_t * obj, const uint16_t objId, const lwm2m_read_callback_t read, const lwm2m_write_callback_t write, const lwm2m_execute_callback_t exec, const lwm2m_create_callback_t create, const lwm2m_delete_callback_t del, const lwm2m_discover_callback_t discover, const lwm2m_block1_write_callback_t write_block, const lwm2m_block1_execute_callback_t exec_block, const lwm2m_block1_create_callback_t create_block);

/**
 * @brief Cleanup the object pointed to by `object` and all its instances
 * 
 * Cleanup an object previously initialized by lwm2m_init_object. It will delete all instances
 * assigned to the object.
 * 
 * @param object Pointer to object to delete
 * @return Returns 0 on success and -1 on failure
 */
int lwm2m_clear_object(lwm2m_object_t * object);

/**
 * @brief Remove client object from server context 
 *  * 
 * @param client Pointer to client object
 * @return Returns 0 on success and -1 on failure
 */
int lwm2m_free_client(lwm2m_client_t * client);

/**
 * @brief Remove remote client object from a lwm2m_client_t 
 *  * 
 * @param object Pointer
 * @return Returns 0 on success and -1 on failure
 */
int lwm2m_free_client_object(lwm2m_client_object_t * object);

/**
 * @brief Create an Instance to add to an existing object
 * 
 * Every object needs at least one instance. Each instance holds its own data
 * buffer pointed to by `data`. To identify each instance an unique id is
 * needed for each instance. `free_data_callback`is needed if `data`holds
 * dynamically allocated memory. Pass in a function pointer of type
 * `lwm2m_free_instance_data_callback`. This called is called during object
 * deletion (which also deletes all of its isntances). If there is no
 * dynamically allocated memory inside `data` this function pointer can be NULL
 * 
 * @param id Short id to identify the instance
 * @param data Pointer to memory used by the instance
 * @param free_data_callback Callback to be called during instance deletion. Callback should free all dynamically alocated memory inside `data`. Can be NULL if no dynamically allocated memory is used inside data.
 * @param freeDataonClear will call lwm2m_free on /p data if true. Use this if you have malloced the data pointer previously
 * 
 * @return lwm2m_object_t* Pointer to created instance
 */
lwm2m_instance_t * lwm2m_create_instance(const uint16_t id, void * data, lwm2m_free_instance_data_callback free_data_callback, bool freeDataOnClear); 

/**
 * @brief Adds an instance to a given object.
 * 
 * @param obj Pointer to Object to add the instance
 * @param instance  Pointer to instance created by XXXX
 * 
 * @return Returns 0 on Success and -1 on failure
 */
int lwm2m_object_add_instance(lwm2m_object_t * obj, lwm2m_instance_t * instance);

void lwm2m_print_state(lwm2m_context_t * lwm2mH);

#ifdef __cplusplus
}
#endif

#endif /* ULWM2M_CLIENT_H_ */
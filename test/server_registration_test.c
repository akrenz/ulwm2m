/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
#include "ethpacket.h"
#include "utest.h"
#include "platform.h"

#include <ulwm2m.h>
// #include <internals.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* test data and acees Macro */
typedef struct{
  lwm2m_context_t * ctx;
  coap_option_data_t opts[6];
}test_state_t;
#define TEST_DATA_CTX(state) ((test_state_t*)*state)->ctx
#define TEST_DATA_OPTS(state) ((test_state_t*)*state)->opts

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  TEST_DATA_CTX(state) = lwm2m_init(NULL);
  
  return 0;
}

static int group_teardown(void **state){
  lwm2m_close(TEST_DATA_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static int test_setup(void ** state){
  TEST_DATA_OPTS(state)[0].option = COAP_OPTION_URI_PATH;
  TEST_DATA_OPTS(state)[0].length = 2;
  TEST_DATA_OPTS(state)[0].data.u16 = 0x6472;

  TEST_DATA_OPTS(state)[1].option = COAP_OPTION_CONTENT_TYPE;
  TEST_DATA_OPTS(state)[1].length=1;
  // TEST_DATA_OPTS(state)[1].data.u8p=lwm2m_malloc(sizeof(uint8_t));
  TEST_DATA_OPTS(state)[1].data.u8 = 0x28;

  TEST_DATA_OPTS(state)[2].option = COAP_OPTION_URI_QUERY;
  TEST_DATA_OPTS(state)[2].length=strlen("lwm2m=1.0");
  TEST_DATA_OPTS(state)[2].data.u8p="lwm2m=1.0";
  
  TEST_DATA_OPTS(state)[3].option = COAP_OPTION_URI_QUERY;
  TEST_DATA_OPTS(state)[3].length=strlen("ep=testlwm2mclient");
  TEST_DATA_OPTS(state)[3].data.u8p="ep=testlwm2mclient";

  TEST_DATA_OPTS(state)[4].option = COAP_OPTION_URI_QUERY;
  TEST_DATA_OPTS(state)[4].length=3;
  TEST_DATA_OPTS(state)[4].data.u8p="b=U";

  TEST_DATA_OPTS(state)[5].option = COAP_OPTION_URI_QUERY;
  TEST_DATA_OPTS(state)[5].length=6;
  TEST_DATA_OPTS(state)[5].data.u8p="lt=300";
  return 0;
}

static int test_teardown(void ** state){
  /* remove clients added during test fom context */
  lwm2m_client_t * client = TEST_DATA_CTX(state)->clientList;
  while(client != NULL){
    lwm2m_client_t * clientInst = client;
    client = client->next;
    lwm2m_free_client(clientInst);
  }
  TEST_DATA_CTX(state)->clientList = NULL;

  return 0;
}

/* helper functions, macros for accessing objects */

lwm2m_object_t* obj(void ** state, const int id){
  assert_non_null(TEST_DATA_CTX(state)->clientList);
  assert_non_null(TEST_DATA_CTX(state)->clientList->objectList);
  return (lwm2m_object_t*)LWM2M_LIST_FIND(TEST_DATA_CTX(state)->clientList->objectList, id);
}
static void obj_has_instances(lwm2m_object_t * obj, const int* instances, const size_t numInstances){
    assert_non_null(obj);
    for(int i=0; i< numInstances;i++){
      assert_non_null(LWM2M_LIST_FIND(obj->instanceList, instances[i]));
    }
}

static void request_objects(void **state){
    coap_t req = {};
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    obj_has_instances(obj(state, 1), (int[]) {0}, 1);
    obj_has_instances(obj(state, 3), (int[]) {0}, 1);
    obj_has_instances(obj(state, 31024), (int[]) {1, 2}, 2);
}

static void request_fails_if_version_not_1(void **state){
    coap_t req = {};
    TEST_DATA_OPTS(state)[2].data.u8p = "lwm2m=2.0";
    TEST_DATA_OPTS(state)[2].length = strlen("lwm2m=2.0");
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);

    /* check if no client has been added */
    lwm2m_client_t * client = (lwm2m_client_t*)LWM2M_LIST_FIND(TEST_DATA_CTX(state)->clientList, 1);
    assert_null(client);
}

static void request_fails_if_version_is_missing(void **state){
    coap_t req = {};
    TEST_DATA_OPTS(state)[2].data.u8p = "lwm2m=";
    TEST_DATA_OPTS(state)[2].length = strlen("lwm2m=");
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);

    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_fails_if_version_is_missing_2(void **state){
    coap_t req = {};
    TEST_DATA_OPTS(state)[2].length = 0;
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);

    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_fails_if_client_name_is_missing(void **state){
    coap_t req = {};
    TEST_DATA_OPTS(state)[3].length=strlen("ep=");
    TEST_DATA_OPTS(state)[3].data.u8p="ep=";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);

    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_fails_if_client_name_is_missing_2(void **state){
    coap_t req = {};
    TEST_DATA_OPTS(state)[3].length=0;
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), "</>,</1/0>,</3/0>,</31024/1>,</31024/2>");

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);

    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_object_with_version_identifier(void **state){
    coap_t req = {};
    const char * payload = "</>,</1/0>,</3/0>,</31024>;ver=\"1.9\",</31024/1>,</31024/2>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    obj_has_instances(obj(state, 1), (int []) {0}, 1);
    obj_has_instances(obj(state, 3), (int []) {0}, 1);
    obj_has_instances(obj(state, 31024), (int []) {1, 2}, 2);
}

static void request_with_alt_path(void **state){
    coap_t req = {};
    const char * payload = "</lwm2m>;rt=\"oma.lwm2m\",</1/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    obj_has_instances(obj(state, 1), (int []) {0}, 1);
    assert_string_equal(TEST_DATA_CTX(state)->clientList->altPath, "lwm2m");
}

static void request_without_altpath_has_altPath_nullptr(void **state){
    coap_t req = {};
    const char * payload = "</>;rt=\"oma.lwm2m\",</1/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    assert_null(TEST_DATA_CTX(state)->clientList->altPath);
}

static void request_with_json_support(void **state){
    coap_t req = {};
    const char * payload = "</>;ct=11543,</1/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    obj_has_instances(obj(state, 1), (int []) {0}, 1);
    assert_true(TEST_DATA_CTX(state)->clientList->supportJSON);
}

static void request_fails_with_invalid_json(void **state){
    coap_t req = {};
    const char * payload = "</>;ct=,</1/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_missing_preceding_separator_fails(void **state){
    coap_t req = {};
    const char * payload = "</>,<1>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_missing_preceding_separator_fails_2(void **state){
    coap_t req = {};
    const char * payload = "</>,<1>,</3/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    
    assert_null(TEST_DATA_CTX(state)->clientList);
}

static void request_lifetime_zero_sets_default_lifetime(void **state){
    coap_t req = {};
    /* length == disables the option */
    TEST_DATA_OPTS(state)[5].length=0;
    const char * payload = "</>,</3/0>";
    size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_POST, TEST_DATA_OPTS(state), sizeof(TEST_DATA_OPTS(state))/sizeof(coap_option_data_t), payload);

    lwm2m_handle_packet(TEST_DATA_CTX(state), (uint8_t*)&req, numbytes, NULL);
    assert_non_null(TEST_DATA_CTX(state)->clientList);
    assert_int_equal(TEST_DATA_CTX(state)->clientList->lifetime, 86400);
    
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test_setup_teardown(request_objects, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_if_version_not_1, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_object_with_version_identifier, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_with_alt_path, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_without_altpath_has_altPath_nullptr, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_with_json_support, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_missing_preceding_separator_fails, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_missing_preceding_separator_fails_2, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_if_client_name_is_missing, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_if_client_name_is_missing_2, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_if_version_is_missing, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_if_version_is_missing_2, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_fails_with_invalid_json, test_setup, test_teardown),
  cmocka_unit_test_setup_teardown(request_lifetime_zero_sets_default_lifetime, test_setup, test_teardown),
};

DEFINE_TEST("Server Registration", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include <ulwm2m.h>

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <arpa/inet.h>

typedef struct{
  lwm2m_context_t * ctx;
}test_state_t;
#define TEST_DATA_CTX(state) ((test_state_t*)*state)->ctx

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  TEST_DATA_CTX(state) = lwm2m_init(NULL);
  return 0;
}

static int group_teardown(void **state){
  lwm2m_close(TEST_DATA_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static void parse_tlv(void **state){
  uint8_t tlv[3] ={0xC1, 0x00, 0x7B};
  lwm2m_data_t * res = NULL;
  lwm2m_uri_t uri = {.objectId = 1, .instanceId = 0, .resourceId = 0};
  int ret = lwm2m_data_parse(&uri, tlv, sizeof(tlv), LWM2M_CONTENT_TLV, &res);

  assert_int_equal(ret, 1);
  assert_int_equal(res[0].value.asBuffer.length, 1);
  assert_int_equal(*res[0].value.asBuffer.buffer, 123);

  lwm2m_data_free(ret, res);
}

static void parse_tlv_2_byte(void **state){
  uint8_t tlv[] ={0xC2, 0x00, 0x1F, 0x40};
  lwm2m_data_t * res = NULL;
  lwm2m_uri_t uri = {.objectId = 1, .instanceId = 0, .resourceId = 0};
  int ret = lwm2m_data_parse(&uri, tlv, sizeof(tlv), LWM2M_CONTENT_TLV, &res);

  assert_int_equal(ret, 1);
  assert_int_equal(res[0].value.asBuffer.length, 2);
  assert_int_equal(ntohs(*((uint16_t*)res[0].value.asBuffer.buffer)), 8000);

  lwm2m_data_free(ret, res);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(parse_tlv),
  cmocka_unit_test(parse_tlv_2_byte),
};

DEFINE_TEST("Data Parse", tests, group_setup, group_teardown);


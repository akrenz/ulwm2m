/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <ulwm2m.h>
#include "utest.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
  lwm2m_object_t obj;
  lwm2m_data_t * data;
}test_state_t;

#define SERVER_STATE_OBJ(state) ((test_state_t*)*state)->obj
#define SERVER_TEST_DATA(state) ((test_state_t*)*state)->data
#define SERVER_INST_DATA(_obj, _data) ((lwm2m_server_instance_data_t*) ((lwm2m_instance_t*)_obj->instanceList)->data)->_data

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  lwm2m_server_instance_data_t * server = lwm2m_malloc(sizeof(lwm2m_server_instance_data_t));
  server->binding = BINDING_S;
  server->defaultMaxObservationPeriod = 200;
  server->defaultMinObservationPeriod = 100;
  server->lifetime = 500;
  server->shortServerId= 123;
  server->storing = true;

  lwm2m_init_object(&SERVER_STATE_OBJ(state), LWM2M_SERVER_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_instance_t * inst = lwm2m_create_instance(0, server, NULL, true);
  lwm2m_object_add_instance(&SERVER_STATE_OBJ(state), inst);

  SERVER_TEST_DATA(state) = lwm2m_data_new(1);

  return 0;
}

static int group_teardown(void **state){
  // lwm2m_instance_t * inst = (lwm2m_instance_t*)SERVER_STATE_OBJ(state).instanceList;
  // lwm2m_free(inst->data);
  lwm2m_clear_object(&SERVER_STATE_OBJ(state));

  lwm2m_data_free(1, SERVER_TEST_DATA(state));

  lwm2m_free(*state);
  return 0;
}

static void lwm2m_server_read_short_id(void **state){

  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_SHORT_ID_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_int_equal(123, data->value.asInteger);
}

static void lwm2m_server_read_lifetime(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_LIFETIME_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_int_equal(500, data->value.asInteger);
}

static void lwm2m_server_read_min_observation_period(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_MIN_PERIOD_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_int_equal(100, data->value.asInteger);
}

static void lwm2m_server_read_max_observation_period(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_MAX_PERIOD_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_int_equal(200, data->value.asInteger);
}

static void lwm2m_server_read_storing(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_STORING_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_true(data->value.asBoolean);
}

static void lwm2m_server_read_binding(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_BINDING_ID;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 1);
  assert_int_equal(data->value.asBuffer.length, 1);
  const char expected = 'S';
  assert_memory_equal(data->value.asBuffer.buffer, &expected, 1);

  lwm2m_free(data->value.asBuffer.buffer);
}

static void lwm2m_server_read_num_data_all_requested(void **state){
  int numData = 0;
  lwm2m_data_t * data;
  lwm2m_server_read(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(numData, 6);
  lwm2m_data_free(numData, data);
}

static void lwm2m_server_write_lifetime(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_LIFETIME_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 20;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(SERVER_INST_DATA(obj, lifetime), 20);
}

static void lwm2m_server_write_min_observation(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);
  
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_MIN_PERIOD_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 20;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(SERVER_INST_DATA(obj, defaultMinObservationPeriod), 20);
}

static void lwm2m_server_write_max_observation(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);

  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_MAX_PERIOD_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 20;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(SERVER_INST_DATA(obj, defaultMaxObservationPeriod), 20);
}

static void lwm2m_server_write_storing(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);

  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_STORING_ID;
  data->type = LWM2M_TYPE_BOOLEAN;
  data->value.asBoolean = false;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_false(SERVER_INST_DATA(obj, storing));
}

static void lwm2m_server_write_binding(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);

  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_BINDING_ID;
  data->type = LWM2M_TYPE_STRING;
  data->value.asBuffer.length = strlen("UQ");
  data->value.asBuffer.buffer = (uint8_t*)lwm2m_strdup("UQ");
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(SERVER_INST_DATA(obj, binding), BINDING_UQ);
  lwm2m_free(data->value.asBuffer.buffer);
}

static void lwm2m_server_write_short_id_fails(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_SHORT_ID_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 1;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);

  assert_int_not_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
}

static void lwm2m_server_write_disable_fails(void**state){
  lwm2m_object_t * obj= &SERVER_STATE_OBJ(state);

  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_DISABLE_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 1;
  uint8_t ret = lwm2m_server_write(0, 1, data, obj);
  
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
}

static void lwm2m_server_write_registration_update_fails(void**state){
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_UPDATE_ID;
  data->type = LWM2M_TYPE_INTEGER;
  data->value.asInteger = 1;
  uint8_t ret = lwm2m_server_write(0, 1, data, &SERVER_STATE_OBJ(state));
  
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
}

static void lwm2m_server_discover_all_id(void **state){
  int numData = 0;
  lwm2m_data_t * data = NULL;
  uint8_t ret = lwm2m_server_discover(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(numData, 9);
  assert_non_null(data);

  lwm2m_data_free(numData, data);
}

static void lwm2m_server_discover_id(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = LWM2M_SERVER_UPDATE_ID;
  uint8_t ret = lwm2m_server_discover(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(numData, 1);
}

static void lwm2m_server_discover_invalid_id_fails(void **state){
  int numData = 1;
  lwm2m_data_t * data = SERVER_TEST_DATA(state);
  data->id = 15;
  uint8_t ret = lwm2m_server_discover(0, &numData, &data, &SERVER_STATE_OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  assert_int_equal(numData, 1);
}

static void lwm2m_server_create_callback(void **state){
  int numData = 2;
  lwm2m_data_t * data = lwm2m_data_new(numData);
  data[0].id = LWM2M_SERVER_LIFETIME_ID;
  data[0].type = LWM2M_TYPE_INTEGER;
  data[0].value.asInteger = 100;
  data[1].id = LWM2M_SERVER_BINDING_ID;
  data[1].type = LWM2M_TYPE_STRING;
  data[1].value.asBuffer.buffer = lwm2m_strdup("US");
  data[1].value.asBuffer.length = strlen("US");
  uint8_t ret = lwm2m_server_create(2, numData, data, &SERVER_STATE_OBJ(state));

  assert_int_equal(ret, COAP_201_CREATED);
  lwm2m_instance_t * inst = (lwm2m_instance_t *)LWM2M_LIST_FIND(SERVER_STATE_OBJ(state).instanceList, 2);
  assert_non_null(inst);
  lwm2m_server_instance_data_t * instData = (lwm2m_server_instance_data_t*)inst->data;
  assert_int_equal(instData->lifetime, 100);
  assert_int_equal(instData->binding, BINDING_US);

  lwm2m_data_free(numData, data);
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(lwm2m_server_read_short_id),
  cmocka_unit_test(lwm2m_server_read_lifetime),
  cmocka_unit_test(lwm2m_server_read_min_observation_period),
  cmocka_unit_test(lwm2m_server_read_max_observation_period),
  cmocka_unit_test(lwm2m_server_read_binding),
  cmocka_unit_test(lwm2m_server_read_storing),
  cmocka_unit_test(lwm2m_server_read_num_data_all_requested),

  /* write tests */
  cmocka_unit_test(lwm2m_server_write_lifetime),
  cmocka_unit_test(lwm2m_server_write_min_observation),
  cmocka_unit_test(lwm2m_server_write_max_observation),
  cmocka_unit_test(lwm2m_server_write_storing),
  cmocka_unit_test(lwm2m_server_write_binding),

  /* writes which should fail */
  cmocka_unit_test(lwm2m_server_write_short_id_fails),
  cmocka_unit_test(lwm2m_server_write_disable_fails),
  cmocka_unit_test(lwm2m_server_write_registration_update_fails),

  /* discover tests */
  cmocka_unit_test(lwm2m_server_discover_all_id),
  cmocka_unit_test(lwm2m_server_discover_id),
  cmocka_unit_test(lwm2m_server_discover_invalid_id_fails),

  /* create tests */
  cmocka_unit_test(lwm2m_server_create_callback),
};

DEFINE_TEST("Object Server", tests, group_setup, group_teardown);
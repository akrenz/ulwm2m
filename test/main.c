/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "utest.h"

extern test_t  __start_test[];
extern test_t  __stop_test[];

#define  NUM_TEST  ((size_t)(__stop_test - __start_test))
#define  TEST(i)   ((__start_test) + (i))

int main(){

  test_t * curr_test = NULL;
  int ret = 0;
  for(curr_test = __start_test; curr_test < __stop_test; curr_test++){
    ret |= _cmocka_run_group_tests(curr_test->group_name ,curr_test->tests, curr_test->num_tests, curr_test->group_setup, curr_test->group_teardown);
  }

  return ret;
}
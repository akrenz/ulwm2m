/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include "platform.h"

#include <ulwm2m.h>
#include <internal.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>

#define UNUSED(x) (void)(sizeof(x))

/* setup and teardown testsuite */
typedef struct{
  lwm2m_context_t * ctx;
  lwm2m_object_t objs[2];
  client_data_t userData;
  uint8_t sessionH;
}test_state_t;

#define TEST_CTX(state) ((test_state_t*)*state)->ctx
#define TEST_OBJS(state) ((test_state_t*)*state)->objs
#define TEST_CTX_USER_DATA(state) ((test_state_t*)*state)->userData
#define TEST_SESSION_HANDLE(state) ((test_state_t*)*state)->sessionH


/** mock write firmware callback */
static uint8_t write_firmware_callback(lwm2m_uri_t * uriP, lwm2m_media_type_t format, uint8_t * buffer, int length, lwm2m_object_t * objectP, uint32_t block_num, uint8_t block_more){
  check_expected(length);
  check_expected(buffer);
  check_expected(block_num);
  check_expected(block_more);
  
  function_called();
  return (uint8_t)mock();
}

static int cleanup_block_data(void **state){
  if(TEST_CTX(state)->serverList->block1Data != NULL){
    lwm2m_free(TEST_CTX(state)->serverList->block1Data);
    TEST_CTX(state)->serverList->block1Data = NULL;
  }

  /* reset values for lwm2m_socket_send() */
  memset(sent_message, 0, sizeof(sent_message));
  sent_bytes = 0;

  return 0;
}

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));

  TEST_CTX(state) = lwm2m_init(&TEST_CTX_USER_DATA(state));

  /* add dummy server to context*/
  TEST_CTX(state)->serverList = lwm2m_malloc(sizeof(lwm2m_server_t));
  memset(TEST_CTX(state)->serverList, 0, sizeof(lwm2m_server_t));
  TEST_CTX(state)->serverList->sessionH = &TEST_SESSION_HANDLE(state);
  TEST_CTX(state)->serverList->status = STATE_REGISTERED;

  /* Add Dummy Server Object*/
  /* create static server object */
  lwm2m_init_object(&TEST_OBJS(state)[0], LWM2M_SERVER_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, NULL, NULL, false);
  lwm2m_object_add_instance(&TEST_OBJS(state)[0], server_inst);

  /* Add Dummy Firmware Update Object */
  lwm2m_init_object(&TEST_OBJS(state)[1], LWM2M_FIRMWARE_UPDATE_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, write_firmware_callback, NULL, NULL);
  lwm2m_instance_t * firmware_inst = lwm2m_create_instance(0, NULL, NULL, false);
  lwm2m_object_add_instance(&TEST_OBJS(state)[1], firmware_inst);

  TEST_CTX(state)->objectList = (lwm2m_object_t*)lwm2m_list_add((lwm2m_list_t *)TEST_CTX(state)->objectList, (lwm2m_list_t *)&TEST_OBJS(state)[0]);
  TEST_CTX(state)->objectList = (lwm2m_object_t*)lwm2m_list_add((lwm2m_list_t *)TEST_CTX(state)->objectList, (lwm2m_list_t *)&TEST_OBJS(state)[1]);

  return 0;
}

static int group_teardown(void **state){
  lwm2m_free(TEST_CTX(state)->serverList);
  lwm2m_free(TEST_CTX(state)->objectList[1].instanceList);
  lwm2m_free(TEST_CTX(state)->objectList[0].instanceList);
  lwm2m_free(TEST_CTX(state));
  lwm2m_free(*state);
  return 0;
}

int check_opaque_data_buffer(const LargestIntegralType _value, const LargestIntegralType _expected){
  lwm2m_data_t * value = (lwm2m_data_t*)_value;
  const char * expected = (const char*)_expected;
  if(strncmp(value->value.asBuffer.buffer, expected, value->value.asBuffer.length) == 0) return true;
  return false;
}

static void blockwise_transfer_single_block(void **state){
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u16 = COAP_BLOCK1_OPTION_DATA(0, 0, 16)}, // first block with 16 size and 'More' Flag not set
  };
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, "Das ist String 1");
  lwm2m_data_t expected_data = {};
  lwm2m_data_encode_opaque("Das ist String 1", strlen("Das ist String 1"), &expected_data);

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, COAP_204_CHANGED);
  expect_value(write_firmware_callback, length, strlen("Das ist String 1"));
  expect_memory(write_firmware_callback, buffer, "Das ist String 1", strlen("Das ist String 1"));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));
  lwm2m_free(expected_data.value.asBuffer.buffer);

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_204_CHANGED);

}

static void blockwise_transfer_multiple_blocks(void **state){
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0, 1, 16)}, // first block with 16 byte size and 'More' Flag set
  };
  coap_t req = {};

  /* setup expectations for all subsequent calls */
  expect_function_calls(write_firmware_callback, 3);
  expect_value_count(write_firmware_callback, length, strlen("Das ist String 1"), 3);
  expect_memory(write_firmware_callback, buffer, "Das ist String 1", strlen("Das ist String 1"));
  expect_memory(write_firmware_callback, buffer, "Das ist String 2", strlen("Das ist String 2"));
  expect_memory(write_firmware_callback, buffer, "Das ist String 3", strlen("Das ist String 3"));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_num, 1);
  expect_value(write_firmware_callback, block_num, 2);
  expect_value_count(write_firmware_callback, block_more, 1, 2);
  expect_value_count(write_firmware_callback, block_more, 0, 1);
  will_return_count(write_firmware_callback, COAP_231_CONTINUE, 2);
  will_return_count(write_firmware_callback, COAP_204_CHANGED, 1);

  /* first block transfer */
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, "Das ist String 1");
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  /* second block transfer */
  options[4].data.u8 = COAP_BLOCK1_OPTION_DATA(1, 1, 16); // middle block with 16 byte size and blocks to follow
  numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, "Das ist String 2");

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  /* third block transfer */
  options[4].data.u8 = COAP_BLOCK1_OPTION_DATA(2, 0, 16); // last block with 16 byte size and no blocks to follow
  numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, "Das ist String 3");

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_204_CHANGED);
}

static void blockwise_transfer_large_block(void **state){
  const char* str = "Das ist ein sehr langer string mit 256 Bytes. Dieser sollte nicht erfolgreich transferiert werden koennen.AAAAAAAAAAAAAAAAAAAAAA";
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0,0,128)}, // first block with 128 size and 'More' Flag not set
  };
  coap_t req = {};
  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, COAP_204_CHANGED);
  expect_value(write_firmware_callback, length, strlen(str));
  expect_memory(write_firmware_callback, buffer, str, strlen(str));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);

  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, str);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_204_CHANGED);
}

static void blockwise_transfer_multiple_large_block(void **state){
  const char* str = "Das ist ein sehr langer string mit 256 Bytes. Dieser sollte nicht erfolgreich transferiert werden koennen.AAAAAAAAAAAAAAAAAAAAAA";
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0,1,128)}, // first block with 128 size and 'More' Flag not set
  };
  coap_t req = {};
  expect_function_calls(write_firmware_callback, 2);
  will_return_count(write_firmware_callback, COAP_231_CONTINUE, 1);
  will_return_count(write_firmware_callback, COAP_204_CHANGED, 1);
  expect_value_count(write_firmware_callback, length, strlen(str), 2);
  expect_memory_count(write_firmware_callback, buffer, str, strlen(str), 2);
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_num, 1);
  expect_value(write_firmware_callback, block_more, 1);
  expect_value(write_firmware_callback, block_more, 0);

  /* first block transfer */
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, str);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  /* second block transfer */
  options[4].data.u16 = COAP_BLOCK1_OPTION_DATA(1, 0, 128);
  numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, "Das ist ein sehr langer string mit 256 Bytes. Dieser sollte nicht erfolgreich transferiert werden koennen.AAAAAAAAAAAAAAAAAAAAAA");
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

}

static void blockwise_transfer_returns_408_if_first_block_missing(void** state){
  const char* str = "Test data";
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(1,1,128)}, // first block starts with 1 instead of 0 which should fail
  };
  coap_t req = {};

  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, str);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_408_REQ_ENTITY_INCOMPLETE);
}

static void blockwise_transfer_ignore_retransmision(void** state){
  const char* str = "Test data";
  coap_option_data_t options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '5'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
    [4] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0,1,128)}, // first block starts with 1 instead of 0 which should fail
  };
  coap_t req = {};

  expect_function_calls(write_firmware_callback, 2);
  will_return_always(write_firmware_callback, COAP_231_CONTINUE);
  expect_value_count(write_firmware_callback, length, strlen(str), 2);
  expect_memory_count(write_firmware_callback, buffer, str, strlen(str), 2);
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_num, 1);
  expect_value_count(write_firmware_callback, block_more, 1, 2);

  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, str);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));
  
  options[4].data.u8 = COAP_BLOCK1_OPTION_DATA(1,1,128);
  numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, options, 5, str);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  memset(sent_message, 0, sizeof(sent_message));
  sent_bytes = 0;
  
  /* retransmission */
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  assert_int_equal(sent_bytes, 0);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test_teardown(blockwise_transfer_single_block, cleanup_block_data),
  cmocka_unit_test_teardown(blockwise_transfer_multiple_blocks, cleanup_block_data),
  cmocka_unit_test_teardown(blockwise_transfer_large_block, cleanup_block_data),
  cmocka_unit_test_teardown(blockwise_transfer_multiple_large_block, cleanup_block_data),
  cmocka_unit_test_teardown(blockwise_transfer_returns_408_if_first_block_missing, cleanup_block_data),
  cmocka_unit_test_teardown(blockwise_transfer_ignore_retransmision, cleanup_block_data),
};

DEFINE_TEST("Blockwise Transfer", tests, group_setup, group_teardown);
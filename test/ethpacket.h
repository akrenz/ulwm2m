/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
#ifndef _LWM2M_ETH_PACKET_H_
#define _LWM2M_ETH_PACKET_H_

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef enum {
  COAP_TYPE_REQ = 0,
  COAP_TYPE_NON = 1,
  COAP_TYPE_ACK = 2
} coap_type_t;

/* CoAP request method codes */
typedef enum {
  COAP_GET = 1,
  COAP_POST,
  COAP_PUT,
  COAP_DELETE
} coap_method_t;

/* CoAP header options */
typedef enum {
  COAP_OPTION_IF_MATCH = 1,       /* 0-8 B */
  COAP_OPTION_URI_HOST = 3,       /* 1-255 B */
  COAP_OPTION_ETAG = 4,           /* 1-8 B */
  COAP_OPTION_IF_NONE_MATCH = 5,  /* 0 B */
  COAP_OPTION_OBSERVE = 6,        /* 0-3 B */
  COAP_OPTION_URI_PORT = 7,       /* 0-2 B */
  COAP_OPTION_LOCATION_PATH = 8,  /* 0-255 B */
  COAP_OPTION_URI_PATH = 11,      /* 0-255 B */
  COAP_OPTION_CONTENT_TYPE = 12,  /* 0-2 B */
  COAP_OPTION_MAX_AGE = 14,       /* 0-4 B */
  COAP_OPTION_URI_QUERY = 15,     /* 0-270 B */
  COAP_OPTION_ACCEPT = 17,        /* 0-2 B */
  COAP_OPTION_TOKEN = 19,         /* 1-8 B */
  COAP_OPTION_LOCATION_QUERY = 20, /* 1-270 B */
  COAP_OPTION_BLOCK2 = 23,        /* 1-3 B */
  COAP_OPTION_BLOCK1 = 27,        /* 1-3 B */
  COAP_OPTION_SIZE = 28,          /* 0-4 B */
  COAP_OPTION_PROXY_URI = 35,     /* 1-270 B */
  OPTION_MAX_VALUE = 0xFFFF
} coap_option_id_t;

/* just used for size calculation */
typedef struct {
  uint8_t dst_mac[6];
  uint8_t src_mac[6];
  uint16_t ethertype;
#if defined(__clang__)
} __attribute__((packed)) eth_t;
#elif defined(__GNUC__) || defined(__GNUG__)
} __attribute__((packed))  eth_t;
#endif

/* just used for size calculation */
typedef struct{
  uint8_t ihl:4;  
  uint8_t version:4;
  uint8_t dscp:6;
  uint8_t ecn:2;
  uint16_t total_length;
  uint16_t identification;
  uint16_t flags_and_fragment;
  uint8_t ttl;
  uint8_t protocol;
  uint16_t checksum;
  uint32_t src;
  uint32_t dst;
#if defined(__clang__)
} __attribute__((packed)) ipv4_t;
#elif defined(__GNUC__) || defined(__GNUG__)
} __attribute__((packed)) ipv4_t;
#endif

/* just used for size calculation */
typedef struct{
  uint16_t src_port;
  uint16_t dst_port;
  uint16_t length;
  uint16_t checksum;
#if defined(__clang__)
} __attribute__((packed)) udp_t;
#elif defined(__GNUC__) || defined(__GNUG__)
} __attribute__((packed)) udp_t;
#endif

#define COAP_MAX_PAYLOAD_SIZE 1514-sizeof(eth_t) -sizeof(ipv4_t) - sizeof(udp_t) - 8
typedef struct{
  uint8_t token_length:4;
  uint8_t type:2;
  uint8_t ver:2;
  uint8_t code;
  uint16_t mid;
  uint32_t token;
  uint8_t payload[COAP_MAX_PAYLOAD_SIZE];
#if defined(__clang__)
} __attribute__((packed)) coap_t;
#elif defined(__GNUC__) || defined(__GNUG__)
} __attribute__((packed)) coap_t;
#endif

#define COAP_PAYLOAD_LENGTH(coap, numbytes) (numbytes - (1514-COAP_MAX_PAYLOAD_SIZE))

typedef struct {
  uint8_t option;
  uint8_t length;
  union{
    uint8_t u8;
    uint16_t u16;
    uint8_t* u8p;
  }data;
#if defined(__clang__)
} __attribute__((packed)) coap_option_data_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) coap_option_data_t;
#endif

static const uint8_t szx_lut[9] = 
{
  [1]   = 0,
  [2]   = 1,
  [4]   = 2,
  [8]   = 3,
};

#define COAP_BLOCK1_OPTION_DATA(num, more, szx) ((num << 4) | (more << 3) | szx_lut[(szx/16)])

#define TLV_LENGTH_IS_U8 0
#define TLV_LENGTH_IS_U16 1
#define TLV_LENGTH_IS_U24 2

#define TLV_SHORT_IDENTIFIER 0
#define TLV_LONG_IDENTIFIER 1

#define TLV_IDENTIFIER_TYPE_RESOURCE_WITH_VALUE 0x11
#define TLV_IDENTIFIER_TYPE_MULTIPLE_RESOURCE 0x10
#define TLV_IDENTIFIER_TYPE_RESOURCE_INSTANCE 0x01
#define TLV_IDENTIFIER_TYPE_OBJECT_INSTANCE 0x00


#define TLV_TYPE_VALUE_LENGTH(tlv) *((uint8_t*)tlv) & 0b00000111
#define TLV_TYPE_LENGTH_TYPE(tlv) (*((uint8_t*)tlv) & 0b00011000) >> 3
#define TLV_TYPE_IDENTIFIER_LENGTH(tlv) (*((uint8_t*)tlv) & 0b00100000) >> 5
#define TLV_TYPE_IDENTIFIER_TYPE(tlv) (*((uint8_t*)tlv) & 0b11000000) >> 6
#define TLV_IDENTIFER_SHORT(tlv) ((uint8_t*)tlv)[1] 
#define TLV_IDENTIFER_LONG(tlv) (((uint8_t*)tlv)[1] << 8) + ((uint8_t*)tlv)[2]
#define TLV_VALUE_8BIT(tlv) get_u8_tlv_value((uint8_t*)tlv)
#define TLV_VALUE_16BIT(tlv) get_u16_tlv_value((uint8_t*)tlv)

uint8_t get_u8_tlv_value(uint8_t* tlv);
uint16_t get_u16_tlv_value(uint8_t* tlv);

typedef struct {
  uint8_t value_length:3;
  uint8_t length_type:2;
  uint8_t identifier_length:1;
  uint8_t identifier_type:2;
  union{
    uint8_t asUint8;
    uint16_t asUint16;
  } identifier;
  uint8_t value[];
#if defined(__clang__)
} __attribute__((packed)) tlv_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) tlv_t;
#endif

#define TLV_VALUE_WITHOUT_LENGTH_FIELD(tlv) ((uint8_t*)tlv)[2]
size_t create_coap_req(coap_t * req, const coap_type_t type, const coap_method_t code, coap_option_data_t * options, const size_t numoptions, const char * payload);
bool coap_message_equal(coap_t* actual, coap_t* expected, size_t size);
tlv_t* get_coap_tlv(coap_t * req);
size_t create_tlv(void* buffer, const uint8_t value);
uint16_t get_mid();
#endif /* _LWM2M_ETH_PACKET_H_ */
/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "platform.h"
#include <sys/types.h>
#include <string.h>

#include <internal.h>

/* buffer to store the message sent via lwm2m_socket_send() */
uint8_t sent_message[1000] = {};
/* bytes sent via last lwm2m_socket_send() call */
size_t sent_bytes = 0;


/* mock sending method */
ssize_t lwm2m_socket_send(client_data_t * cdata, const void * data, size_t n, __CONST_SOCKADDR_ARG addr){
  memcpy(sent_message, data, n);
  sent_bytes = n;
  return n;
}

// uint8_t *bufferSendBuffer;
// size_t bufferSendBufferLength;

// uint8_t lwm2m_buffer_send(void * sessionH, uint8_t * buffer, size_t length, void * userdata){
//   bufferSendBuffer = buffer;
//   bufferSendBufferLength = length;
//   return length;
// }
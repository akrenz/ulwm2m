/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <ulwm2m.h>
#include "utest.h"

#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

// forward declarations
static uint32_t write_firmware_callback(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more);
static uint32_t execute_update_callback(void);

typedef struct{
  lwm2m_object_t obj;
}test_state_t;

#define OBJ(state) ((test_state_t*)*state)->obj

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  memset(*state, 0, sizeof(test_state_t));

  lwm2m_init_object(&OBJ(state), LWM2M_FIRMWARE_UPDATE_OBJECT_ID, lwm2m_firmware_update_read, NULL, lwm2m_firmware_update_execute, NULL, NULL, NULL, lwm2m_firmware_update_write, NULL, NULL);
  lwm2m_firmware_update_instance_data_t * data = lwm2m_calloc(1, sizeof(lwm2m_firmware_update_instance_data_t));
  data->write_firmware = write_firmware_callback;
  data->execute_update = execute_update_callback;
  lwm2m_instance_t * inst = lwm2m_create_instance(0, data, NULL, true);
  lwm2m_object_add_instance(&OBJ(state), inst);

  return 0;
}

static int group_teardown(void **state){
  lwm2m_clear_object(&OBJ(state));

  lwm2m_free(*state);
  return 0;
}

static int reset_state(void **state){
  lwm2m_instance_t* inst = (lwm2m_instance_t*)OBJ(state).instanceList;
  lwm2m_firmware_update_instance_data_t* data = (lwm2m_firmware_update_instance_data_t*)inst->data;
  data->state = 0;
  return 0;
}

/** mock write firmware callback */
static uint32_t write_firmware_callback(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more){
  check_expected(length);
  check_expected(buffer);
  check_expected(block_num);
  check_expected(block_more);
  
  function_called();
  return (uint8_t)mock();
}

static uint32_t execute_update_callback(void){
  function_called();

  return (uint32_t)mock();
}

static void create_instance_statically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_FIRMWARE_UPDATE_OBJECT_ID, lwm2m_firmware_update_read, NULL, lwm2m_firmware_update_execute, NULL, NULL, NULL, lwm2m_firmware_update_write, NULL, NULL);
  lwm2m_firmware_update_instance_data_t data = {};
  lwm2m_instance_t * inst = lwm2m_create_instance(0, &data, NULL, false);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void create_instance_dynamically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_FIRMWARE_UPDATE_OBJECT_ID, lwm2m_firmware_update_read, NULL, lwm2m_firmware_update_execute, NULL, NULL, NULL, lwm2m_firmware_update_write, NULL, NULL);
  lwm2m_firmware_update_instance_data_t * data = lwm2m_calloc(1, sizeof(lwm2m_firmware_update_instance_data_t));
  lwm2m_instance_t * inst = lwm2m_create_instance(0, data, NULL, true);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void read_returns_not_found_error_if_instance_not_found(void **state){
  lwm2m_data_t * data = NULL;
  int num_data = 0;
  uint8_t ret = lwm2m_firmware_update_read(1, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
}

static void state_is_idle_before_downloading(void **state){
  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_IDLE);
}

static void write_to_package_node_sets_state_to_downloading(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 1);

  uint8_t ret = lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 1);

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  assert_int_equal(ret, COAP_231_CONTINUE);
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_DOWNLOADING);
}

static void write_last_to_package_node_sets_state_to_downloaded(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);

  uint8_t ret = lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 0);

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_DOWNLOADED);
}

static void execute_update_node_fails_if_state_not_downloaded(void **state){
  uint8_t ret = lwm2m_firmware_update_execute(0, 2, NULL, 0, &OBJ(state));

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  assert_int_equal(ret, COAP_400_BAD_REQUEST);
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_IDLE);
}

static void execute_update_node_succeeds_if_state_downloaded(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);

  lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 0);

  expect_function_call(execute_update_callback);
  will_return(execute_update_callback, 0);
  uint8_t ret = lwm2m_firmware_update_execute(0, 2, NULL, 0, &OBJ(state));

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;

  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_UPDATING);
}

static void execute_update_fails(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);

  lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 0);

  expect_function_call(execute_update_callback);
  will_return(execute_update_callback, 1);
  uint8_t ret = lwm2m_firmware_update_execute(0, 2, NULL, 0, &OBJ(state));

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;

  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(inst_data->state, LWM2M_FWUP_STATE_IDLE);
}

static void execute_update_fails_if_data_not_empty(void **state){
  uint8_t data[] = {1,2,3,4,5};

  int ret = lwm2m_firmware_update_execute(0, 2, data, sizeof(data), &OBJ(state));

  assert_int_equal(ret, COAP_400_BAD_REQUEST);
}

static void execute_update_accepts_only_resource_id_2(void **state){
  uint8_t data[] = {1,2,3,4,5};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);
  lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 0);


  uint8_t ret = lwm2m_firmware_update_execute(0, 1, NULL, 0, &OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  ret = lwm2m_firmware_update_execute(0, 0, NULL, 0, &OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
}

static void firmware_update_delivery_method_is_push_only(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 9;
  int num_data = 1;

  lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(data->type, LWM2M_TYPE_INTEGER);
  assert_int_equal(data->value.asInteger, 1);

  lwm2m_data_free(1, data);
}

static void read_package_nodeid_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 0;
  int num_data = 1;

  int ret = lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  lwm2m_data_free(1, data);
}

static void read_update_nodeid_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 2;
  int num_data = 1;

  int ret = lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  lwm2m_data_free(1, data);
}

static void read_all_nodeids(void **state){
  lwm2m_data_t * data = NULL;
  int num_data = 0;
  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  inst_data->result = 2;
  strncpy(inst_data->uri, "coap://test.domain.org", strlen("coap://test.domain.org")+1);
  inst_data->pkg_name ="firmware";
  inst_data->pkg_version = "v0.0.1";

  int ret = lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(num_data, 6);
  const uint64_t ids[] = {1,3,5,6,7,9};
  assert_in_set(data[0].id, ids, sizeof(ids)/sizeof(uint64_t));
  assert_in_set(data[1].id, ids, sizeof(ids)/sizeof(uint64_t));
  assert_in_set(data[2].id, ids, sizeof(ids)/sizeof(uint64_t));
  assert_in_set(data[3].id, ids, sizeof(ids)/sizeof(uint64_t));
  assert_in_set(data[4].id, ids, sizeof(ids)/sizeof(uint64_t));
  assert_in_set(data[5].id, ids, sizeof(ids)/sizeof(uint64_t));

  lwm2m_data_free(num_data, data);
}

static void read_state(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 3;
  int num_data = 1;
  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  inst_data->state = 2;

  int ret = lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(data->value.asInteger, 2);

  lwm2m_data_free(1, data);
}

static void read_result(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 3;
  int num_data = 1;
  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  inst_data->state = 2;

  int ret = lwm2m_firmware_update_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(data->value.asInteger, 2);

  lwm2m_data_free(1, data);
}

static void result_is_set_to_zero_when_download_started(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 1);

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  inst_data->result = 9;

  lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 1);

  assert_int_equal(inst_data->result, 0);
}

static void result_is_set_to_failed_when_update_fails(void **state){
  uint8_t data[10] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
  lwm2m_uri_t uri = {.instanceId = 0, .objectId = 5, .resourceId = 0};

  expect_function_call(write_firmware_callback);
  will_return(write_firmware_callback, 0);
  expect_value(write_firmware_callback, length, sizeof(data));
  expect_memory(write_firmware_callback, buffer, data, sizeof(data));
  expect_value(write_firmware_callback, block_num, 0);
  expect_value(write_firmware_callback, block_more, 0);
  lwm2m_firmware_update_write(&uri, LWM2M_CONTENT_OPAQUE, data, sizeof(data), &OBJ(state), 0, 0);
  
  expect_function_call(execute_update_callback);
  will_return(execute_update_callback, 1);
  int ret = lwm2m_firmware_update_execute(0, 2, NULL, 0, &OBJ(state));

  lwm2m_object_t  * obj = &OBJ(state);
  lwm2m_firmware_update_instance_data_t * inst_data = ((lwm2m_instance_t*)LWM2M_LIST_FIND(obj->instanceList, 0))->data;
  assert_int_equal(ret, COAP_204_CHANGED);
  assert_int_equal(inst_data->result, 8);
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(create_instance_statically),
  cmocka_unit_test(create_instance_dynamically),
  cmocka_unit_test(read_returns_not_found_error_if_instance_not_found),
  cmocka_unit_test(state_is_idle_before_downloading),
  cmocka_unit_test(write_to_package_node_sets_state_to_downloading),
  cmocka_unit_test(write_last_to_package_node_sets_state_to_downloaded),
  cmocka_unit_test_setup(write_last_to_package_node_sets_state_to_downloaded, reset_state),
  cmocka_unit_test_setup(execute_update_node_fails_if_state_not_downloaded, reset_state),
  cmocka_unit_test_setup(execute_update_node_succeeds_if_state_downloaded, reset_state),
  cmocka_unit_test(execute_update_fails),
  cmocka_unit_test(execute_update_fails_if_data_not_empty),
  cmocka_unit_test(execute_update_accepts_only_resource_id_2),
  cmocka_unit_test(firmware_update_delivery_method_is_push_only),
  cmocka_unit_test(read_package_nodeid_fails),
  cmocka_unit_test(read_update_nodeid_fails),
  cmocka_unit_test(read_all_nodeids),
  cmocka_unit_test(read_state),
  cmocka_unit_test(read_result),
  cmocka_unit_test(result_is_set_to_zero_when_download_started),
  cmocka_unit_test(result_is_set_to_failed_when_update_fails),
};
  
DEFINE_TEST("Object Firmware Update", tests, group_setup, group_teardown);
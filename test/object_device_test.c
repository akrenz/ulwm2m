/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <ulwm2m.h>
#include "utest.h"

#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>


typedef struct{
  lwm2m_object_t obj;
}test_state_t;

#define DEVICE_OBJ(state) ((test_state_t*)*state)->obj

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  lwm2m_device_instance_data_t * device = lwm2m_malloc(sizeof(lwm2m_device_instance_data_t));
  memset(device, 0, sizeof(lwm2m_device_instance_data_t));

  lwm2m_init_object(&DEVICE_OBJ(state), LWM2M_DEVICE_OBJECT_ID, lwm2m_device_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_instance_t * inst = lwm2m_create_instance(0, device, NULL, true);
  lwm2m_object_add_instance(&DEVICE_OBJ(state), inst);

  return 0;
}

static int group_teardown(void **state){
  lwm2m_clear_object(&DEVICE_OBJ(state));

  lwm2m_free(*state);
  return 0;
}

static int clean_all_data(void ** state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  memset(data, 0, sizeof(lwm2m_device_instance_data_t));
  data->availablePowerSource = LWM2M_DEVICE_POWER_SOURCE_NONE;

  return 0;
}

static void create_instance_statically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_DEVICE_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t data = {};
  lwm2m_instance_t * inst = lwm2m_create_instance(0, &data, NULL, false);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void create_instance_dynamically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_DEVICE_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t * data = lwm2m_calloc(1, sizeof(lwm2m_device_instance_data_t));
  lwm2m_instance_t * inst = lwm2m_create_instance(0, data, NULL, true);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

#define READ_STRING_TEST(_id, _value, _testname) static void _testname(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList , 0);\
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;\
  data->_value = "this_is_a_test_string";\
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=_id};\
  lwm2m_data_t * p = &requestedData;\
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(requestedData.type, LWM2M_TYPE_STRING);\
  assert_memory_equal(data->_value, requestedData.value.asBuffer.buffer, requestedData.value.asBuffer.length);\
  lwm2m_free(requestedData.value.asBuffer.buffer);\
}

#define READ_STRING_FAILS_TEST(_id, _testname) static void _testname(void **state){\
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=_id};\
  lwm2m_data_t * p = &requestedData;\
  uint8_t ret = lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(ret, COAP_404_NOT_FOUND);\
}

READ_STRING_TEST(LWM2M_DEVICE_MANUFACTURER_ID, manufacturer, read_manufacturer);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_MANUFACTURER_ID, read_manufacturer_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_MODEL_NUMBER_ID, model, read_model_number);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_MODEL_NUMBER_ID, read_model_number_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_SERIAL_NUMBER_ID, serial, read_serial);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_SERIAL_NUMBER_ID, read_serial_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_FIRMWARE_VERSION_ID, firmware, read_firmware_version);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_FIRMWARE_VERSION_ID, read_firmware_version_returns_not_found_if_not_set);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_UTC_OFFSET_ID, read_utc_offset_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_TIMEZONE_ID, timezone, read_timezone);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_TIMEZONE_ID, read_timezone_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_DEVICE_TYPE_ID, deviceType, read_device_type);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_DEVICE_TYPE_ID, read_device_type_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_HARDWARE_VERSION_ID, hardwareVersion, read_hardware_version);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_HARDWARE_VERSION_ID, read_hardware_version_returns_not_found_if_not_set);
READ_STRING_TEST(LWM2M_DEVICE_SOFTWARE_VERSION_ID, softwareVersion, read_software_version);
READ_STRING_FAILS_TEST(LWM2M_DEVICE_SOFTWARE_VERSION_ID, read_software_version_returns_not_found_if_not_set);

static void read_utc_offset(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList , 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  strncpy(data->utcOffset, "+01:00", 7);
  int numData = 1;
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_UTC_OFFSET_ID};
  lwm2m_data_t * p = &requestedData;
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));
  assert_int_equal(requestedData.type, LWM2M_TYPE_STRING);
  assert_memory_equal(data->utcOffset, requestedData.value.asBuffer.buffer, requestedData.value.asBuffer.length);
  lwm2m_free(requestedData.value.asBuffer.buffer);
}

static void read_reboot_id_returns_method_not_allowed(void **state){
  int numData = 1;
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_REBOOT_ID};
  lwm2m_data_t * p = &requestedData;
  uint8_t ret = lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED); 
}

static void read_factory_reset_returns_method_not_allowed(void **state){
  int numData = 1;
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_FACTORY_RESET_ID};
  lwm2m_data_t * p = &requestedData;
  uint8_t ret = lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED); 
}

#define READ_INTEGER_TEST(_id, _value, _testname) static void _testname(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList , 0);\
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;\
  data->_value = 15;\
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=_id};\
  lwm2m_data_t * p = &requestedData;\
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(requestedData.type, LWM2M_TYPE_INTEGER);\
  assert_int_equal(requestedData.value.asInteger, data->_value);\
}

READ_INTEGER_TEST(LWM2M_DEVICE_AVL_POWER_SOURCES_ID, availablePowerSource, read_available_power_sources);
READ_INTEGER_TEST(LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID, powerSourceVoltage, read_power_source_voltages);
READ_INTEGER_TEST(LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID, powerSourceCurrent, read_power_source_currents);
READ_INTEGER_TEST(LWM2M_DEVICE_MEMORY_TOTAL_ID, totalMemory, read_total_memory);

#define READ_BINDING_MODE(_mode, _string) static void read_binding_mode_##_mode(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList , 0);\
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;\
  data->binding = _mode;\
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_BINDING_MODES_ID};\
  lwm2m_data_t * p = &requestedData;\
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(requestedData.type, LWM2M_TYPE_STRING);\
  assert_memory_equal(_string, requestedData.value.asBuffer.buffer, requestedData.value.asBuffer.length); \
  \
  lwm2m_free(requestedData.value.asBuffer.buffer);\
}

READ_BINDING_MODE(BINDING_U, "U");
READ_BINDING_MODE(BINDING_UQ, "UQ");
READ_BINDING_MODE(BINDING_UQS, "UQS");
READ_BINDING_MODE(BINDING_US, "US");
READ_BINDING_MODE(BINDING_S, "S");
READ_BINDING_MODE(BINDING_SQ, "SQ");

#define READ_CALLBACK_METHOD(_id, _callback, _value, _testname) static void _testname(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);\
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;\
  data->_value = _callback;\
  \
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=_id};\
  lwm2m_data_t * p = &requestedData;\
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(requestedData.type, LWM2M_TYPE_INTEGER);\
  assert_int_equal(requestedData.value.asInteger, _callback());\
}

int8_t int8_callback(void){ return 80;}
int32_t int32_callback(void){ return 1000;}
READ_CALLBACK_METHOD(LWM2M_DEVICE_BATTERY_LEVEL_ID, int8_callback, batteryLevelCallback, read_battery_level);
READ_CALLBACK_METHOD(LWM2M_DEVICE_MEMORY_FREE_ID, int32_callback, freeMemorySpaceCallback, read_free_memory_space);
READ_CALLBACK_METHOD(LWM2M_DEVICE_CURRENT_TIME_ID, int32_callback, currentTimeReadCallback, read_current_time);

#define READ_CALLBACK_METHOD_FAILS(_id, _testname) static void _testname(void **state){\
  int numData = 1;\
  lwm2m_data_t requestedData = {.id=_id};\
  lwm2m_data_t * p = &requestedData;\
  uint8_t ret = lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));\
  assert_int_equal(ret, COAP_404_NOT_FOUND);\
}
READ_CALLBACK_METHOD_FAILS(LWM2M_DEVICE_BATTERY_LEVEL_ID, read_battery_level_fails_if_no_callback);
READ_CALLBACK_METHOD_FAILS(LWM2M_DEVICE_MEMORY_FREE_ID, read_free_memory_space_fails_if_no_callback);
READ_CALLBACK_METHOD_FAILS(LWM2M_DEVICE_CURRENT_TIME_ID, read_current_time_fails_if_no_callback);

lwm2m_device_battery_status battery_status_test(void){return LWM2M_DEVICE_BATTERY_STATUS_LOW_BATTERY;};
static void read_battery_status(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  data->batteryStatusCallback = battery_status_test;
  
  int numData = 1;
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_BATTERY_STATUS_ID};
  lwm2m_data_t * p = &requestedData;
  lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));
  assert_int_equal(requestedData.type, LWM2M_TYPE_INTEGER);
  assert_int_equal(requestedData.value.asInteger, battery_status_test());
}

static void read_battery_status_returns_not_found_if_not_defined(void **state){

  int numData = 1;
  lwm2m_data_t requestedData = {.id=LWM2M_DEVICE_BATTERY_STATUS_ID};
  lwm2m_data_t * p = &requestedData;
  uint8_t ret = lwm2m_device_read(0, &numData, &p, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_404_NOT_FOUND);
}

int32_t write_time(int32_t val){
  check_expected(val);

  function_called();
  return mock_type(int32_t);
}

static void write_current_time(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = inst->data;
  data->currentTimeWriteCallback = write_time;

  int numData = 1;
  lwm2m_data_t writeData = {.id=LWM2M_DEVICE_CURRENT_TIME_ID, .type=LWM2M_TYPE_INTEGER, .value.asInteger=10};
  expect_function_call(write_time);
  expect_value(write_time, val, 10);
  will_return(write_time, 10);
  uint8_t ret = lwm2m_device_write(0, numData, &writeData, &DEVICE_OBJ(state));

  assert_int_equal(ret, COAP_204_CHANGED);
}

static void write_utc_offset(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = inst->data;
  int numData = 1;
  lwm2m_data_t writeData = {.id=LWM2M_DEVICE_UTC_OFFSET_ID, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer="+01:00", .value.asBuffer.length=strlen("+01:00")};

  uint8_t ret = lwm2m_device_write(0, numData, &writeData, &DEVICE_OBJ(state));

  assert_int_equal(ret, COAP_204_CHANGED);
  assert_string_equal(data->utcOffset, "+01:00");
}

#define WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(_resource, _id) static void write_read_only_ressource_ ##_resource##_returns_not_allowed(void **state){\
  int numData = 1;\
  char string[] = {"New Manufacturer"};\
  lwm2m_data_t writeData = {.id=_id, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer = string, .value.asBuffer.length = sizeof(string)};\
  \
  uint8_t ret = lwm2m_device_write(0, numData, &writeData, &DEVICE_OBJ(state));\
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);\
  \
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);\
  lwm2m_device_instance_data_t * data = inst->data;\
  assert_null(data->_resource);\
}
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(manufacturer, LWM2M_DEVICE_MANUFACTURER_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(model, LWM2M_DEVICE_MODEL_NUMBER_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(serial, LWM2M_DEVICE_SERIAL_NUMBER_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(firmware, LWM2M_DEVICE_FIRMWARE_VERSION_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(rebootCallback, LWM2M_DEVICE_REBOOT_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(factoryResetCallback, LWM2M_DEVICE_FACTORY_RESET_ID);
// WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(availablePowerSource, LWM2M_DEVICE_AVL_POWER_SOURCES_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(powerSourceVoltage, LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(powerSourceCurrent, LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(batteryLevelCallback, LWM2M_DEVICE_BATTERY_LEVEL_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(freeMemorySpaceCallback, LWM2M_DEVICE_MEMORY_FREE_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(totalMemory, LWM2M_DEVICE_MEMORY_TOTAL_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(binding, LWM2M_DEVICE_BINDING_MODES_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(deviceType, LWM2M_DEVICE_DEVICE_TYPE_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(hardwareVersion, LWM2M_DEVICE_HARDWARE_VERSION_ID);
WRITING_READ_ONLY_RESOURCE_RETURNS_NOT_ALLOWED(softwareVersion, LWM2M_DEVICE_SOFTWARE_VERSION_ID);

static void write_read_only_ressource_availablePowerSource_returns_not_allowed(void** state){
  int numData = 1;
  lwm2m_data_t writeData = {.id=LWM2M_DEVICE_AVL_POWER_SOURCES_ID, .type=LWM2M_TYPE_INTEGER, .value.asInteger=LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_AC};
  
  uint8_t ret = lwm2m_device_write(0, numData, &writeData, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
  
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = inst->data;
  assert_int_equal(data->availablePowerSource, LWM2M_DEVICE_POWER_SOURCE_NONE);
}

int8_t battery_level_callback(void){return 80;}
int32_t free_memory_callback(void){return 80;}
int32_t current_time_callback(void){return 80;}
lwm2m_device_battery_status battery_status_callback(void){ return LWM2M_DEVICE_BATTERY_STATUS_CHARGING;}
static void read_all_ressources(void** state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = inst->data;
  data->manufacturer = "Manufacturer";
  data->firmware = "firmware";
  data->model = "Model 123";
  data->serial = "123";
  data->availablePowerSource = LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_BAT;
  data->powerSourceCurrent = 10;
  data->powerSourceVoltage = 10;
  data->binding = BINDING_UQ;
  data->totalMemory = 100;
  data->batteryLevelCallback = battery_level_callback;
  data->freeMemorySpaceCallback = free_memory_callback;
  data->currentTimeReadCallback = current_time_callback;
  strncpy(data->utcOffset, "+01:00", sizeof(data->utcOffset));
  data->timezone = "UTC";
  data->deviceType = "device type";
  data->hardwareVersion = "hardware version";
  data->softwareVersion = "software version";
  data->batteryStatusCallback = battery_status_callback;
  int numData = 0;
  lwm2m_data_t * resources = NULL;
  
  uint8_t ret = lwm2m_device_read(0, &numData, &resources, &DEVICE_OBJ(state));

  for(int i=0;i<numData;i++){
    lwm2m_free(resources[i].value.asBuffer.buffer);
  }
  lwm2m_free(resources);

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(numData, 18);
}

void void_callback(void){

  function_called();
}
#define CALL_EXECUTABLE_RESOURCE(_id, _ressource) static void execute_##_ressource(void **state){\
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);\
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;\
  data->_ressource = void_callback;\
  \
  expect_function_call(void_callback);\
  uint8_t ret = lwm2m_device_execute(0, _id, NULL, 0, &DEVICE_OBJ(state));\
  \
  assert_int_equal(ret, COAP_204_CHANGED);\
}

CALL_EXECUTABLE_RESOURCE(LWM2M_DEVICE_REBOOT_ID, rebootCallback);
CALL_EXECUTABLE_RESOURCE(LWM2M_DEVICE_FACTORY_RESET_ID, factoryResetCallback);

#define CALL_EXECUTABLE_RESOURCE_NOT_FOUND_IF_NOT_SET(_id, _resource) static void execute_##_resource##_returns_not_found_if_not_set(void **state){\
  uint8_t ret = lwm2m_device_execute(0, _id, NULL, 0, &DEVICE_OBJ(state));\
  assert_int_equal(ret, COAP_404_NOT_FOUND);\
}
CALL_EXECUTABLE_RESOURCE_NOT_FOUND_IF_NOT_SET(LWM2M_DEVICE_REBOOT_ID, rebootCallback);
CALL_EXECUTABLE_RESOURCE_NOT_FOUND_IF_NOT_SET(LWM2M_DEVICE_FACTORY_RESET_ID, factoryResetCallback);

static void execute_read_or_write_resource_returns_not_allowed(void **state){
  uint8_t ret = lwm2m_device_execute(0, LWM2M_DEVICE_MANUFACTURER_ID, NULL, 0, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  ret = lwm2m_device_execute(0, LWM2M_DEVICE_BATTERY_LEVEL_ID, NULL, 0, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);
}

static void discover_returns_num_of_set_ids(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  char string[] = {"Buf String"};
  data->manufacturer = string;
  data->model = string;
  data->serial = string;
  data->firmware = string;
  data->timezone = string;
  data->deviceType = string;
  data->hardwareVersion = string;
  data->softwareVersion = string;

  int num = 0;
  lwm2m_data_t * results = NULL;
  uint8_t ret = lwm2m_device_discover(0, &num, &results, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(num, 8);

  lwm2m_free(results);
}

static void discover_returns_all_available_ids(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  char string[] = {"Buf String"};
  data->manufacturer = string;
  data->model = string;
  data->serial = string;
  data->firmware = string;
  data->timezone = string;
  data->deviceType = string;
  data->hardwareVersion = string;
  data->softwareVersion = string;

  int num = 0;
  lwm2m_data_t * results = NULL;
  lwm2m_device_discover(0, &num, &results, &DEVICE_OBJ(state));
  LargestIntegralType ids[23] = {};
  for(int i=0;i<num;i++) ids[i] = results[i].id;
  assert_in_set(LWM2M_DEVICE_MANUFACTURER_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_MODEL_NUMBER_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_SERIAL_NUMBER_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_FIRMWARE_VERSION_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_TIMEZONE_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_DEVICE_TYPE_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_HARDWARE_VERSION_ID, ids, num);
  assert_in_set(LWM2M_DEVICE_SOFTWARE_VERSION_ID, ids, num);

  lwm2m_free(results);
}

static void discover_specified_ids(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  char string[] = {"Buf String"};
  data->manufacturer = string;
  data->model = string;
  data->serial = string;

  int num = 3;
  lwm2m_data_t * results = lwm2m_data_new(3);
  results[0].id = LWM2M_DEVICE_MANUFACTURER_ID;
  results[1].id = LWM2M_DEVICE_MODEL_NUMBER_ID;
  results[2].id = LWM2M_DEVICE_SERIAL_NUMBER_ID;
  uint8_t ret = lwm2m_device_discover(0, &num, &results, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_205_CONTENT);

  lwm2m_free(results);
}

static void discover_returns_not_found_if_instance_unequal_0(void **state){
  int num = 3;
  lwm2m_data_t * results = NULL;
  uint8_t ret = lwm2m_device_discover(1, &num, &results, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_404_NOT_FOUND);

}

static void discover_returns_not_found_if_asking_for_not_available_resource(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(DEVICE_OBJ(state).instanceList, 0);
  lwm2m_device_instance_data_t * data = (lwm2m_device_instance_data_t*)inst->data;
  char string[] = {"Buf String"};
  data->manufacturer = string;
  data->model = string;
  data->serial = string;

  int num = 1;
  lwm2m_data_t * results = lwm2m_data_new(1);
  results[0].id = LWM2M_DEVICE_FIRMWARE_VERSION_ID;
  uint8_t ret = lwm2m_device_discover(0, &num, &results, &DEVICE_OBJ(state));
  assert_int_equal(ret, COAP_404_NOT_FOUND);

  lwm2m_free(results);
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(create_instance_statically),
  cmocka_unit_test(create_instance_dynamically),
  cmocka_unit_test(read_manufacturer),
  cmocka_unit_test_setup(read_manufacturer_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_model_number),
  cmocka_unit_test_setup(read_model_number_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_serial),
  cmocka_unit_test_setup(read_serial_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_firmware_version),
  cmocka_unit_test_setup(read_firmware_version_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_reboot_id_returns_method_not_allowed),
  cmocka_unit_test(read_factory_reset_returns_method_not_allowed),
  cmocka_unit_test(read_available_power_sources),
  cmocka_unit_test(read_power_source_voltages),
  cmocka_unit_test(read_power_source_currents),
  cmocka_unit_test(read_binding_mode_BINDING_U),
  cmocka_unit_test(read_binding_mode_BINDING_UQ),
  cmocka_unit_test(read_binding_mode_BINDING_UQS),
  cmocka_unit_test(read_binding_mode_BINDING_US),
  cmocka_unit_test(read_binding_mode_BINDING_S),
  cmocka_unit_test(read_binding_mode_BINDING_SQ),
  cmocka_unit_test(read_battery_level),
  cmocka_unit_test_setup(read_battery_level_fails_if_no_callback, clean_all_data),
  cmocka_unit_test(read_free_memory_space),
  cmocka_unit_test_setup(read_free_memory_space_fails_if_no_callback, clean_all_data),
  cmocka_unit_test(read_current_time),
  cmocka_unit_test_setup(read_current_time_fails_if_no_callback, clean_all_data),
  cmocka_unit_test(read_utc_offset),
  cmocka_unit_test_setup(read_utc_offset_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_timezone),
  cmocka_unit_test_setup(read_timezone_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_device_type),
  cmocka_unit_test_setup(read_device_type_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_hardware_version),
  cmocka_unit_test_setup(read_hardware_version_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_software_version),
  cmocka_unit_test_setup(read_software_version_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test(read_battery_status),
  cmocka_unit_test_setup(read_battery_status_returns_not_found_if_not_defined, clean_all_data),
  cmocka_unit_test(read_total_memory),
  cmocka_unit_test_setup(read_all_ressources, clean_all_data),

  /* write tests */
  cmocka_unit_test(write_current_time),
  cmocka_unit_test(write_utc_offset),
  cmocka_unit_test_setup(write_read_only_ressource_manufacturer_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_model_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_serial_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_firmware_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_rebootCallback_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_factoryResetCallback_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_availablePowerSource_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_powerSourceVoltage_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_powerSourceCurrent_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_binding_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_batteryLevelCallback_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_totalMemory_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_freeMemorySpaceCallback_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_deviceType_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_hardwareVersion_returns_not_allowed, clean_all_data),
  cmocka_unit_test_setup(write_read_only_ressource_softwareVersion_returns_not_allowed, clean_all_data),

  /* execute tests */
  cmocka_unit_test_setup(execute_rebootCallback, clean_all_data),
  cmocka_unit_test_setup(execute_rebootCallback_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test_setup(execute_factoryResetCallback, clean_all_data),
  cmocka_unit_test_setup(execute_factoryResetCallback_returns_not_found_if_not_set, clean_all_data),
  cmocka_unit_test_setup(execute_read_or_write_resource_returns_not_allowed, clean_all_data),

  /* discover unit tests */
  cmocka_unit_test_setup(discover_returns_num_of_set_ids, clean_all_data),
  cmocka_unit_test_setup(discover_returns_all_available_ids, clean_all_data),
  cmocka_unit_test_setup(discover_specified_ids, clean_all_data),
  cmocka_unit_test_setup(discover_returns_not_found_if_asking_for_not_available_resource, clean_all_data),
  cmocka_unit_test_setup(discover_returns_not_found_if_instance_unequal_0, clean_all_data),
};

DEFINE_TEST("Object Device", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <ulwm2m.h>
#include "utest.h"

#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>


typedef struct{
  lwm2m_object_t obj;
}test_state_t;

/**
 * Write Callback helper functions/variables
*/
static bool write_callback_data = false;
static bool write_callback_called = false;

uint8_t writeCallback(bool d){
  write_callback_called = true;
  write_callback_data = d;

  return 0;
}

/**
 * Read Callback helper functions/variables
*/
static bool read_callback_called = false;
static bool readOutputCallback(void){
  read_callback_called = true;
  return true;
}

static int callbackSetup(void **state) {
  write_callback_data = false;
  write_callback_called = false;
  read_callback_called = false;

  return 0;
}

#define OBJ(state) ((test_state_t*)*state)->obj

static lwm2m_data_t* find_data_object(lwm2m_data_t* data, uint32_t num_data, uint32_t id){
  for(int i=0; i<num_data;i++){
      if(data[i].id == id){
        return &data[i];
      }
    }

  return NULL;
}

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  lwm2m_digital_output_instance_data_t * output = lwm2m_malloc(sizeof(lwm2m_digital_output_instance_data_t));
  memset(output, 0, sizeof(lwm2m_digital_output_instance_data_t));
  output->writeOutput = writeCallback;
  output->readOutput = readOutputCallback;

  lwm2m_init_object(&OBJ(state), LWM2M_DIGITAL_OUTPUT_OBJECT_ID, NULL, lwm2m_digital_output_write, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_instance_t * inst = lwm2m_create_instance(0, output, NULL, true);
  lwm2m_object_add_instance(&OBJ(state), inst);

  write_callback_called = false;

  return 0;
}

static int group_teardown(void **state){
  lwm2m_clear_object(&OBJ(state));

  lwm2m_free(*state);
  return 0;
}

static void create_instance_statically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_DIGITAL_OUTPUT_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t data = {};
  lwm2m_instance_t * inst = lwm2m_create_instance(0, &data, NULL, false);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void create_instance_dynamically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_DIGITAL_OUTPUT_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t * data = lwm2m_calloc(1, sizeof(lwm2m_device_instance_data_t));
  lwm2m_instance_t * inst = lwm2m_create_instance(0, data, NULL, true);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void write_output_callback_gets_called(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true};

  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_true(write_callback_called);
}

static void write_output_callback_gets_correct_data(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true};

  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_true(write_callback_data);
}

static void write_output_callback_gets_correct_data2(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = false};

  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_false(write_callback_data);
}

static void write_output_data_as_string(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer ="1", .value.asBuffer.length= 1};

  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_true(write_callback_data);
}

static void write_output_data_as_string2(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer ="0", .value.asBuffer.length= 1};

  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_false(write_callback_data);
}

static void write_output_write_unknown_id_fails(void **state){
  lwm2m_data_t writeData = {.id=1234, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true};

  uint8_t ret = lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
}

static void read_output_callback_gets_called(void **state){
  int numData = 0;
  lwm2m_data_t * data;
  lwm2m_digital_output_read(0, &numData, &data, &OBJ(state));

  assert_true(read_callback_called);
  lwm2m_data_free(numData, data);
}

static void read_output_callback_returns_output_state(void **state){
  int numData = 0;
  lwm2m_data_t * data;
  lwm2m_digital_output_read(0, &numData, &data, &OBJ(state));

  assert_true(data[0].value.asBoolean);
  lwm2m_data_free(numData, data);
}

static void read_output_callback_returns_correct_object_id(void **state){
  int numData = 0;
  lwm2m_data_t * data;
  lwm2m_digital_output_read(0, &numData, &data, &OBJ(state));

  assert_int_equal(data[0].id, LWM2M_DIGITAL_OUTPUT_STATE_ID);
  lwm2m_data_free(numData, data);
}

static void read_output_callback_single_obj(void **state){
  int numData = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = LWM2M_DIGITAL_OUTPUT_STATE_ID;
  
  lwm2m_digital_output_read(0, &numData, &data, &OBJ(state));

  assert_true(data[0].value.asBoolean);
  lwm2m_data_free(numData, data);
}

static void read_output_callback_all_obj(void **state){
  int numData = 0;
  lwm2m_data_t * data = NULL;
  
  lwm2m_digital_output_read(0, &numData, &data, &OBJ(state));

  assert_int_equal(numData, 2);
  lwm2m_data_free(numData, data);
}

static void read_invalid_object_fails(void **state){
  int num_data = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id=1234;
  data->type=LWM2M_TYPE_BOOLEAN;
  
  uint8_t ret = lwm2m_digital_output_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  lwm2m_data_free(num_data, data);
}

static void read_invalid_instance_fails(void **state){
  int num_data = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID;
  data->type=LWM2M_TYPE_BOOLEAN;
  
  uint8_t ret = lwm2m_digital_output_read(1, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  lwm2m_data_free(num_data, data);
}

static void write_invalid_instance_fails(void **state){
  int num_data = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID;
  data->type=LWM2M_TYPE_BOOLEAN;
  data->value.asBoolean = true;
  
  uint8_t ret = lwm2m_digital_output_write(1, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  lwm2m_data_free(num_data, data);
}

static void write_polarity_succeeds(void **state){
  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true};

  uint8_t ret = lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_int_equal(ret, COAP_NO_ERROR);
}

static void write_polarity_check_value(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true};
  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_true(data->polarity);
}

static void write_polarity_as_bool_check_value(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer = "1", .value.asBuffer.length=1};
  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_true(data->polarity);
}

static void write_polarity_as_bool_check_value2(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_STRING, .value.asBuffer.buffer = "0", .value.asBuffer.length=1};
  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_false(data->polarity);
}

static void write_polarity_check_value2(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  lwm2m_data_t writeData = {.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = false};
  lwm2m_digital_output_write(0, 1, &writeData, &OBJ(state));

  assert_false(data->polarity);
}

static void write_output_with_inverse_polarity(void **state){
  lwm2m_data_t writeData[2] = {{.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true},
                               {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true}};
  lwm2m_digital_output_write(0, 2, writeData, &OBJ(state));

  assert_false(write_callback_data);
}

static void write_output_with_inverse_polarity2(void **state){
  lwm2m_data_t writeData[2] = {{.id=LWM2M_DIGITAL_OUTPUT_POLARITY_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = true},
                               {.id=LWM2M_DIGITAL_OUTPUT_STATE_ID, .type=LWM2M_TYPE_BOOLEAN, .value.asBoolean = false}};
  lwm2m_digital_output_write(0, 2, writeData, &OBJ(state));

  assert_true(write_callback_data);
}

static void read_type(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * inst_data = (lwm2m_digital_output_instance_data_t*)inst->data;
  inst_data->type = "OUTPUT_TYPE";
  int num_data = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id=LWM2M_DIGITAL_OUTPUT_TYPE_ID;
  
  uint8_t ret = lwm2m_digital_output_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_memory_equal(data->value.asBuffer.buffer, "OUTPUT_TYPE", data->value.asBuffer.length);
  lwm2m_data_free(num_data, data);
  inst_data->type = NULL;
}

static void read_all_obj_with_type(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * inst_data = (lwm2m_digital_output_instance_data_t*)inst->data;
  inst_data->type = "OUTPUT_TYPE";
  
  int num_data = 0;
  lwm2m_data_t * data = NULL;
  
  lwm2m_digital_output_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(num_data, 3);
  assert_memory_equal(find_data_object(data, num_data, LWM2M_DIGITAL_OUTPUT_TYPE_ID)->value.asBuffer.buffer, "OUTPUT_TYPE", find_data_object(data, num_data, LWM2M_DIGITAL_OUTPUT_TYPE_ID)->value.asBuffer.length);
  lwm2m_data_free(num_data, data);
  inst_data->type = NULL;
}

static void write_type_not_allowed(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t*)LWM2M_LIST_FIND(OBJ(state).instanceList, 0);
  lwm2m_digital_output_instance_data_t * inst_data = (lwm2m_digital_output_instance_data_t*)inst->data;
  inst_data->type = "OUTPUT_TYPE";

  int num_data = 1;
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id=LWM2M_DIGITAL_OUTPUT_TYPE_ID;
  data->value.asBuffer.buffer = "NEW_OUTPUT_TYPE";
  data->value.asBuffer.length = strlen("NEW_OUTPUT_TYPE");
  uint8_t ret = lwm2m_digital_output_write(0, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_400_BAD_REQUEST);
  lwm2m_data_free(num_data, data);
  inst_data->type = NULL;
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(create_instance_statically),
  cmocka_unit_test(create_instance_dynamically),
  cmocka_unit_test_setup(write_output_callback_gets_called, callbackSetup),
  cmocka_unit_test_setup(write_output_callback_gets_correct_data, callbackSetup),
  cmocka_unit_test_setup(write_output_callback_gets_correct_data2, callbackSetup),
  cmocka_unit_test_setup(write_output_data_as_string, callbackSetup),
  cmocka_unit_test_setup(write_output_data_as_string2, callbackSetup),
  cmocka_unit_test(write_output_write_unknown_id_fails),
  cmocka_unit_test_setup(read_output_callback_gets_called, callbackSetup),
  cmocka_unit_test_setup(read_output_callback_returns_output_state, callbackSetup),
  cmocka_unit_test_setup(read_output_callback_single_obj, callbackSetup),
  cmocka_unit_test_setup(read_output_callback_all_obj, callbackSetup),
  cmocka_unit_test(read_output_callback_returns_correct_object_id),
  cmocka_unit_test(write_polarity_succeeds),
  cmocka_unit_test(write_polarity_check_value),
  cmocka_unit_test(write_polarity_check_value2),
  cmocka_unit_test(write_polarity_as_bool_check_value),
  cmocka_unit_test(write_polarity_as_bool_check_value2),
  cmocka_unit_test(read_invalid_object_fails),
  cmocka_unit_test(read_invalid_instance_fails),
  cmocka_unit_test(write_invalid_instance_fails),
  cmocka_unit_test_setup(write_output_with_inverse_polarity, callbackSetup),
  cmocka_unit_test_setup(write_output_with_inverse_polarity2, callbackSetup),
  cmocka_unit_test_setup(read_type, callbackSetup),
  cmocka_unit_test_setup(read_all_obj_with_type, callbackSetup),
  cmocka_unit_test_setup(write_type_not_allowed, callbackSetup),
};

DEFINE_TEST("Object Digital Output", tests, group_setup, group_teardown);
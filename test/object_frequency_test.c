/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include <ulwm2m.h>
#include "utest.h"

#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

typedef struct{
  lwm2m_object_t obj;
}test_state_t;

#define OBJ(state) ((test_state_t*)*state)->obj

static bool read_value_callback_called = false;
static float read_value_callback(){
  read_value_callback_called = true;
  return 1000.0;
}

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  memset(*state, 0, sizeof(test_state_t));
  lwm2m_frequency_instance_data_t * freq = lwm2m_malloc(sizeof(lwm2m_frequency_instance_data_t));
  memset(freq, 0, sizeof(lwm2m_frequency_instance_data_t));
  freq->frequency = read_value_callback;
  freq->units = "Hz";

  lwm2m_init_object(&OBJ(state), LWM2M_FREQUENCY_OBJECT_ID, lwm2m_frequency_read, lwm2m_frequency_write, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_instance_t * inst = lwm2m_create_instance(0, freq, NULL, true);
  lwm2m_object_add_instance(&OBJ(state), inst);

  return 0;
}

static int group_teardown(void **state){
  lwm2m_clear_object(&OBJ(state));

  lwm2m_free(*state);
  return 0;
}

static void create_instance_statically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_FREQUENCY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t data = {};
  lwm2m_instance_t * inst = lwm2m_create_instance(0, &data, NULL, false);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void create_instance_dynamically(void **state){
  lwm2m_object_t  obj = {};
  lwm2m_init_object(&obj, LWM2M_FREQUENCY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_device_instance_data_t * data = lwm2m_calloc(1, sizeof(lwm2m_device_instance_data_t));
  lwm2m_instance_t * inst = lwm2m_create_instance(0, data, NULL, true);
  lwm2m_object_add_instance(&obj, inst);
  lwm2m_clear_object(&obj);
}

static void returns_not_found_error_if_instance_not_found(void **state){
  lwm2m_data_t * data = NULL;
  int num_data = 0;
  uint8_t ret = lwm2m_frequency_read(1, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
}

static void returns_not_found_if_frequency_value_callback_is_missing(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(OBJ(state).instanceList, 0);
  lwm2m_frequency_instance_data_t * obj = (lwm2m_frequency_instance_data_t*)inst->data;
  obj->frequency = NULL;

  lwm2m_data_t data = {.id = LWM2M_FREQUENCY_SENSOR_VALUE_ID};
  lwm2m_data_t * tmp = &data;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &tmp, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  obj->frequency = read_value_callback;
}

static void returns_not_found_if_frequency_unit_is_missing(void **state){
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(OBJ(state).instanceList, 0);
  lwm2m_frequency_instance_data_t * obj = (lwm2m_frequency_instance_data_t*)inst->data;
  obj->units = NULL;

  lwm2m_data_t data = {.id = LWM2M_FREQUENCY_SENSOR_UNITS_ID};
  lwm2m_data_t* tmp = &data;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &tmp, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);
  obj->units = "Hz";
}

static void read_frequency_callback_gets_called(void **state){
  lwm2m_data_t * data = NULL;
  int num_data = 0;
  lwm2m_frequency_read(0, &num_data, &data, &OBJ(state));

  assert_true(read_value_callback_called);
  lwm2m_data_free(num_data, data);
}

static void returns_all_obj(void **state){
  lwm2m_data_t * data = NULL;
  int num_data = 0;
  
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(num_data, 2);
  assert_int_equal(data[0].id, LWM2M_FREQUENCY_SENSOR_VALUE_ID);
  assert_int_equal(data[0].type, LWM2M_TYPE_FLOAT);
  assert_int_equal(data[0].value.asFloat, 1000);
  assert_int_equal(data[1].id, LWM2M_FREQUENCY_SENSOR_UNITS_ID);
  assert_int_equal(data[1].type, LWM2M_TYPE_STRING);
  assert_memory_equal(data[1].value.asBuffer.buffer, "Hz", data[1].value.asBuffer.length);

  lwm2m_data_free(num_data, data);
}

static void returns_single_obj(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = LWM2M_FREQUENCY_SENSOR_VALUE_ID;
  int num_data = 1;
  
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(num_data, 1);
  assert_int_equal(data->id, LWM2M_FREQUENCY_SENSOR_VALUE_ID);
  assert_int_equal(data->type, LWM2M_TYPE_FLOAT);
  assert_int_equal(data->value.asFloat, 1000);

  lwm2m_data_free(num_data, data);
}

static void returns_single_obj2(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = LWM2M_FREQUENCY_SENSOR_UNITS_ID;
  int num_data = 1;
  
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_205_CONTENT);
  assert_int_equal(num_data, 1);
  assert_int_equal(data->id, LWM2M_FREQUENCY_SENSOR_UNITS_ID);
  assert_int_equal(data->type, LWM2M_TYPE_STRING);
  assert_memory_equal(data->value.asBuffer.buffer, "Hz", data->value.asBuffer.length);

  lwm2m_data_free(num_data, data);
}

static void write_frequency_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = LWM2M_FREQUENCY_SENSOR_VALUE_ID;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_write(0, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  lwm2m_data_free(1, data);
}

static void write_frequency_units_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = LWM2M_FREQUENCY_SENSOR_UNITS_ID;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_write(0, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_405_METHOD_NOT_ALLOWED);

  lwm2m_data_free(1, data);
}

static void write_unknown_node_id_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 1234;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_write(0, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);

  lwm2m_data_free(1, data);
}

static void read_unknown_node_id_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 1234;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_read(0, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);

  lwm2m_data_free(1, data);
}

static void read_unknown_instance_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 1234;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_read(1, &num_data, &data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);

  lwm2m_data_free(1, data);
}

static void write_unknown_instance_fails(void **state){
  lwm2m_data_t * data = lwm2m_data_new(1);
  data->id = 1234;
  int num_data = 1;
  uint8_t ret = lwm2m_frequency_write(1, num_data, data, &OBJ(state));

  assert_int_equal(ret, COAP_404_NOT_FOUND);

  lwm2m_data_free(1, data);
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(create_instance_statically),
  cmocka_unit_test(create_instance_dynamically),
  cmocka_unit_test(returns_not_found_error_if_instance_not_found),
  cmocka_unit_test(returns_not_found_if_frequency_value_callback_is_missing),
  cmocka_unit_test(returns_not_found_if_frequency_unit_is_missing),
  cmocka_unit_test(read_frequency_callback_gets_called),
  cmocka_unit_test(returns_all_obj),
  cmocka_unit_test(returns_single_obj),
  cmocka_unit_test(returns_single_obj2),
  cmocka_unit_test(write_frequency_fails),
  cmocka_unit_test(write_frequency_units_fails),
  cmocka_unit_test(write_unknown_node_id_fails),
  cmocka_unit_test(read_unknown_node_id_fails),
  cmocka_unit_test(read_unknown_instance_fails),
  cmocka_unit_test(write_unknown_instance_fails),
};
  
DEFINE_TEST("Object Frequency", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include "platform.h"
#include <ulwm2m.h>
#include <internal.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>

/* setup and teardown testsuite */
typedef struct{
  lwm2m_context_t * ctx;
  client_data_t userData;
  uint8_t sessionH;
}test_state_t;

#define TEST_CTX(state) ((test_state_t*)*state)->ctx
#define TEST_CTX_USER_DATA(state) ((test_state_t*)*state)->userData
#define TEST_SESSION_HANDLE(state) ((test_state_t*)*state)->sessionH

#define COAP_OPTION_URI_PATH 11
#define COAP_OPTION_CONTENT_TYPE 12

static coap_option_data_t test_coap_options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '1'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // Content type opaque
};

uint8_t write_callback(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP){
  check_expected(instanceId);
  check_expected(numData);
  check_expected(objectP);

  function_called();
  return (uint8_t)mock();
}

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));

  TEST_CTX(state) = lwm2m_init(&TEST_CTX_USER_DATA(state));

  /* add dummy server to context*/
  TEST_CTX(state)->serverList = lwm2m_malloc(sizeof(lwm2m_server_t));
  TEST_CTX(state)->serverList->sessionH = &TEST_SESSION_HANDLE(state);
  TEST_CTX(state)->serverList->status = STATE_REGISTERED;

  /* Add Dummy Server Object*/
  /* create static server object */
  lwm2m_object_t * server = lwm2m_malloc(sizeof(lwm2m_object_t));
  memset(server, 0, sizeof(lwm2m_object_t));
  server->objID = LWM2M_SERVER_OBJECT_ID;
  server->writeFunc = write_callback;
  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, NULL, NULL, false);
  lwm2m_object_add_instance(server, server_inst);

  TEST_CTX(state)->objectList = server;

  return 0;
}

static int group_teardown(void **state){
  lwm2m_free(TEST_CTX(state)->serverList);
  lwm2m_free(TEST_CTX(state)->objectList[0].instanceList);
  lwm2m_free(TEST_CTX(state)->objectList);
  lwm2m_free(TEST_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static void write_single_resource(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");
  
  expect_function_call(write_callback);
  expect_value(write_callback, instanceId, 0);
  int expectedNumData = 1;
  expect_value(write_callback, numData, expectedNumData);
  expect_memory(write_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(write_callback, COAP_204_CHANGED);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_204_CHANGED);
}

static void write_single_resource_return_message_corect_status_code(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");
  
  ignore_function_calls(write_callback);
  expect_any(write_callback, instanceId);
  expect_any(write_callback, numData);
  expect_any(write_callback, objectP);
  will_return(write_callback, COAP_204_CHANGED);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_204_CHANGED);
}

static void write_readonly_resource_fails(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");
  
  ignore_function_calls(write_callback);
  expect_any(write_callback, instanceId);
  expect_any(write_callback, numData);
  expect_any(write_callback, objectP);
  will_return(write_callback, COAP_405_METHOD_NOT_ALLOWED);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_405_METHOD_NOT_ALLOWED);
}

static void write_not_existing_resource_fails(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");
  
  ignore_function_calls(write_callback);
  expect_any(write_callback, instanceId);
  expect_any(write_callback, numData);
  expect_any(write_callback, objectP);
  will_return(write_callback, COAP_404_NOT_FOUND);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static void write_not_existing_instance_fails(void **state){
  coap_t req = {};
  test_coap_options[1].data.u8 = '5'; // Instance with ID doe snot exist
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static void write_not_existing_object_fails(void **state){
  coap_t req = {};
  test_coap_options[0].data.u8 = '5'; // Object with ID doe snot exist
  size_t numbytes = create_coap_req(&req, COAP_TYPE_REQ, COAP_PUT, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), "\x34");

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(write_single_resource),
  cmocka_unit_test(write_single_resource_return_message_corect_status_code),
  cmocka_unit_test(write_readonly_resource_fails),
  cmocka_unit_test(write_not_existing_resource_fails),
  cmocka_unit_test(write_not_existing_instance_fails),
  cmocka_unit_test(write_not_existing_object_fails),
};

DEFINE_TEST("Handle write request", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include "platform.h"
#include <ulwm2m.h>
#include <er-coap-13.h>
#include <internal.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>

/* setup and teardown testsuite */
typedef struct{
  lwm2m_context_t * ctx;
  client_data_t userData;
  uint8_t sessionH;
}test_state_t;

#define TEST_CTX(state) ((test_state_t*)*state)->ctx
#define TEST_CTX_USER_DATA(state) ((test_state_t*)*state)->userData
#define TEST_SESSION_HANDLE(state) ((test_state_t*)*state)->sessionH

static coap_option_data_t test_coap_options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '1'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
};

uint8_t read_callback(uint16_t instanceId,int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP){
  check_expected(instanceId);
  check_expected(numDataP);
  check_expected(objectP);

  if(*numDataP == 0){
    *dataArrayP = lwm2m_data_new(2);
    (*dataArrayP)[0].id = LWM2M_SERVER_SHORT_ID_ID;
    (*dataArrayP)[0].type = LWM2M_TYPE_INTEGER;
    (*dataArrayP)[0].value.asInteger = 5;
    (*dataArrayP)[1].id = LWM2M_SERVER_LIFETIME_ID;
    (*dataArrayP)[1].type = LWM2M_TYPE_INTEGER;
    (*dataArrayP)[1].value.asInteger = 300;
    *numDataP = 2;
  }else{
    for(int i=0; i<*numDataP;i++){
      switch((*dataArrayP)[i].id){
        case LWM2M_SERVER_SHORT_ID_ID:
          (*dataArrayP)[i].type = LWM2M_TYPE_INTEGER;
          (*dataArrayP)[i].value.asInteger = 5;
          break;

        case LWM2M_SERVER_LIFETIME_ID:
          (*dataArrayP)[i].type = LWM2M_TYPE_INTEGER;
          (*dataArrayP)[i].value.asInteger = 300;
          break;
      } 
    }
  }
  function_called();
  return (uint8_t)mock();
}

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));

  TEST_CTX(state) = lwm2m_init(&TEST_CTX_USER_DATA(state));

  /* add dummy server to context*/
  TEST_CTX(state)->serverList = lwm2m_malloc(sizeof(lwm2m_server_t));
  TEST_CTX(state)->serverList->sessionH = &TEST_SESSION_HANDLE(state);
  TEST_CTX(state)->serverList->status = STATE_REGISTERED;

  /* Add Dummy Server Object*/
  /* create static server object */
  lwm2m_object_t * server = lwm2m_malloc(sizeof(lwm2m_object_t));
  memset(server, 0, sizeof(lwm2m_object_t));
  server->objID = LWM2M_SERVER_OBJECT_ID;
  server->readFunc = read_callback;
  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, NULL, NULL, false);
  lwm2m_object_add_instance(server, server_inst);

  TEST_CTX(state)->objectList = server;

  return 0;
}

static int group_teardown(void **state){
  lwm2m_free(TEST_CTX(state)->serverList);
  lwm2m_free(TEST_CTX(state)->objectList[0].instanceList);
  lwm2m_free(TEST_CTX(state)->objectList);
  lwm2m_free(TEST_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static void request_all_resources(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_GET, test_coap_options, 2, NULL);
  
  expect_function_call(read_callback);
  expect_value(read_callback, instanceId, 0);
  int expectedNumData = 0;
  expect_memory(read_callback, numDataP, &expectedNumData, sizeof(expectedNumData));
  expect_memory(read_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(read_callback, COAP_205_CONTENT);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));
}

static void request_single_resource(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_GET, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), NULL);
  
  expect_function_call(read_callback);
  expect_value(read_callback, instanceId, 0);
  int expectedNumData = 1;
  expect_memory(read_callback, numDataP, &expectedNumData, sizeof(expectedNumData));
  expect_memory(read_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(read_callback, COAP_205_CONTENT);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));
}

static void request_single_resource_check_response_tlv(void **state){
  coap_t req = {};
  size_t numbytes = create_coap_req(&req, COAP_GET, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), NULL);
  
  expect_function_call(read_callback);
  expect_value(read_callback, instanceId, 0);
  int expectedNumData = 1;
  expect_memory(read_callback, numDataP, &expectedNumData, sizeof(expectedNumData));
  expect_memory(read_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(read_callback, COAP_205_CONTENT);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  tlv_t * data = get_coap_tlv((coap_t*)sent_message);
  assert_int_equal(TLV_TYPE_VALUE_LENGTH(data), 1);
  assert_int_equal(TLV_TYPE_LENGTH_TYPE(data), 0);
  assert_int_equal(TLV_TYPE_IDENTIFIER_LENGTH(data), TLV_SHORT_IDENTIFIER);
  assert_int_equal(TLV_IDENTIFER_SHORT(data), 0);
  assert_int_equal(TLV_VALUE_8BIT(data), 5);
}

static void request_single_resource_returns_205(void **state){
  coap_t req = {};
  coap_option_data_t opts[3];
  opts[0].option = COAP_OPTION_URI_PATH;
  opts[0].length = 1;
  opts[0].data.u8 = '1';
  opts[1].option = COAP_OPTION_URI_PATH;
  opts[1].length = 1;
  opts[1].data.u8 = '0';
  opts[2].option = COAP_OPTION_URI_PATH;
  opts[2].length = 1;
  opts[2].data.u8 = '0';
  size_t numbytes = create_coap_req(&req, COAP_GET, opts, sizeof(opts)/sizeof(coap_option_data_t), NULL);
  
  expect_function_call(read_callback);
  expect_value(read_callback, instanceId, 0);
  int expectedNumData = 1;
  expect_memory(read_callback, numDataP, &expectedNumData, sizeof(expectedNumData));
  expect_memory(read_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(read_callback, COAP_205_CONTENT);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_205_CONTENT);
}

static void request_not_available_resource_returns_404(void **state){
  coap_t req = {};
  test_coap_options[2].data.u8 = '3';
  size_t numbytes = create_coap_req(&req, COAP_GET, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), NULL);
  
  expect_function_call(read_callback);
  expect_value(read_callback, instanceId, 0);
  int expectedNumData = 1;
  expect_memory(read_callback, numDataP, &expectedNumData, sizeof(expectedNumData));
  expect_memory(read_callback, objectP, TEST_CTX(state)->objectList, sizeof(lwm2m_object_t));
  will_return(read_callback, COAP_404_NOT_FOUND);

  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static void request_not_available_object_returns_404(void **state){
  coap_t req = {};
  coap_option_data_t opts = {.option = COAP_OPTION_URI_PATH, .length = 1, .data.u8 = '5'};
  size_t numbytes = create_coap_req(&req, COAP_GET, &opts, 1, NULL);
  
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static void request_not_available_instance_returns_404(void **state){
  coap_t req = {};
  test_coap_options[1].data.u8 = '9'; // Instance 9 not available
  size_t numbytes = create_coap_req(&req, COAP_GET, test_coap_options, sizeof(test_coap_options)/sizeof(coap_option_data_t), NULL);
  
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&req, numbytes, &TEST_SESSION_HANDLE(state));

  coap_t * msg = (coap_t*)sent_message;
  assert_int_equal(msg->code, COAP_404_NOT_FOUND);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(request_all_resources),
  cmocka_unit_test(request_single_resource),
  cmocka_unit_test(request_single_resource_check_response_tlv),
  cmocka_unit_test(request_single_resource_returns_205),
  cmocka_unit_test(request_not_available_resource_returns_404),
  cmocka_unit_test(request_not_available_object_returns_404),
  cmocka_unit_test(request_not_available_instance_returns_404),
};

DEFINE_TEST("Handle get Request", tests, group_setup, group_teardown);
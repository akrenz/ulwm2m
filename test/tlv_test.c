/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"

#include <internals.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>

static int group_setup(void **state){
  return 0;
}

static int group_teardown(void **state){
  return 0;
}

static void tlv_serialize_u8(void **state){
  uint8_t *buffer = NULL;
  lwm2m_data_t data = {.id = 2, .type=LWM2M_TYPE_INTEGER, .value.asInteger = 10};
  int numbytes = tlv_serialize(true, 1, &data, (uint8_t**)&buffer);

  assert_int_equal(TLV_IDENTIFER_SHORT(buffer), 2); // resource instance number
  assert_int_equal(TLV_TYPE_IDENTIFIER_TYPE(buffer), 0b01); // type is 'resource instance with value'
  assert_int_equal(TLV_TYPE_IDENTIFIER_LENGTH(buffer), 0); // length of identifier is 8 bit (=0)
  assert_int_equal(TLV_TYPE_LENGTH_TYPE(buffer), 0b00); // no length value, value follows directly after identifier
  assert_int_equal(TLV_TYPE_VALUE_LENGTH(buffer), 1); // length of value is 1 byte
  assert_int_equal(TLV_VALUE_8BIT(buffer), 10);
  assert_int_equal(numbytes, 3);

  lwm2m_free(buffer);
}

static void tlv_serialize_u16(void **state){
  uint8_t *buffer = NULL;
  lwm2m_data_t data = {.id = 2, .type=LWM2M_TYPE_INTEGER, .value.asInteger = 1000};
  int numbytes = tlv_serialize(true, 1, &data, (uint8_t**)&buffer);

  assert_int_equal(TLV_IDENTIFER_SHORT(buffer), 2); // resource instance number
  assert_int_equal(TLV_TYPE_IDENTIFIER_TYPE(buffer), 0b01); // type is 'resource instance with value'
  assert_int_equal(TLV_TYPE_IDENTIFIER_LENGTH(buffer), 0); // length of identifier is 8 bit (=0)
  assert_int_equal(TLV_TYPE_LENGTH_TYPE(buffer), 0b00); // no length value, value follows directly after identifier
  assert_int_equal(TLV_TYPE_VALUE_LENGTH(buffer), 2); // length of value is 2 byte
  assert_int_equal(TLV_VALUE_16BIT(buffer), 1000);
  assert_int_equal(numbytes, 4);
  
  lwm2m_free(buffer);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(tlv_serialize_u8),
  cmocka_unit_test(tlv_serialize_u16),
};

DEFINE_TEST("TLV Serialize", tests, group_setup, group_teardown);
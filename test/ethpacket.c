/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

/* SPDX-License-Identifier: BSD-2-Clause-Patent */
#include "ethpacket.h"

#include <arpa/inet.h>
#include <string.h>

static uint16_t s_mid = 0;

typedef struct {
  uint8_t length:4;
  uint8_t delta:4;
  uint8_t data;
#if defined(__clang__)
} __attribute__((packed)) coap_option_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) coap_option_t;
#endif

typedef struct {
  uint8_t length:4;
  uint8_t delta:4;
  uint8_t delta_extended;
  uint8_t data;
#if defined(__clang__)
} __attribute__((packed)) coap_option_extendet_delta_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) coap_option_extendet_delta_t;
#endif

typedef struct {
  uint8_t length:4;
  uint8_t delta:4;
  uint8_t length_extended;
  uint8_t data;
#if defined(__clang__)
} __attribute__((packed)) coap_option_extendet_length_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) coap_option_extendet_length_t;
#endif

typedef struct {
  uint8_t length:4;
  uint8_t delta:4;
  uint8_t delta_extended;
  uint8_t length_extended;
  uint8_t data;
#if defined(__clang__)
} __attribute__((packed)) coap_option_extendet_delta_length_t;
#elif defined(__GNUC__) || defined(__GNUG__)
}  __attribute__((packed)) coap_option_extendet_delta_length_t;
#endif

#define OPTION_NEEDS_EXTENDED_DELTA(new, old) (new - old > 12)
#define OPTION_NEEDS_EXTENDED_LENGTH(length) (length > 12)

size_t encode_option(uint8_t* data, coap_option_data_t * option, coap_option_data_t* old_option){
  size_t numbytes = 0;
  uint32_t old_option_num = 0;

  /* for the first option old_option must be NULL, therefor the first option gets encoded with delta equal to its option number */
  if(old_option != NULL){
    old_option_num = old_option->option;
  }

  if(!OPTION_NEEDS_EXTENDED_DELTA(option->option, old_option_num) && !OPTION_NEEDS_EXTENDED_LENGTH(option->length)){
    coap_option_t * opt = (coap_option_t *)data;
    opt->delta = option->option - old_option_num;
    opt->length = option->length;
    if(opt->length == 1){
      opt->data = option->data.u8;
    }else if(opt->length == 2){
      *((uint16_t*)&opt->data) = option->data.u16;
    }else{
      memcpy(&opt->data, option->data.u8p, option->length);
    }
    numbytes = 1 + option->length;

  }else if(OPTION_NEEDS_EXTENDED_DELTA(option->option, old_option_num) && !OPTION_NEEDS_EXTENDED_LENGTH(option->length)){
    coap_option_extendet_delta_t * opt = (coap_option_extendet_delta_t*)data;
    opt->delta = 13;
    opt->delta_extended = (option->option - old_option_num) -13;
    opt->length = option->length;
    if(opt->length == 1){
      opt->data = option->data.u8;
    }else if(opt->length == 2){
      *((uint16_t*)&opt->data) = option->data.u16;
    }else{
      memcpy(&opt->data, option->data.u8p, option->length);
    }
    numbytes = 2 + option->length;
  
  }else if(!OPTION_NEEDS_EXTENDED_DELTA(option->option, old_option_num) && OPTION_NEEDS_EXTENDED_LENGTH(option->length)){
    coap_option_extendet_length_t * opt = (coap_option_extendet_length_t*)data;
    opt->delta = option->option - old_option_num;
    opt->length = 13;
    opt->length_extended = option->length - 13;
    if(opt->length == 1){
      opt->data = option->data.u8;
    }else if(opt->length == 2){
      *((uint16_t*)&opt->data) = option->data.u16;
    }else{
      memcpy(&opt->data, option->data.u8p, option->length);
    }
    numbytes = 2 + option->length;
  }else if(OPTION_NEEDS_EXTENDED_DELTA(option->option, old_option_num) && OPTION_NEEDS_EXTENDED_LENGTH(option->length)){
    coap_option_extendet_delta_length_t * opt = (coap_option_extendet_delta_length_t*)data;
    opt->delta = 13;
    opt->delta_extended = (option->option - old_option_num) -13;
    opt->length = 13;
    opt->length_extended = option->length - 13;
    if(opt->length == 1){
      opt->data = option->data.u8;
    }else if(opt->length == 2){
      *((uint16_t*)&opt->data) = option->data.u16;
    }else{
      memcpy(&opt->data, option->data.u8p, option->length);
    }
    numbytes = 3 + option->length;
  }

  return numbytes;
}

size_t create_coap_req(coap_t * req, const coap_type_t type, const coap_method_t code, coap_option_data_t * options, const size_t numoptions, const char * payload){
  req->ver = 1;
  req->type = type;
  req->token_length = 4;
  req->code = code;
  req->mid = htons(s_mid++);
  req->token = htonl(0x3b21e2f2l);

  size_t offset = 0;
  offset += encode_option(&req->payload[offset], &options[0], NULL);
  for(int i=1; i< numoptions; ++i){
    offset += encode_option(&req->payload[offset], &options[i], &options[i-1]);
  }
  if(payload != NULL){
    req->payload[offset] = 0xff; /* end of options marker */
    offset++;
  }
  if(payload != NULL){
    strcpy(req->payload + offset, payload);
  }

  size_t numbytes = sizeof(coap_t);
  numbytes += offset;
  if(payload != NULL){
    numbytes += strlen(payload);
  }
  numbytes -= COAP_MAX_PAYLOAD_SIZE;
  return numbytes;
}

bool coap_message_equal(coap_t* actual, coap_t* expected, size_t size){
  if(actual->ver != expected->ver) return false;
  if(actual->code != expected->code) return false;
  if(actual->token_length != expected->token_length) return false;
  if(actual->type != expected->type) return false;
  if(memcmp(actual->payload, expected->payload, size - 8) != 0) return false;
  
  return true;
}

tlv_t* get_coap_tlv(coap_t * msg){
  coap_t * coap = (coap_t*)msg;
  uint8_t * ptr = coap->payload;
  while(*ptr != 0xFF){
    ptr = ptr + (ptr[0] & 0x0F) + 1;
  }
  ptr++;
  return (tlv_t*)ptr;
}

inline uint8_t get_u8_tlv_value(uint8_t* tlv){
  if(TLV_TYPE_IDENTIFIER_LENGTH(tlv) == TLV_SHORT_IDENTIFIER){
    return ((uint8_t*)tlv)[2];
  }else{
    return ((uint8_t*)tlv)[3];
  }
}

inline uint16_t get_u16_tlv_value(uint8_t* tlv){
  if(TLV_TYPE_IDENTIFIER_LENGTH(tlv) == TLV_SHORT_IDENTIFIER){
    uint16_t data =  ((uint8_t*)tlv)[2] << 8 | ((uint8_t*)tlv)[3];
    return data;
  }else{
    uint16_t data =  ((uint8_t*)tlv)[3] << 8 | ((uint8_t*)tlv)[4];
    return data;
  }
}

size_t create_tlv(void* buffer, const uint8_t value){
  tlv_t* tlv = buffer;
  tlv->value_length = 1;
  tlv->length_type = TLV_LENGTH_IS_U8;
  tlv->identifier_length = TLV_SHORT_IDENTIFIER;
  tlv->identifier.asUint8 = TLV_IDENTIFIER_TYPE_RESOURCE_WITH_VALUE;

  return 3;
}

uint16_t get_mid(){
  return s_mid;
}
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include "platform.h"
#include <ulwm2m.h>

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

typedef struct{
  lwm2m_context_t * ctx;
}test_state_t;
#define TEST_DATA_CTX(state) ((test_state_t*)*state)->ctx

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));
  TEST_DATA_CTX(state) = lwm2m_init(NULL);
  return 0;
}

static int group_teardown(void **state){
  lwm2m_close(TEST_DATA_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static int sessionH = 1;
static int add_client(void **state){
  lwm2m_context_t * contextP = TEST_DATA_CTX(state);
  lwm2m_client_t * clientP = (lwm2m_client_t*)lwm2m_malloc(sizeof(lwm2m_client_t));
  memset(clientP, 0, sizeof(lwm2m_client_t));
  clientP->internalID = lwm2m_list_newId((lwm2m_list_t *)contextP->clientList);
  clientP->sessionH = &sessionH;
  contextP->clientList = (lwm2m_client_t *)LWM2M_LIST_ADD(contextP->clientList, clientP);

  return clientP->internalID;
}

static void lwm2m_server_read_data_from_non_existing_clientid_fails(void **state){
  lwm2m_uri_t uri= {.objectId=3, .instanceId=0, .flag=LWM2M_URI_FLAG_OBJECT_ID | LWM2M_URI_FLAG_INSTANCE_ID};
  int result = lwm2m_dm_read(TEST_DATA_CTX(state), 99, &uri, NULL, NULL);
  assert_int_not_equal(result, COAP_NO_ERROR);
}

static void lwm2m_server_read_data_req_correct_length(void **state){
  int id = add_client(state);
  char buffer[] = "/3/0/";
  lwm2m_uri_t uri= {};
  lwm2m_stringToUri(buffer, sizeof(buffer), &uri);
  lwm2m_dm_read(TEST_DATA_CTX(state), id, &uri, NULL, NULL);

  coap_t req = {};
  coap_option_data_t opts[3];
  opts[0].option = COAP_OPTION_URI_PATH;
  opts[0].length = 1;
  opts[0].data.u8 = '3';
  opts[1].option = COAP_OPTION_URI_PATH;
  opts[1].length = 1;
  opts[1].data.u8 = '0';
  opts[2].option = COAP_OPTION_ACCEPT;
  opts[2].length = 2;
  opts[2].data.u16 = 0x162d;
  // opts[2].data[1] = 0x16;

  int size = create_coap_req(&req, COAP_TYPE_REQ, COAP_GET, opts, 3, NULL);

  assert_int_equal(size, sent_bytes);
  lwm2m_free(TEST_DATA_CTX(state)->transactionList->userData);
}

static void lwm2m_server_read_data_req_correct_content(void **state){
  int id = add_client(state);
  char buffer[] = "/3/0/";
  lwm2m_uri_t uri= {};
  lwm2m_stringToUri(buffer, sizeof(buffer), &uri);
  lwm2m_dm_read(TEST_DATA_CTX(state), id, &uri, NULL, NULL);

  coap_t req = {};
  coap_option_data_t opts[3];
  opts[0].option = COAP_OPTION_URI_PATH;
  opts[0].length = 1;
  opts[0].data.u8 = '3';
  opts[1].option = COAP_OPTION_URI_PATH;
  opts[1].length = 1;
  opts[1].data.u8 = '0';
  opts[2].option = COAP_OPTION_ACCEPT;
  opts[2].length = 2;
  opts[2].data.u16 = 0x162d;
  // opts[2].data[1] = 0x16;

  create_coap_req(&req, COAP_TYPE_REQ, COAP_GET, opts, 3, NULL);

  assert_true(coap_message_equal(&req, (coap_t*)sent_message, sent_bytes));
}
  
static const struct CMUnitTest tests[] = {
  cmocka_unit_test(lwm2m_server_read_data_from_non_existing_clientid_fails),
  cmocka_unit_test(lwm2m_server_read_data_req_correct_length),
  cmocka_unit_test(lwm2m_server_read_data_req_correct_content),
};
DEFINE_TEST("Device Management Read", tests, group_setup, group_teardown);
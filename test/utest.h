/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _UTEST_H_
#define _UTEST_H_

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <cmocka.h>

typedef int (*execute_test_callback)(const char *group_name,
                            const struct CMUnitTest * const tests,
                            const size_t num_tests,
                            CMFixtureFunction group_setup,
                            CMFixtureFunction group_teardown);

#ifndef  ALIGN_SECTION
#if defined(__LP64__) || defined(_LP64)
#define  ALIGN_SECTION  __attribute__((__aligned__(16)))
#elif defined(__ILP32__) || defined(_ILP32)
#define  ALIGN_SECTION  __attribute__((__aligned__(8)))
#else
#define  ALIGN_SECTION
#endif
#endif

typedef struct _test_t{
  const char* group_name;
  const struct CMUnitTest * const tests;
  const size_t num_tests;
  CMFixtureFunction group_setup;
  CMFixtureFunction group_teardown;
  uint8_t padding[24];  // ToDo Get the Alignement right. Struct size is 40 Byte, but linker aligns it to 64 bytes
} test_t ALIGN_SECTION;


#define  TEST_NAME(counter)  TEST_CAT(info_, counter)
#define  TEST_CAT(a, b)      TEST_DUMMY() a ## b
#define  TEST_DUMMY()

#define  DEFINE_TEST(group_name, tests, group_setup, group_teardown) \
         static test_t  TEST_NAME(__COUNTER__) \
             __attribute__((__used__, __section__("test"))) \
             = {group_name, tests, sizeof(tests)/sizeof(tests[0]), group_setup, group_teardown }

// extern test_t utest_tests[NUM_TESTS];

// #define ADD_TEST(callback, group_name, tests, num_tests, group_setup, group_teardown, I) utest_tests[I] = {.callback =callback, .group_name = group_name, .tests = tests, .num_tests = num_tests, .group_setup = group_setup, .group_teardown = group_teardown}


#endif /* _UTEST_H_ */
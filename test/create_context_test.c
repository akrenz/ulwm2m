/*******************************************************************************
 *
 * Copyright (c) 2021 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "utest.h"
#include <ulwm2m.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

static int group_setup(void **state){
  return 0;
}

static int group_teardown(void **state){
  return 0;
}

static void create_ipv6_context(void **state){
  lwm2m_object_t objArray[3];

  lwm2m_init_object(&objArray[0], LWM2M_SECURITY_OBJECT_ID, lwm2m_security_read, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  lwm2m_security_instance_data_t secInstData = {.uri="coap://2002:543f:ae34:0:ed2e:a4c8:a3c:6a7e:5683",
                                                .isBootstrap = false,
                                                .securityMode = SECURITY_MODE_NO_SECURITY,
                                                .keyOrIdentity = NULL,
                                                .serverPublicKey = NULL,
                                                .secretKey = NULL,
                                                .shortID=123,
                                                .clientHoldOffTime=10,
                                                .BootstrapServerAccountTimeout=0};
  lwm2m_instance_t * sec_inst = lwm2m_create_instance(0, &secInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[0], sec_inst);

  /* create static server object */
  lwm2m_init_object(&objArray[1], LWM2M_SERVER_OBJECT_ID, lwm2m_server_read, lwm2m_server_write, lwm2m_server_execute, lwm2m_server_create, lwm2m_server_delete, lwm2m_server_discover, NULL, NULL, NULL);
  lwm2m_server_instance_data_t serverInstData = { .shortServerId = 123,
                                                  .lifetime = 300,
                                                  .defaultMinObservationPeriod = 10,
                                                  .defaultMaxObservationPeriod = 60,
                                                  .storing = false,
                                                  .binding = BINDING_U
  };
  lwm2m_instance_t * server_inst = lwm2m_create_instance(0, &serverInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[1], server_inst);

  /* create mandatory device object */
  lwm2m_init_object(&objArray[2], LWM2M_DEVICE_OBJECT_ID, lwm2m_device_read, NULL, lwm2m_device_execute, NULL, NULL, lwm2m_device_discover, NULL, NULL, NULL);
  lwm2m_device_instance_data_t deviceInstData = {  .manufacturer = "Albert Krenz",
                                                .firmware = "Number 1.2.3",
                                                .binding = BINDING_U
  };
  lwm2m_instance_t * device_inst = lwm2m_create_instance(0, &deviceInstData, NULL, false);
  lwm2m_object_add_instance(&objArray[2], device_inst);

  lwm2m_context_t * ctx = lwm2m_create_context(objArray, 3, "Democlient", "5683", AF_INET6, 5000);
  assert_non_null(ctx);

  lwm2m_close_context(ctx);
  lwm2m_clear_object(&objArray[0]);
  lwm2m_clear_object(&objArray[1]);
  lwm2m_clear_object(&objArray[2]);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(create_ipv6_context),
};

DEFINE_TEST("create context tests", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
#include <ulwm2m.h>
#include "utest.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

static int group_setup(void **state){
  return 0;
}

static int group_teardown(void **state){
  return 0;
}

static void lwm2m_init_object_returns_failure_if_no_object_passed(void **state){
  int ret = lwm2m_init_object(NULL, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  assert_int_equal(ret, -1);
}

static void lwm2m_init_object_returns_1_on_success(void **state){
  lwm2m_object_t obj = {};
  int ret = lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
  assert_int_equal(ret, 0);
}

static void lwm2m_init_object_inserts_correct_obj_id(void **state){
  lwm2m_object_t obj = {};
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.objID, LWM2M_SECURITY_OBJECT_ID);
}

static void lwm2m_init_object_inserts_correct_read_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, callback, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.readFunc, callback);
}

static void lwm2m_init_object_inserts_correct_write_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, callback, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.writeFunc, callback);
}

static void lwm2m_init_object_inserts_correct_execute_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, callback, NULL, NULL, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.executeFunc, callback);
}

static void lwm2m_init_object_inserts_correct_create_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, callback, NULL, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.createFunc, callback);
}

static void lwm2m_init_object_inserts_correct_delete_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, NULL, callback, NULL, NULL, NULL, NULL);

  assert_int_equal(obj.deleteFunc, callback);
}

static void lwm2m_init_object_inserts_correct_discover_callback(void **state){
  lwm2m_object_t obj = {};
  uint8_t callback(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP){
    return 0;
  }
  lwm2m_init_object(&obj, LWM2M_SECURITY_OBJECT_ID, NULL, NULL, NULL, NULL, NULL, callback, NULL, NULL, NULL);

  assert_int_equal(obj.discoverFunc, callback);
}

static const struct CMUnitTest tests[] = {
  cmocka_unit_test(lwm2m_init_object_returns_failure_if_no_object_passed),
  cmocka_unit_test(lwm2m_init_object_returns_1_on_success),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_obj_id),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_read_callback),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_write_callback),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_execute_callback),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_discover_callback),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_create_callback),
  cmocka_unit_test(lwm2m_init_object_inserts_correct_delete_callback),
};

DEFINE_TEST("Init Object", tests, group_setup, group_teardown);
/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "ethpacket.h"
#include "utest.h"
#include "platform.h"
#include "internal.h"
#include <ulwm2m.h>

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <cmocka.h>

typedef struct{
  lwm2m_context_t * ctx;
  client_data_t userData;
  uint8_t sessionH;
}test_state_t;

#define TEST_CTX(state) ((test_state_t*)*state)->ctx
#define TEST_CTX_USER_DATA(state) ((test_state_t*)*state)->userData
#define TEST_SESSION_HANDLE(state) ((test_state_t*)*state)->sessionH

static int group_setup(void **state){
  *state = lwm2m_malloc(sizeof(test_state_t));

  TEST_CTX(state) = lwm2m_init(NULL);

  lwm2m_context_t * contextP = TEST_CTX(state);
  lwm2m_client_t * clientP = (lwm2m_client_t*)lwm2m_malloc(sizeof(lwm2m_client_t));
  memset(clientP, 0, sizeof(lwm2m_client_t));
  clientP->internalID = lwm2m_list_newId((lwm2m_list_t *)contextP->clientList);
  clientP->sessionH = &TEST_SESSION_HANDLE(state);
  contextP->clientList = (lwm2m_client_t *)LWM2M_LIST_ADD(contextP->clientList, clientP);
  contextP->nextMID = get_mid();
  return 0;
}

static int group_teardown(void **state){
  lwm2m_close(TEST_CTX(state));
  lwm2m_free(*state);
  return 0;
}

static void blockwise_send(void **state){
  lwm2m_uri_t uri = {.flag = LWM2M_URI_FLAG_OBJECT_ID | LWM2M_URI_FLAG_INSTANCE_ID | LWM2M_URI_FLAG_RESOURCE_ID, .objectId = 1, .instanceId = 0, .resourceId = 0};
  uint8_t buffer[256] = {"Das soll ein Firmware update sein welches sehr sehr gross ist. Das soll ein Firmware update sein welches sehr sehr gross ist. Das soll ein Firmware update sein welches sehr sehr gross ist. Das soll ein Firmware update sein welches sehr sehr gross ist."};
  uint8_t sent_messages[2][1000 ] = {};
  size_t sent_message_bytes[2] = {};

  lwm2m_dm_write(TEST_CTX(state), 0, &uri, LWM2M_CONTENT_OPAQUE, buffer, sizeof(buffer), NULL, NULL);
  memcpy(sent_messages[0], sent_message, sent_bytes);
  sent_message_bytes[0] = sent_bytes;

  /* create ACK to first message */
  coap_option_data_t ack_options[] = {
    [0] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '1'},
    [1] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] ={.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] ={.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0, 1, 128)}
  };
  coap_t ack = {};
  size_t numbytes = create_coap_req(&ack, COAP_TYPE_ACK, COAP_231_CONTINUE, ack_options, sizeof(ack_options)/sizeof(coap_option_data_t), NULL);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&ack, numbytes, &TEST_SESSION_HANDLE(state));
  memcpy(sent_messages[1], sent_message, sent_bytes);
  sent_message_bytes[1] = sent_bytes;

  ack_options[3].data.u8 = COAP_BLOCK1_OPTION_DATA(1, 0, 128);
  numbytes = create_coap_req(&ack, COAP_TYPE_ACK, COAP_231_CONTINUE, ack_options, sizeof(ack_options)/sizeof(coap_option_data_t), NULL);
  lwm2m_handle_packet(TEST_CTX(state), (uint8_t*)&ack, numbytes, &TEST_SESSION_HANDLE(state));

  /* check first block sent */
  coap_option_data_t sent_block_options[] = {
    [0] = {.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '1'},
    [1] = {.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [2] = {.option = COAP_OPTION_URI_PATH, .length=1, .data.u8 = '0'},
    [3] = {.option = COAP_OPTION_CONTENT_TYPE, .length=1, .data.u8 = 0x2A}, // content type OPAQUE
    [4] = {.option = COAP_OPTION_BLOCK1, .length=1, .data.u8 = COAP_BLOCK1_OPTION_DATA(0, 1, 128)}
  };
  coap_t expected_block = {};
  create_coap_req(&expected_block, COAP_TYPE_REQ, COAP_PUT, sent_block_options, sizeof(sent_block_options)/sizeof(coap_option_data_t), "Das soll ein Firmware update sein welches sehr sehr gross ist. Das soll ein Firmware update sein welches sehr sehr gross ist. Da");
  assert_true(coap_message_equal((coap_t*)sent_messages[0], &expected_block, sent_message_bytes[0]));

  /* check second block sent */
  sent_block_options[4].data.u8 = COAP_BLOCK1_OPTION_DATA(1, 0, 128);
  memset(&expected_block, 0, sizeof(coap_t));
  create_coap_req(&expected_block, COAP_TYPE_REQ, COAP_PUT, sent_block_options, sizeof(sent_block_options)/sizeof(coap_option_data_t), "s soll ein Firmware update sein welches sehr sehr gross ist. Das soll ein Firmware update sein welches sehr sehr gross ist.");
  assert_true(coap_message_equal((coap_t*)sent_messages[1], &expected_block, sent_message_bytes[1]));
}

static const struct CMUnitTest tests[] = {
  /* read tests */
  cmocka_unit_test(blockwise_send),

};

DEFINE_TEST("Server Blockwise send", tests, group_setup, group_teardown);
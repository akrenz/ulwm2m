/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdint.h>
#include <stddef.h>

/* buffer to store the message sent via lwm2m_socket_send() */
extern uint8_t sent_message[1000];
/* bytes sent via last lwm2m_socket_send() call */
extern  size_t sent_bytes;

// extern uint8_t *bufferSendBuffer;
// extern size_t bufferSendBufferLength;

#endif /* _PLATFORM_H_ */
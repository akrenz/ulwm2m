/*******************************************************************************
 *
 * Copyright (c) 2025 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "object_power.h"

#include <ulwm2m.h>

static uint8_t prv_get_value(lwm2m_data_t * data, lwm2m_power_instance_data_t * inst_data){
  uint8_t ret = COAP_205_CONTENT;
  switch(data->id)
  {
    case LWM2M_POWER_SENSOR_VALUE_ID:
    {
      if(inst_data->power) {
        lwm2m_data_encode_float(inst_data->power(), data);
      }else{
        ret = COAP_404_NOT_FOUND;
      }
      break;
    }

    case LWM2M_POWER_SENSOR_UNITS_ID:
    {
      if(inst_data->units != NULL){
        lwm2m_data_encode_string(inst_data->units, data);
      }else{
        ret = COAP_404_NOT_FOUND;
      }
      break;
    }

    default:
    {
      ret = COAP_404_NOT_FOUND;
    }
  }

  return ret;
  
}

static uint8_t prv_set_value(lwm2m_data_t * data, lwm2m_power_instance_data_t * inst_data){
  uint8_t ret = COAP_405_METHOD_NOT_ALLOWED;
  switch(data->id)
  {
    case LWM2M_POWER_SENSOR_VALUE_ID:
    case LWM2M_POWER_SENSOR_UNITS_ID:
    {
      break;
    }

    default:
    {
      ret = COAP_404_NOT_FOUND;
    }
  }

  return ret;
  
}

uint8_t lwm2m_power_read(uint16_t instanceId, int *num_data, lwm2m_data_t **dataArrayP, lwm2m_object_t *objectP)
{
  uint8_t ret = COAP_404_NOT_FOUND;
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
  if (NULL == inst)
  {
      return COAP_404_NOT_FOUND;
  }
  lwm2m_power_instance_data_t * inst_data = (lwm2m_power_instance_data_t*)inst->data;

  if(*num_data == 0){ /* return all objects */
    if(inst_data->power != NULL) (*num_data)++;
    if(inst_data->units != NULL) (*num_data)++;


    *dataArrayP = lwm2m_data_new(*num_data);
    if (*dataArrayP == NULL) return COAP_500_INTERNAL_SERVER_ERROR;

    lwm2m_data_t* data = *dataArrayP;
    if(inst_data->power){
      data->id = LWM2M_POWER_SENSOR_VALUE_ID;
      data++;
    }

    if(inst_data->units){
      data->id  = LWM2M_POWER_SENSOR_UNITS_ID;
      data++;
    }
  }

  for(size_t i=0; i< *num_data; i++){
    ret = prv_get_value(&(*dataArrayP)[i], inst_data);
    if(ret != COAP_205_CONTENT) break;
  }

  return ret;
}

uint8_t lwm2m_power_write(uint16_t instanceId, int numData, lwm2m_data_t *dataArray, lwm2m_object_t *objectP)
{
  uint8_t ret = COAP_400_BAD_REQUEST;
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
  if (NULL == inst)
  {
      return COAP_404_NOT_FOUND;
  }
  
  if(numData != 0 && dataArray != NULL){
    for(size_t i=0; i<numData; i++){
      if((ret = prv_set_value(&dataArray[i], inst->data)) != COAP_204_CHANGED) break;
    }
  }
  return ret;
}
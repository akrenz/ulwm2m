/*******************************************************************************
 *
 * Copyright (c) 2025 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _OBJECT_POWER_H_
#define _OBJECT_POWER_H_

#include <ulwm2m.h>

/**
 * @brief Ressource ID's for LWM2M Power Object
 * 
 */
#define LWM2M_POWER_OBJECT_ID       3328
#define LWM2M_POWER_SENSOR_VALUE_ID 5700
#define LWM2M_POWER_SENSOR_UNITS_ID 5701

/**
 * @brief Instance Data for a `Power` object identified by LWM2M_FREQUENCY_OBJECT_ID
 *
 * This object should be used to report power measurements. It also provides resources
 * for measurement unit. An example measurement unit is Watt.
 */
typedef struct {
  float  (*power)(void); // Callback to retrieve the current measured power value
  const char * units;           // String containing the Units (e.g. Watt) of the measured power
} lwm2m_power_instance_data_t;

uint8_t lwm2m_power_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t lwm2m_power_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);

#endif /* _OBJECT_POWER_H_ */
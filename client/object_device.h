/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

/** \file object_device.h
 * This File contains the LWM2M Device Object definitions and read/write/execute/etc.
 * methods. The Device object provides device related information:
 *  - Manufacturer String
 *  - Mode Number String
 *  - Serial Number String
 *  - Firmware Version String
 *  - Hardware Version String
 *  - Software Version String
 *  - Reboot callback
 *  - Factory Reset callback
 *  - Available Power Source (DC, Battery, Solar, etc.)
 *  - Power Source Voltage Level
 *  - Power Source Current
 *  - Battery Level (only valid if Available Power Source is Battery)
 *  - Current Battery Status (Charging, Low Battery, etc.)
 *  - Total amount of device Memory 
 *  - Free available Memory
 *  - Current system time
 *  - UTC Offset for system time
 *  - Timezone
 *  - Supported Binding Mode (UDP, Queued UDP, SMS, etc.)
 */
 
#ifndef OBJECT_DEVICE_H_
#define OBJECT_DEVICE_H_

#include <ulwm2m.h>

/**
 * @brief Ressource ID's for LWM2M Device Object
 * 
 */
#define LWM2M_DEVICE_MANUFACTURER_ID          0
#define LWM2M_DEVICE_MODEL_NUMBER_ID          1
#define LWM2M_DEVICE_SERIAL_NUMBER_ID         2
#define LWM2M_DEVICE_FIRMWARE_VERSION_ID      3
#define LWM2M_DEVICE_REBOOT_ID                4
#define LWM2M_DEVICE_FACTORY_RESET_ID         5
#define LWM2M_DEVICE_AVL_POWER_SOURCES_ID     6
#define LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID  7
#define LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID  8
#define LWM2M_DEVICE_BATTERY_LEVEL_ID         9
#define LWM2M_DEVICE_MEMORY_FREE_ID           10
#define LWM2M_DEVICE_ERROR_CODE_ID            11
#define LWM2M_DEVICE_RESET_ERROR_CODE_ID      12
#define LWM2M_DEVICE_CURRENT_TIME_ID          13
#define LWM2M_DEVICE_UTC_OFFSET_ID            14
#define LWM2M_DEVICE_TIMEZONE_ID              15
#define LWM2M_DEVICE_BINDING_MODES_ID         16
#define LWM2M_DEVICE_DEVICE_TYPE_ID           17
#define LWM2M_DEVICE_HARDWARE_VERSION_ID      18
#define LWM2M_DEVICE_SOFTWARE_VERSION_ID      19
#define LWM2M_DEVICE_BATTERY_STATUS_ID        20
#define LWM2M_DEVICE_MEMORY_TOTAL_ID          21

/**
 * @brief Valid Power Source Types for availablePowerSource in lwm2m_device_instance_data_t
 * 
 * The Device object provides device related information:
 *  - Manufacturer String
 *  - Mode Number String
 *  - Serial Number String
 *  - Firmware Version String
 *  - Hardware Version String
 *  - Software Version String
 *  - Reboot callback
 *  - Factory Reset callback
 *  - Available Power Source (DC, Battery, Solar, etc.)
 *  - Power Source Voltage Level
 *  - Power Source Current
 *  - Battery Level (only valid if Available Power Source is Battery)
 *  - Current Battery Status (Charging, Low Battery, etc.)
 *  - Total amount of device Memory 
 *  - Free available Memory
 *  - Current system time
 *  - UTC Offset for system time
 *  - Timezone
 *  - Supported Binding Mode (UDP, Queued UDP, SMS, etc.)
 */
typedef enum {
  LWM2M_DEVICE_POWER_SOURCE_NONE = -1, // Use this if you want to make this resource unavailable to the server.
  LWM2M_DEVICE_POWER_SOURCE_DC = 0,
  LWM2M_DEVICE_POWER_SOURCE_INTERNAL_BAT, // Internal Device Battery
  LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_BAT, // Externally connected battery
  LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_POE, // Power over Ethernet
  LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_USB,
  LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_AC,
  LWM2M_DEVICE_POWER_SOURCE_EXTERNAL_SOLAR,
} lwm2m_device_power_source_t;

/**
 * @brief Return Value of batteryStatusCallback callback in lwm2m_device_instance_data_t
 * 
 */
typedef enum {
  LWM2M_DEVICE_BATTERY_STATUS_NORMAL=0,
  LWM2M_DEVICE_BATTERY_STATUS_CHARGING,
  LWM2M_DEVICE_BATTERY_STATUS_CHARGE_COMPLETE,
  LWM2M_DEVICE_BATTERY_STATUS_DAMAGD,
  LWM2M_DEVICE_BATTERY_STATUS_LOW_BATTERY,
  LWM2M_DEVICE_BATTERY_STATUS_NOT_INSTALLED,
  LWM2M_DEVICE_BATTERY_STATUS_UNKNOWN
} lwm2m_device_battery_status;

/**
 * @brief LWM2M Device Object Isntance Data
 * 
 * Stores information for each Device Object Instance.
 * 
 */
typedef struct {
    const char *    manufacturer; // Optional Parameter. Human readable manufacturer name
    const char *    model; // Optional Parameter. A model identifier (manufacturer specified string)
    const char *    serial; // Optional Parameter. Serial Number of the model
    const char *    firmware; // Optional Parameter. Firmware version string
    void            (*rebootCallback)(void); // Reboot the LwM2M Device to restore the Device from unexpected firmware failure.
    void            (*factoryResetCallback)(void); // Perform factory reset of the LwM2M Device to make the LwM2M Device to go through initial deployment sequence where provisioning and bootstrap sequence is performed.
    lwm2m_device_power_source_t       availablePowerSource; // Optional Parameter. Available Power Source for the device. Use LWM2M_DEVICE_POWER_SOURCES as identifiers. 
    int32_t         powerSourceVoltage; // Optional Parameter. Voltage for Power Source in mV
    int32_t         powerSourceCurrent; // Optional Parameter. Current for Power Source in mA
    lwm2m_binding_t binding;
    int8_t          (*batteryLevelCallback)(void); // Contains the current battery level as a percentage (with a range from 0 to 100).
    int32_t         totalMemory; // Total amount of storage space which can store data and software in the LwM2M Device (expressed in kilobytes).
    int32_t         (*freeMemorySpaceCallback)(void); // Estimated current available amount of storage space (in kilobytes) which can store data and software in the LwM2M Device
    int32_t         (*currentTimeReadCallback)(void); // Current UNIX time of the LwM2M Client. The LwM2M Client should be responsible to increase this time value as every second elapses. 
    int32_t         (*currentTimeWriteCallback)(int32_t); // The LwM2M Server is able to write the current Time to make the LwM2M Client synchronized with the LwM2M Server.
    char            utcOffset[7]; // Indicates the UTC offset currently in effect for this LwM2M Device. This value must have ISO 8601 format
    const char *    timezone; // Indicates in which time zone the LwM2M Device is located, in IANA Timezone (TZ) database format
    const char *    deviceType; // Type of the device (manufacturer specified string: e.g., smart meters / dev Class…)
    const char *    hardwareVersion; // Current hardware version of the device
    const char *    softwareVersion; // Current software version of the device (manufacturer specified string). On elaborated LwM2M device, SW could be split in 2 parts: a firmware one and a higher level software on top.
    lwm2m_device_battery_status (*batteryStatusCallback)(void); // Current Status of the Battery. This should only be set if availablePowerSource indicates a battery.

} lwm2m_device_instance_data_t;

/**
 * @brief Read Function called if Server executes a GET on the whole Device Object or a specific ressource of it.
 * 
 * @param instanceId ID of the instance to query
 * @param numDataP If 0 it will return all available Ressources. Otherwise give the number of ressources you want to query.
 * @param dataArrayP Array with lwm2m_data_t elements containing all ID's (given in lwm2m_data_t::id) of the requested ressources. Size of dataArayP is given in numDataP
 * @param objectP Pointer to object containing the requested instance ID
 * @return COAP_205_CONTENT if all requested values could be found, otherwise COAP_404_NOT_FOUND. If trying to read a ressource which is not readable (like factoryReset Callback) COAP_405_METHOD_NOT_ALLOWED will be returned.
 */
uint8_t lwm2m_device_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);

/**
 * @brief 
 * 
 * @param instanceId 
 * @param numData 
 * @param dataArray 
 * @param objectP 
 * @return uint8_t 
 */
uint8_t lwm2m_device_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);

/**
 * @brief Execute Function called if Server tries to execute a ressource.
 * 
 * Executing a ressource which is not executable will result in a COAP_405_METHOD_NOT_ALLOWED.
 * 
 * @param instanceId ID of the instance to for which a ressource should be executed. Since this is a single Isntance object the only valid InstanceId is 0
 * @param resourceId ID of the ressource to execute. Must be one of LWM2M_DEVICE_*_ID 
 * @param buffer buffer storing data given as input paramter to the execute callback.
 * @param length Length of the buffer in bytes given to the execute callback
 * @param objectP Pointer to object containing the requested instance ID
 * @return COAP_205_CHANGED if requested ressource could be executed, otherwise COAP_404_NOT_FOUND. If trying to execute a ressource which is not executable (like manufacturer string) COAP_405_METHOD_NOT_ALLOWED will be returned.
 */
uint8_t lwm2m_device_execute(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP);

/**
 * @brief 
 * 
 * @param instanceId 
 * @param numDataP 
 * @param dataArrayP 
 * @param objectP 
 * @return uint8_t 
 */
uint8_t lwm2m_device_discover(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);

#endif /* OBJECT_DEVICE_H_ */
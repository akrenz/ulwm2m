/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
 
#ifndef OBJECT_SECURITY_H_
#define OBJECT_SECURITY_H_

#include <ulwm2m.h>

/**
 * @brief Data for a LWM2M Security Object
 * 
 * This LWM2MObject provides the keying material of a LwM2MClient appropriate
 * to access a specified LwM2MServer. One Object Instance SHOULD address a
 * LWM2MBootstrap-Server
 * 
 */
typedef struct{
    const char *                 uri;               // Uniquely identifies the LwM2MServer or LwM2MBootstrap-Server. 
    bool                         isBootstrap;       // Determines if the current instance concerns a LwM2MBootstrap-Server (true) or a standard LwM2MServer (false)
    uint8_t                      securityMode;      // Determines which UDP payload security mode is used. Use enum lwm2m_security_mode_t
    void *                       keyOrIdentity;     // Stores the LwM2MClient’s Certificate (Certificate mode), public key (RPK mode) or PSK Identity (PSK mode)
    void *                       serverPublicKey;   // Stores the LwM2MServer’s or LwM2MBootstrap-Server’s Certificate (Certificate mode), public key (RPK mode)
    void *                       secretKey;         // Stores the secret key or private key of the security mode.
    uint16_t                     shortID;           // This identifier uniquely identifies each LwM2MServer configured for the LwM2MClient.This Resource MUST be set when the Bootstrap-Server Resource has false value.Specific ID:0 and ID:65535 values MUST NOT be used for identifying the LwM2MServer
    uint32_t                     clientHoldOffTime; // Relevant information for a Bootstrap-Server only.The number of seconds to wait before initiating a Client Initiated Bootstrap once the LwM2MClient has determined it should initiate this bootstrap mode.
    uint32_t                     BootstrapServerAccountTimeout; // The LwM2MClient MUST purge the LwM2MBootstrap-Server Account after the timeout value given by this resource. The lowest timeout value is 1.If the value is set to 0, or if this resource is not instantiated,the Bootstrap-Server Account lifetime is infinite.
} lwm2m_security_instance_data_t;

/**
 * @brief Get the security uri object
 * 
 * @param objectP 
 * @param secObjInstID 
 * @return char* 
 */
const char * get_security_uri(const lwm2m_object_t * objectP, const uint16_t secObjInstID);

uint8_t lwm2m_security_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArray, lwm2m_object_t * objectP);

#endif /* OBJECT_SECURITY_H_ */
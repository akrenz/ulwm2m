/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef OBJECT_DIGITAL_OUTPUT_H_
#define OBJECT_DIGITAL_OUTPUT_H_

#include <ulwm2m.h>

/**
 * @brief Ressource ID's for LWM2M Device Object
 * 
 */
#define LWM2M_DIGITAL_OUTPUT_OBJECT_ID       3201
#define LWM2M_DIGITAL_OUTPUT_STATE_ID        5550
#define LWM2M_DIGITAL_OUTPUT_POLARITY_ID     5551
#define LWM2M_DIGITAL_OUTPUT_TYPE_ID         5552

/**
 * @brief Instance Data for a `Digital Output` object identified by LWM2M_DIGITAL_OUTPUT_OBJECT_ID
 *
 * This LwM2M Objects provides the data related to a Digital Output for non-specific actuators 
 */
typedef struct {
    uint8_t     (*writeOutput)(bool);   // Callback for write Operation on output
    bool        (*readOutput)();        // Callback for read Operation on output. This should return the current output value
    bool        polarity;               // The polarity of the digital output as a Boolean (false = Normal, true = Reversed). 
    const char* type;                   // The application type of the sensor or actuator as a string depending on the use case. According to specification write should be allowed. But write is not implemented and therefor writing this object returns an error.
} lwm2m_digital_output_instance_data_t;

uint8_t lwm2m_digital_output_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t lwm2m_digital_output_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);

#endif /* OBJECT_DIGITAL_OUTPUT_H_ */
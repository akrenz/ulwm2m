/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _OBJECT_FIRMWARE_UPDATE_H_
#define _OBJECT_FIRMWARE_UPDATE_H_

#include <ulwm2m.h>

/**
 * @brief Ressource ID's for LWM2M Firmware Update Object
 * 
 */
#define LWM2M_FIRMWARE_UPDATE_PACKAGE_ID 0
#define LWM2M_FIRMWARE_UPDATE_PACKAGE_URI_ID 1
#define LWM2M_FIRMWARE_UPDATE_UPDATE_ID 2
#define LWM2M_FIRMWARE_UPDATE_STATE_ID 3
#define LWM2M_FIRMWARE_UPDATE_UPDATE_RESULT_ID 5
#define LWM2M_FIRMWARE_UPDATE_PKG_NAME_ID 6
#define LWM2M_FIRMWARE_UPDATE_PKG_VERSION_ID 7
#define LWM2M_FIRMWARE_UPDATE_PROTOCOL_SUPPORT_ID 8
#define LWM2M_FIRMWARE_UPDATE_DELIVERY_METHOD_ID 9

typedef enum
{
    LWM2M_FWUP_STATE_IDLE = 0,
    LWM2M_FWUP_STATE_DOWNLOADING = 1,
    LWM2M_FWUP_STATE_DOWNLOADED = 2,
    LWM2M_FWUP_STATE_UPDATING = 3
} lwm2m_firmware_update_state_t;

typedef enum
{
    LWM2M_FWUP_RESULT_INIT = 0,
    LWM2M_FWUP_RESULT_SUCCESS = 1,
    LWM2M_FWUP_RESULT_NOT_ENOUGH_FLASH = 2,
    LWM2M_FWUP_RESULT_OUT_OF_RAM = 3,
    LWM2M_FWUP_RESULT_CONNECTION_LOST = 4,
    LWM2M_FWUP_RESULT_INTEGRITY_CHECK_FAIL = 5,
    LWM2M_FWUP_RESULT_UNSUPPORTED_PACKAGE_TYPE = 6,
    LWM2M_FWUP_RESULT_INVALID_URI = 7,
    LWM2M_FWUP_RESULT_FAILED = 8,
    LWM2M_FWUP_RESULT_UNSUPPORTED_PROTOCOL = 9
} lwm2m_firmware_update_result_t;


/**
 * @brief Instance Data for a `Firmware Update` object identified by LWM2M_FIRMWARE_UPDATE_OBJECT_ID
 *
 */
typedef struct {
  uint32_t (*write_firmware)(uint8_t * buffer, int length, uint32_t block_num, uint8_t block_more); // Callback which is called if block data is written to node LWM2M_FIRMWARE_UPDATE_PACKAGE_ID )
  uint32_t (*execute_update)(void);       // Callback for executing a previous downloaded update. Must return 0 on success, and 1 otherwise.
  char uri[255];                       // URI from where the device can download the firmware package by an alternative mechanism. As soon the device has received the Package URI it performs the download at the next practical opportunity
  lwm2m_firmware_update_state_t state;    // Indicates current state with respect to this firmware update. This value is set by the LwM2M Client. 0: Idle (before downloading or after successful updating) 1: Downloading (The data sequence is on the way) 2: Downloaded 3: Updating If writing the firmware package to Package Resource is done, or, if the device has downloaded the firmware package from the Package URI the state changes to Downloaded.
  lwm2m_firmware_update_result_t result;  // Contains the result of downloading or updating the firmware
  const char* pkg_name;
  const char* pkg_version;
} lwm2m_firmware_update_instance_data_t;

uint8_t lwm2m_firmware_update_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t lwm2m_firmware_update_write(lwm2m_uri_t * uriP, lwm2m_media_type_t format, uint8_t * buffer, int length, lwm2m_object_t * objectP, uint32_t block_num, uint8_t block_more);
uint8_t lwm2m_firmware_update_execute(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP);

#endif /* _OBJECT_FIRMWARE_UPDATE_H_ */
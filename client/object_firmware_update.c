/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "object_firmware_update.h"

static bool object_has_instance(lwm2m_object_t * objectP, uint16_t id){
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, id);
  if (NULL == inst)
  {
    return false;
  }

  return true;
}

static uint8_t read_nodeid(lwm2m_data_t* data, const lwm2m_firmware_update_instance_data_t* inst){
  uint8_t ret = COAP_404_NOT_FOUND;
  switch(data->id){
    case LWM2M_FIRMWARE_UPDATE_STATE_ID:
    {
      lwm2m_data_encode_int(inst->state, data);
      ret = COAP_205_CONTENT;
      break;
    }
    case LWM2M_FIRMWARE_UPDATE_DELIVERY_METHOD_ID:
    {
      lwm2m_data_encode_int(1, data);
      ret = COAP_205_CONTENT;
      break;
    }
    case LWM2M_FIRMWARE_UPDATE_UPDATE_RESULT_ID:
    {
      lwm2m_data_encode_int(inst->result, data);
      ret = COAP_205_CONTENT;
      break;
    }

    case LWM2M_FIRMWARE_UPDATE_PACKAGE_URI_ID:
    {
      lwm2m_data_encode_string(inst->uri, data);
      ret = COAP_205_CONTENT;
      break;
    }

    case LWM2M_FIRMWARE_UPDATE_PKG_NAME_ID:
    {
      if(inst->pkg_name != NULL){
        lwm2m_data_encode_string(inst->pkg_name, data);
        ret = COAP_205_CONTENT;
      }
      break;
    }

    case LWM2M_FIRMWARE_UPDATE_PKG_VERSION_ID:
    {
      if(inst->pkg_version != NULL){
        lwm2m_data_encode_string(inst->pkg_version, data);
        ret = COAP_205_CONTENT;
      }
      break;
    }

    default:
    {
      ret = COAP_405_METHOD_NOT_ALLOWED;
      break;
    }
  }

  return ret;
}

static uint8_t available_resource_ids(const lwm2m_firmware_update_instance_data_t * data, uint8_t avlResourceIds[]){
  uint8_t numData = 0;

  avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_STATE_ID;
  numData++;

  avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_DELIVERY_METHOD_ID;
  numData++;

  avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_UPDATE_RESULT_ID;
  numData++;

  if(data->uri[0] != 0){
    avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_PACKAGE_URI_ID;
    numData++;
  }
  if(data->pkg_name != NULL){
    avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_PKG_NAME_ID;
    numData++;
  }
  if(data->pkg_version != NULL){
    avlResourceIds[numData] = LWM2M_FIRMWARE_UPDATE_PKG_VERSION_ID;
    numData++;
  }

  return numData;
}

uint8_t lwm2m_firmware_update_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP){
  if (!object_has_instance(objectP, instanceId))
  {
      return COAP_404_NOT_FOUND;
  }

  lwm2m_firmware_update_instance_data_t* inst = ((lwm2m_instance_t*)LWM2M_LIST_FIND(objectP->instanceList, instanceId))->data;

  uint8_t ids[6] = {};
  if(*numDataP == 0){
    *numDataP = available_resource_ids(inst, ids);
    *dataArrayP = lwm2m_data_new(*numDataP);
    for(int i=0; i<*numDataP;i++){
        (*dataArrayP)[i].id = ids[i];
      }
  }

  for(int i=0; i<*numDataP;i++){
    uint8_t ret = read_nodeid(&(*dataArrayP)[i], inst);
    if(ret != COAP_205_CONTENT) return ret;
  }

  return COAP_205_CONTENT;
}

uint8_t lwm2m_firmware_update_write(lwm2m_uri_t * uriP, lwm2m_media_type_t format, uint8_t * buffer, int length, lwm2m_object_t * objectP, uint32_t block_num, uint8_t block_more){
  if (!object_has_instance(objectP, uriP->instanceId))
  {
      return COAP_404_NOT_FOUND;
  }

  lwm2m_firmware_update_instance_data_t* inst = ((lwm2m_instance_t*)LWM2M_LIST_FIND(objectP->instanceList, uriP->instanceId))->data;
  inst->result = LWM2M_FWUP_RESULT_INIT;
  inst->write_firmware(buffer, length, block_num, block_more);
  if(block_more != 0){
    inst->state = LWM2M_FWUP_STATE_DOWNLOADING;
    return COAP_231_CONTINUE;
  }else{
    inst->state = LWM2M_FWUP_STATE_DOWNLOADED;
    return COAP_204_CHANGED;
  }
}

uint8_t lwm2m_firmware_update_execute(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP){
  lwm2m_firmware_update_instance_data_t* inst = ((lwm2m_instance_t*)LWM2M_LIST_FIND(objectP->instanceList, instanceId))->data;
  if(inst->state != LWM2M_FWUP_STATE_DOWNLOADED){
    return COAP_400_BAD_REQUEST;
  }

  if(resourceId != LWM2M_FIRMWARE_UPDATE_UPDATE_ID) return COAP_405_METHOD_NOT_ALLOWED;

  if(buffer != NULL || length != 0) return COAP_400_BAD_REQUEST;
  
  if(inst->execute_update() != 0){
    inst->state = LWM2M_FWUP_STATE_IDLE;
    inst->result = LWM2M_FWUP_RESULT_FAILED;

  }else{
    inst->state = LWM2M_FWUP_STATE_UPDATING;
  }
  return COAP_204_CHANGED;
}

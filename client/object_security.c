/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

/*
 * Here we implement a very basic LWM2M Security Object which only knows NoSec security mode.
 */

#include "object_security.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


// static uint16_t next_security_instance_id = 0;

static uint8_t prv_get_value(lwm2m_data_t * dataP,
                             lwm2m_security_instance_data_t * targetP)
{

    switch (dataP->id)
    {
    case LWM2M_SECURITY_URI_ID:
        lwm2m_data_encode_string(targetP->uri, dataP);
        break;

    case LWM2M_SECURITY_BOOTSTRAP_ID:
        lwm2m_data_encode_bool(targetP->isBootstrap, dataP);
        break;

    case LWM2M_SECURITY_SECURITY_ID:
        lwm2m_data_encode_int(targetP->securityMode, dataP);
        break;

    case LWM2M_SECURITY_PUBLIC_KEY_ID:
        {
            lwm2m_data_encode_opaque(targetP->keyOrIdentity, 1, dataP);
        }
        break;

    case LWM2M_SECURITY_SERVER_PUBLIC_KEY_ID:
        // Here we return an opaque of 1 byte containing 0
        {
            lwm2m_data_encode_opaque(targetP->serverPublicKey, 1, dataP);
        }
        break;

    case LWM2M_SECURITY_SECRET_KEY_ID:
        // Here we return an opaque of 1 byte containing 0
        {
            lwm2m_data_encode_opaque(targetP->secretKey, 1, dataP);
        }
        break;

    case LWM2M_SECURITY_SHORT_SERVER_ID:
        lwm2m_data_encode_int(targetP->shortID, dataP);
        break;

    case LWM2M_SECURITY_HOLD_OFF_ID:
        lwm2m_data_encode_int(targetP->clientHoldOffTime, dataP);
        break;

    case LWM2M_SECURITY_BOOTSTRAP_TIMEOUT_ID:
        lwm2m_data_encode_int(targetP->BootstrapServerAccountTimeout, dataP);
        break;

    default:
        return COAP_404_NOT_FOUND;
    }

    return COAP_205_CONTENT;
}

uint8_t lwm2m_security_read(uint16_t instanceId,
                                 int * numDataP,
                                 lwm2m_data_t ** dataArrayP,
                                 lwm2m_object_t * objectP)
{
    uint8_t result;
    int i;

    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP) return COAP_404_NOT_FOUND;
    lwm2m_security_instance_data_t * data = targetP->data;

    // is the server asking for the full instance ?
    if (*numDataP == 0)
    {
        uint16_t resList[] = {LWM2M_SECURITY_URI_ID,
                              LWM2M_SECURITY_BOOTSTRAP_ID,
                              LWM2M_SECURITY_SECURITY_ID,
                              LWM2M_SECURITY_PUBLIC_KEY_ID,
                              LWM2M_SECURITY_SERVER_PUBLIC_KEY_ID,
                              LWM2M_SECURITY_SECRET_KEY_ID,
                              LWM2M_SECURITY_SHORT_SERVER_ID,
                              LWM2M_SECURITY_HOLD_OFF_ID};
        int nbRes = sizeof(resList)/sizeof(uint16_t);

        *dataArrayP = lwm2m_data_new(nbRes);
        if (*dataArrayP == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
        *numDataP = nbRes;
        for (i = 0 ; i < nbRes ; i++)
        {
            (*dataArrayP)[i].id = resList[i];
        }
    }

    i = 0;
    do
    {
        result = prv_get_value((*dataArrayP) + i, data);
        i++;
    } while (i < *numDataP && result == COAP_205_CONTENT);

    return result;
}

const char * get_security_uri(const lwm2m_object_t * objectP, const uint16_t secObjInstID)
{
    lwm2m_instance_t * targetP = (lwm2m_instance_t *)LWM2M_LIST_FIND(objectP->instanceList, secObjInstID);

    if (NULL != targetP)
    {
        lwm2m_security_instance_data_t * data = targetP->data;
        return data->uri;
    }

    return NULL;
}

/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#ifndef _OBJECT_FREQUENCY_H_
#define _OBJECT_FREQUENCY_H_

#include <ulwm2m.h>

/**
 * @brief Ressource ID's for LWM2M Frequency Object
 * 
 */
#define LWM2M_FREQUENCY_OBJECT_ID       3318
#define LWM2M_FREQUENCY_SENSOR_VALUE_ID 5700
#define LWM2M_FREQUENCY_SENSOR_UNITS_ID 5701

/**
 * @brief Instance Data for a `Frequency` object identified by LWM2M_FREQUENCY_OBJECT_ID
 *
 * This object should be used to report frequency measurements. It also provides resources
 * for measurement unit. An example measurement unit is hertz.
 */
typedef struct {
  float  (*frequency)(void); // Callback to retrieve the current measured frequency value
  const char * units;           // String containing the Units (e.g. Hz) of the measured frequency
} lwm2m_frequency_instance_data_t;

uint8_t lwm2m_frequency_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t lwm2m_frequency_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);

#endif /* _OBJECT_FREQUENCY_H_ */
/*******************************************************************************
 *
 * Copyright (c) 2023 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "object_digital_output.h"

#include <memory.h>

static uint8_t prv_get_value(lwm2m_data_t * data,
                             lwm2m_digital_output_instance_data_t * inst_data)
{
    switch (data->id)
    {
    case LWM2M_DIGITAL_OUTPUT_STATE_ID:
    {
      lwm2m_data_encode_bool(inst_data->readOutput(), data);
      break;
    }

    case LWM2M_DIGITAL_OUTPUT_POLARITY_ID:
    {
      lwm2m_data_encode_bool(inst_data->polarity, data);
      break;
    }
    
    case LWM2M_DIGITAL_OUTPUT_TYPE_ID:
    {
      lwm2m_data_encode_string(inst_data->type, data);
      break;
    }

    default:
    {

    }
        return COAP_404_NOT_FOUND;
    }

    return COAP_205_CONTENT;
}

uint8_t lwm2m_digital_output_read(uint16_t instanceId, int * num_data, lwm2m_data_t ** data_array, lwm2m_object_t * obj){
  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(obj->instanceList, instanceId);
  if (NULL == inst)
  {
      return COAP_404_NOT_FOUND;
  }
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  if(*num_data == 0){ /* request all objects */
    // check if type id has non-null string
    if(data->type != NULL){
      *data_array = lwm2m_data_new(3);
      if (*data_array == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
      (*data_array)[0].id = LWM2M_DIGITAL_OUTPUT_STATE_ID;
      (*data_array)[1].id = LWM2M_DIGITAL_OUTPUT_POLARITY_ID;
      (*data_array)[2].id = LWM2M_DIGITAL_OUTPUT_TYPE_ID;
      *num_data = 3;
    }else{
      *data_array = lwm2m_data_new(2);
      if (*data_array == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
      (*data_array)[0].id = LWM2M_DIGITAL_OUTPUT_STATE_ID;
      (*data_array)[1].id = LWM2M_DIGITAL_OUTPUT_POLARITY_ID;
      *num_data = 2;
    }
  }

  for(int i=0; i< *num_data; i++){
    uint8_t ret = prv_get_value(&(*data_array)[i], data);
    if(ret != COAP_205_CONTENT){
      return ret;
    }
  }

  return COAP_205_CONTENT;
}

uint8_t lwm2m_digital_output_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP){
  uint8_t result = COAP_404_NOT_FOUND;

  lwm2m_instance_t * inst = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
  if (NULL == inst)
  {
      return COAP_404_NOT_FOUND;
  }
  lwm2m_digital_output_instance_data_t * data = (lwm2m_digital_output_instance_data_t*)inst->data;

  for(int i=0; i< numData;i++){
    switch(dataArray[i].id){
      case LWM2M_DIGITAL_OUTPUT_STATE_ID:
      {
        bool b = false;
        lwm2m_data_decode_bool(&dataArray[i], &b);
        result = data->writeOutput(b ^ data->polarity);
        break;
      }

      case LWM2M_DIGITAL_OUTPUT_POLARITY_ID:
      {
        if(lwm2m_data_decode_bool(&dataArray[i], &data->polarity) != 1){
          result = COAP_400_BAD_REQUEST;
        }else{
          result = COAP_NO_ERROR;
        }
        break;
      }

      case LWM2M_DIGITAL_OUTPUT_TYPE_ID:
      {
        result = COAP_400_BAD_REQUEST;
        break;
      }

      default:
      {
        result = COAP_404_NOT_FOUND;
        break;
      }
    }
  }
  return result;
}
/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

#include "object_server.h"

#include <internal.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char * LWM2M_BINDING_STRING[7] = { "",
                                                "U",
                                                "UQ",
                                                "S",
                                                "SQ",
                                                "US",
                                                "UQS"
};

static uint8_t prv_get_value(lwm2m_data_t * dataP,
                             lwm2m_server_instance_data_t * data)
{
    switch (dataP->id)
    {
    case LWM2M_SERVER_SHORT_ID_ID:
        lwm2m_data_encode_int(data->shortServerId, dataP);
        break;

    case LWM2M_SERVER_LIFETIME_ID:
        lwm2m_data_encode_int(data->lifetime, dataP);
        break;

    case LWM2M_SERVER_MIN_PERIOD_ID:
        lwm2m_data_encode_int(data->defaultMinObservationPeriod, dataP);
        break;

    case LWM2M_SERVER_MAX_PERIOD_ID:
        lwm2m_data_encode_int(data->defaultMaxObservationPeriod, dataP);
        break;

    case LWM2M_SERVER_DISABLE_ID:
        return COAP_405_METHOD_NOT_ALLOWED;

    case LWM2M_SERVER_STORING_ID:
        lwm2m_data_encode_bool(data->storing, dataP);
        break;

    case LWM2M_SERVER_BINDING_ID:
        lwm2m_data_encode_string(LWM2M_BINDING_STRING[data->binding], dataP);
        break;

    case LWM2M_SERVER_UPDATE_ID:
        return COAP_405_METHOD_NOT_ALLOWED;

    default:
        return COAP_404_NOT_FOUND;
    }

    return COAP_205_CONTENT;
}

uint8_t lwm2m_server_read(uint16_t instanceId,
                               int * numDataP,
                               lwm2m_data_t ** dataArrayP,
                               lwm2m_object_t * objectP)
{
    uint8_t result;
    int i;

    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP) return COAP_404_NOT_FOUND;
    lwm2m_server_instance_data_t * data = (lwm2m_server_instance_data_t*)targetP->data;
    // is the server asking for the full instance ?
    if (*numDataP == 0)
    {
        uint16_t resList[] = {
                LWM2M_SERVER_SHORT_ID_ID,
                LWM2M_SERVER_LIFETIME_ID,
                LWM2M_SERVER_MIN_PERIOD_ID,
                LWM2M_SERVER_MAX_PERIOD_ID,
                LWM2M_SERVER_STORING_ID,
                LWM2M_SERVER_BINDING_ID
        };
        int nbRes = sizeof(resList)/sizeof(uint16_t);

        *dataArrayP = lwm2m_data_new(nbRes);
        if (*dataArrayP == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
        *numDataP = nbRes;
        for (i = 0 ; i < nbRes ; i++)
        {
            (*dataArrayP)[i].id = resList[i];
        }
    }

    i = 0;
    do
    {
        result = prv_get_value((*dataArrayP) + i, data);
        i++;
    } while (i < *numDataP && result == COAP_205_CONTENT);

    return result;
}

uint8_t lwm2m_server_discover(uint16_t instanceId,
                                   int * numDataP,
                                   lwm2m_data_t ** dataArrayP,
                                   lwm2m_object_t * objectP)
{
    uint8_t result;
    int i;

    result = COAP_205_CONTENT;

    // is the server asking for the full object ?
    if (*numDataP == 0)
    {
        uint16_t resList[] = {
            LWM2M_SERVER_SHORT_ID_ID,
            LWM2M_SERVER_LIFETIME_ID,
            LWM2M_SERVER_MIN_PERIOD_ID,
            LWM2M_SERVER_MAX_PERIOD_ID,
            LWM2M_SERVER_DISABLE_ID,
            LWM2M_SERVER_TIMEOUT_ID,
            LWM2M_SERVER_STORING_ID,
            LWM2M_SERVER_BINDING_ID,
            LWM2M_SERVER_UPDATE_ID
        };
        int nbRes = sizeof(resList)/sizeof(uint16_t);

        *dataArrayP = lwm2m_data_new(nbRes);
        if (*dataArrayP == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
        *numDataP = nbRes;
        for (i = 0 ; i < nbRes ; i++)
        {
            (*dataArrayP)[i].id = resList[i];
        }
    }
    else
    {
        for (i = 0; i < *numDataP && result == COAP_205_CONTENT; i++)
        {
            switch ((*dataArrayP)[i].id)
            {
            case LWM2M_SERVER_SHORT_ID_ID:
            case LWM2M_SERVER_LIFETIME_ID:
            case LWM2M_SERVER_MIN_PERIOD_ID:
            case LWM2M_SERVER_MAX_PERIOD_ID:
            case LWM2M_SERVER_DISABLE_ID:
            case LWM2M_SERVER_TIMEOUT_ID:
            case LWM2M_SERVER_STORING_ID:
            case LWM2M_SERVER_BINDING_ID:
            case LWM2M_SERVER_UPDATE_ID:
                break;
            default:
                result = COAP_404_NOT_FOUND;
            }
        }
    }

    return result;
}

static uint8_t prv_set_int_value(lwm2m_data_t * dataArray,
                                 uint32_t * data)
{
    uint8_t result;
    int64_t value;

    if (1 == lwm2m_data_decode_int(dataArray, &value))
    {
        if (value >= 0 && value <= 0xFFFFFFFF)
        {
            *data = value;
            result = COAP_204_CHANGED;
        }
        else
        {
            result = COAP_406_NOT_ACCEPTABLE;
        }
    }
    else
    {
        result = COAP_400_BAD_REQUEST;
    }
    return result;
}

#define LWM2M_DATA_BUFFER_IS_STRING(_buffer, _string) strncmp((char*)_buffer.value.asBuffer.buffer, _string,  _buffer.value.asBuffer.length)
uint8_t lwm2m_server_write(uint16_t instanceId,
                                int numData,
                                lwm2m_data_t * dataArray,
                                lwm2m_object_t * objectP)
{
    int i;
    uint8_t result = COAP_404_NOT_FOUND;

    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP)
    {
        return COAP_404_NOT_FOUND;
    }
    lwm2m_server_instance_data_t * data = (lwm2m_server_instance_data_t*)targetP->data;

    i = 0;
    do
    {
        switch (dataArray[i].id)
        {
        case LWM2M_SERVER_SHORT_ID_ID:
        {
            /* writing server short id not allowed according to OMA Specification E.2 */
            return COAP_400_BAD_REQUEST;
            break;
        }

        case LWM2M_SERVER_LIFETIME_ID:
            result = prv_set_int_value(dataArray + i, (uint32_t *)&(data->lifetime));
            break;

        case LWM2M_SERVER_MIN_PERIOD_ID:
            result = prv_set_int_value(dataArray + i, (uint32_t*)&(data->defaultMinObservationPeriod));
            break;

        case LWM2M_SERVER_MAX_PERIOD_ID:
            result = prv_set_int_value(dataArray + i, (uint32_t*)&(data->defaultMaxObservationPeriod));
            break;

        case LWM2M_SERVER_DISABLE_ID:
            result = COAP_405_METHOD_NOT_ALLOWED;
            break;

        case LWM2M_SERVER_STORING_ID:
        {
            bool value;

            if (1 == lwm2m_data_decode_bool(dataArray + i, &value))
            {
                data->storing = value;
                result = COAP_204_CHANGED;
            }
            else
            {
                result = COAP_400_BAD_REQUEST;
            }
        }
        break;

        case LWM2M_SERVER_BINDING_ID:
            if ((dataArray[i].type == LWM2M_TYPE_STRING || dataArray[i].type == LWM2M_TYPE_OPAQUE)
             && dataArray[i].value.asBuffer.length > 0 && dataArray[i].value.asBuffer.length <= 3){
                 if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "U") == 0){
                    data->binding = BINDING_U;
                    result = COAP_204_CHANGED;
                 }else if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "UQ") == 0){
                    data->binding = BINDING_UQ;
                    result = COAP_204_CHANGED;
                 }else if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "S") == 0){
                    data->binding = BINDING_S;
                    result = COAP_204_CHANGED;
                 }else if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "SQ") == 0){
                    data->binding = BINDING_SQ;
                    result = COAP_204_CHANGED;
                 }else if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "US") == 0){
                    data->binding = BINDING_US;
                    result = COAP_204_CHANGED;
                 }else if(LWM2M_DATA_BUFFER_IS_STRING(dataArray[i], "UQS") == 0){
                    data->binding = BINDING_UQS;
                    result = COAP_204_CHANGED;
                 }else{
                     result = COAP_400_BAD_REQUEST;
                }
             }
            else
            {
                result = COAP_400_BAD_REQUEST;
            }
            break;

        case LWM2M_SERVER_UPDATE_ID:
            result = COAP_405_METHOD_NOT_ALLOWED;
            break;

        default:
            result = COAP_404_NOT_FOUND;
            break;
        }
        i++;
    } while (i < numData && result == COAP_204_CHANGED);

    return result;
}

uint8_t lwm2m_server_execute(uint16_t instanceId,
                                  uint16_t resourceId,
                                  uint8_t * buffer,
                                  int length,
                                  lwm2m_object_t * objectP)

{
    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP) return COAP_404_NOT_FOUND;

    switch (resourceId)
    {
    case LWM2M_SERVER_DISABLE_ID:
        // executed in core, if COAP_204_CHANGED is returned
        return COAP_204_CHANGED;

    case LWM2M_SERVER_UPDATE_ID:
        // executed in core, if COAP_204_CHANGED is returned
        return COAP_204_CHANGED;

    default:
        return COAP_405_METHOD_NOT_ALLOWED;
    }
}

uint8_t lwm2m_server_delete(uint16_t id,
                                 lwm2m_object_t * objectP)
{
    lwm2m_instance_t * serverInstance;

    objectP->instanceList = lwm2m_list_remove(objectP->instanceList, id, (lwm2m_list_t **)&serverInstance);
    if (NULL == serverInstance) return COAP_404_NOT_FOUND;

    lwm2m_free(serverInstance);

    return COAP_202_DELETED;
}

uint8_t lwm2m_server_create(uint16_t instanceId,
                                 int numData,
                                 lwm2m_data_t * dataArray,
                                 lwm2m_object_t * objectP)
{
    uint8_t result;

    lwm2m_server_instance_data_t * data = lwm2m_malloc(sizeof(lwm2m_server_instance_data_t));
    lwm2m_instance_t * serverInstance = lwm2m_create_instance(instanceId, data, NULL, true);

    objectP->instanceList = LWM2M_LIST_ADD(objectP->instanceList, serverInstance);

    result = lwm2m_server_write(instanceId, numData, dataArray, objectP);

    if (result != COAP_204_CHANGED)
    {
        (void)lwm2m_server_delete(instanceId, objectP);
    }
    else
    {
        result = COAP_201_CREATED;
    }

    return result;
}
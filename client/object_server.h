/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */
 
#ifndef OBJECT_SERVER_H_
#define OBJECT_SERVER_H_

#include <ulwm2m.h>
/**
 * @brief Instance Data for a server object identified by LWM2M_SERVER_OBJECT_ID
 *
 * This LwM2M Objects provides the data related to a LwM2M Server. A
 * Bootstrap-Server has no such an Object Instance associated to it. 
 */
typedef struct {
    uint16_t        shortServerId; // Used as link to associate server Object Instance. MUST be equal to a shortID of an instance in the security Object.
    uint32_t        lifetime;   // Specify the lifetime of the registration in seconds
    uint32_t        defaultMinObservationPeriod; // The default value the LwM2M Client should use for the Minimum Period of an Observation in the absence of this parameter being included in an Observation. If this Resource doesn’t exist, the default value is 0.
    uint32_t        defaultMaxObservationPeriod; // The default value the LwM2M Client should use for the Maximum Period of an Observation in the absence of this parameter being included in an Observation.
    bool            storing;    // If true, the LwM2M Client stores “Notify” operations to the LwM2M Server while the LwM2M Server account is disabled or the LwM2M Client is offline. After the LwM2M Server account is enabled or the LwM2M Client is online, the LwM2M Client reports the stored “Notify” operations to the Server. If false, the LwM2M Client discards all the “Notify” operations or temporarily disables the Observe function while the LwM2M Server is disabled or the LwM2M Client is offline.
    lwm2m_binding_t binding; // Use enum lwm2m_binding_t. Defines the transport binding configured for the LwM2M Client. If the LwM2M Client supports the binding specified in this Resource, the LwM2M Client MUST use that transport for the Current Binding Mode.
} lwm2m_server_instance_data_t;


uint8_t lwm2m_server_read(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);
uint8_t lwm2m_server_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);
uint8_t lwm2m_server_execute(uint16_t instanceId, uint16_t resourceId, uint8_t * buffer, int length, lwm2m_object_t * objectP);
/**
 * @brief Callback for creating a new server instance
 * 
 * @param instanceId 
 * @param numData 
 * @param dataArray 
 * @param objectP 
 * @return uint8_t 
 */
uint8_t lwm2m_server_create(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP);
uint8_t lwm2m_server_delete(uint16_t id, lwm2m_object_t * objectP);
uint8_t lwm2m_server_discover(uint16_t instanceId, int * numDataP, lwm2m_data_t ** dataArrayP, lwm2m_object_t * objectP);

#endif /* OBJECT_SERVER_H_ */
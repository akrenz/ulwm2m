/*******************************************************************************
 *
 * Copyright (c) 2020 Albert Krenz
 * 
 * This code is licensed under BSD + Patent (see LICENSE.txt for full license text)
 *******************************************************************************/

 /* SPDX-License-Identifier: BSD-2-Clause-Patent */

/*
 * This object is single instance only, and is mandatory to all LWM2M device as it describe the object such as its
 * manufacturer, model, etc...
 *
 * Here we implement only some of the optional resources.
 *
 */

#include "object_device.h"

#include <internal.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* Prototypes */
static bool id_within_array(const uint8_t id, const uint8_t ids[], const size_t numIds);

static uint8_t copy_string(const char* str, lwm2m_data_t * data){
  if(str != NULL){
    lwm2m_data_encode_string(str, data);
    return COAP_205_CONTENT;
  }
  return COAP_404_NOT_FOUND;
}

static uint8_t prv_get_value(lwm2m_data_t * dataP, lwm2m_device_instance_data_t * targetP)
{
    // a simple switch structure is used to respond at the specified resource asked
    switch (dataP->id)
    {
    case LWM2M_DEVICE_MANUFACTURER_ID:
      return copy_string(targetP->manufacturer, dataP);

    case LWM2M_DEVICE_MODEL_NUMBER_ID:
      return copy_string(targetP->model, dataP);

    case LWM2M_DEVICE_SERIAL_NUMBER_ID:
      return copy_string(targetP->serial, dataP);

    case LWM2M_DEVICE_FIRMWARE_VERSION_ID:
      return copy_string(targetP->firmware, dataP);

    case LWM2M_DEVICE_REBOOT_ID:
    case LWM2M_DEVICE_FACTORY_RESET_ID:
        return COAP_405_METHOD_NOT_ALLOWED;
      
    case LWM2M_DEVICE_AVL_POWER_SOURCES_ID:
      if(targetP->availablePowerSource != LWM2M_DEVICE_POWER_SOURCE_NONE){
        lwm2m_data_encode_int(targetP->availablePowerSource, dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID:
      lwm2m_data_encode_int(targetP->powerSourceVoltage, dataP);
      return COAP_205_CONTENT;

    case LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID:
      lwm2m_data_encode_int(targetP->powerSourceCurrent, dataP);
      return COAP_205_CONTENT;

    case LWM2M_DEVICE_BINDING_MODES_ID:
        switch(targetP->binding)
        {
          case BINDING_U:
            lwm2m_data_encode_string("U", dataP); 
            break;
          
          case BINDING_US:
            lwm2m_data_encode_string("US", dataP); 
            break;

          case BINDING_UQ:
            lwm2m_data_encode_string("UQ", dataP);
            break;

          case BINDING_UQS:
            lwm2m_data_encode_string("UQS", dataP);
            break;

          case BINDING_S:
            lwm2m_data_encode_string("S", dataP);
            break;

          case BINDING_SQ:
            // buffer[1] = 'Q';
            lwm2m_data_encode_string("SQ", dataP);
            break;

          case BINDING_UNKNOWN:
            break;
        }
        // lwm2m_data_encode_string(buffer, dataP);
        return COAP_205_CONTENT;

    case LWM2M_DEVICE_BATTERY_LEVEL_ID:
      if(targetP->batteryLevelCallback != NULL){
        lwm2m_data_encode_int(targetP->batteryLevelCallback(), dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_MEMORY_FREE_ID:
      if(targetP->freeMemorySpaceCallback != NULL){
        lwm2m_data_encode_int(targetP->freeMemorySpaceCallback(), dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_CURRENT_TIME_ID:
      if(targetP->currentTimeReadCallback != NULL){
        lwm2m_data_encode_int(targetP->currentTimeReadCallback(), dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_UTC_OFFSET_ID:
      if(targetP->utcOffset[0] != 0){
        lwm2m_data_encode_string(targetP->utcOffset, dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_TIMEZONE_ID:
      return copy_string(targetP->timezone, dataP);

    case LWM2M_DEVICE_DEVICE_TYPE_ID:
      return copy_string(targetP->deviceType, dataP);

    case LWM2M_DEVICE_HARDWARE_VERSION_ID:
      return copy_string(targetP->hardwareVersion, dataP);

    case LWM2M_DEVICE_SOFTWARE_VERSION_ID:
      return copy_string(targetP->softwareVersion, dataP);

    case LWM2M_DEVICE_BATTERY_STATUS_ID:
      if(targetP->batteryStatusCallback != NULL){
        lwm2m_data_encode_int(targetP->batteryStatusCallback(), dataP);
        return COAP_205_CONTENT;
      }
      return COAP_404_NOT_FOUND;

    case LWM2M_DEVICE_MEMORY_TOTAL_ID:
        lwm2m_data_encode_int(targetP->totalMemory, dataP);
        return COAP_205_CONTENT;

    default:
        return COAP_404_NOT_FOUND;
    }
}

static uint8_t available_resource_ids(const lwm2m_device_instance_data_t * data, uint8_t avlResourceIds[]){
  uint8_t numData = 0;
  if(data->manufacturer != NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_MANUFACTURER_ID;
    numData++;
  }
  if(data->model != NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_MODEL_NUMBER_ID;
    numData++;
  }
  if(data->serial != NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_SERIAL_NUMBER_ID;
    numData++;
  }
  if(data->firmware != NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_FIRMWARE_VERSION_ID;
    numData++;
  }
  if(data->availablePowerSource != LWM2M_DEVICE_POWER_SOURCE_NONE){
    avlResourceIds[numData] = LWM2M_DEVICE_AVL_POWER_SOURCES_ID;
    numData++;
  }
  if(data->powerSourceVoltage > 0){
    avlResourceIds[numData] = LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID;
    numData++;
  }
  if(data->powerSourceCurrent > 0){
    avlResourceIds[numData] = LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID;
    numData++;
  }
  if(data->binding!= BINDING_UNKNOWN){
    avlResourceIds[numData] = LWM2M_DEVICE_BINDING_MODES_ID;
    numData++;
  }
  if(data->batteryLevelCallback!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_BATTERY_LEVEL_ID;
    numData++;
  }
  if(data->totalMemory!= 0){
    avlResourceIds[numData] = LWM2M_DEVICE_MEMORY_TOTAL_ID;
    numData++;
  }
  if(data->freeMemorySpaceCallback!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_MEMORY_FREE_ID;
    numData++;
  }
  if(data->currentTimeReadCallback!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_CURRENT_TIME_ID;
    numData++;
  }
  if(data->utcOffset[0]!= 0){
    avlResourceIds[numData] = LWM2M_DEVICE_UTC_OFFSET_ID;
    numData++;
  }
  if(data->timezone!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_TIMEZONE_ID;
    numData++;
  }
  if(data->deviceType!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_DEVICE_TYPE_ID;
    numData++;
  }
  if(data->hardwareVersion!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_HARDWARE_VERSION_ID;
    numData++;
  }
  if(data->softwareVersion!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_SOFTWARE_VERSION_ID;
    numData++;
  }
  if(data->batteryStatusCallback!= NULL){
    avlResourceIds[numData] = LWM2M_DEVICE_BATTERY_STATUS_ID;
    numData++;
  }

  return numData;
}

uint8_t lwm2m_device_read(uint16_t instanceId,
                               int * numDataP,
                               lwm2m_data_t ** dataArrayP,
                               lwm2m_object_t * objectP)
{
    uint8_t result;
    uint8_t avlResourceIds[23] = {}; //maximum available number of ressources is 23 according to the standard

    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP) return COAP_404_NOT_FOUND;

    // is the server asking for the full object ?
    if (*numDataP == 0)
    {
      lwm2m_device_instance_data_t * data = targetP->data;
      *numDataP = available_resource_ids(data, avlResourceIds);
      *dataArrayP = lwm2m_data_new(*numDataP);
      for(int i=0; i<*numDataP;i++){
        (*dataArrayP)[i].id = avlResourceIds[i];
      }
    }

    int i = 0;
    do
    {
        result = prv_get_value((*dataArrayP) + i, targetP->data);
        i++;
    } while (i < *numDataP && result == COAP_205_CONTENT);

    return result;
}

static int prv_check_time_offset(char * buffer,
                                 int length)
{
    int min_index;

    if (length != 3 && length != 5 && length != 6) return 0;
    if (buffer[0] != '-' && buffer[0] != '+') return 0;
    switch (buffer[1])
    {
    case '0':
        if (buffer[2] < '0' || buffer[2] > '9') return 0;
        break;
    case '1':
        if (buffer[2] < '0' || buffer[2] > '2') return 0;
        break;
    default:
        return 0;
    }
    switch (length)
    {
    case 3:
        return 1;
    case 5:
        min_index = 3;
        break;
    case 6:
        if (buffer[3] != ':') return 0;
        min_index = 4;
        break;
    default:
        // never happen
        return 0;
    }
    if (buffer[min_index] < '0' || buffer[min_index] > '5') return 0;
    if (buffer[min_index+1] < '0' || buffer[min_index+1] > '9') return 0;

    return 1;
}

uint8_t lwm2m_device_write(uint16_t instanceId, int numData, lwm2m_data_t * dataArray, lwm2m_object_t * objectP){
  lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
  if (NULL == targetP) return COAP_404_NOT_FOUND;
  lwm2m_device_instance_data_t * data = targetP->data;
  uint8_t ret = 0;

  for(int i=0; i < numData; i++){
    switch(dataArray[i].id){
      case LWM2M_DEVICE_CURRENT_TIME_ID:
      {
        int64_t val;
        if(1 == lwm2m_data_decode_int(&dataArray[i], &val)){
          if(data->currentTimeWriteCallback != NULL){
            data->currentTimeWriteCallback(val);
            ret = COAP_204_CHANGED;
          }else{
            ret = COAP_404_NOT_FOUND;
          }
        }else{
          ret = COAP_400_BAD_REQUEST;
        }
        break;
      }

      case LWM2M_DEVICE_UTC_OFFSET_ID:
      {
        if(dataArray[i].type == LWM2M_TYPE_STRING){
          if(prv_check_time_offset(dataArray[i].value.asBuffer.buffer, dataArray[i].value.asBuffer.length) == 1){
            strncpy(data->utcOffset, dataArray[i].value.asBuffer.buffer, dataArray[i].value.asBuffer.length);
            ret = COAP_204_CHANGED;
          }else{
            ret = COAP_400_BAD_REQUEST;
          }
        }else{
          ret = COAP_400_BAD_REQUEST;
        }
        break;
      }

      case LWM2M_DEVICE_MANUFACTURER_ID:
      case LWM2M_DEVICE_MODEL_NUMBER_ID:
      case LWM2M_DEVICE_SERIAL_NUMBER_ID:
      case LWM2M_DEVICE_FIRMWARE_VERSION_ID:
      case LWM2M_DEVICE_REBOOT_ID:
      case LWM2M_DEVICE_FACTORY_RESET_ID:
      case LWM2M_DEVICE_AVL_POWER_SOURCES_ID:
      case LWM2M_DEVICE_POWER_SOURCE_VOLTAGE_ID:
      case LWM2M_DEVICE_POWER_SOURCE_CURRENT_ID:
      case LWM2M_DEVICE_BATTERY_LEVEL_ID:
      case LWM2M_DEVICE_MEMORY_FREE_ID:
      case LWM2M_DEVICE_MEMORY_TOTAL_ID:
      case LWM2M_DEVICE_BINDING_MODES_ID:
      case LWM2M_DEVICE_DEVICE_TYPE_ID:
      case LWM2M_DEVICE_HARDWARE_VERSION_ID:
      case LWM2M_DEVICE_SOFTWARE_VERSION_ID:
      {
        /* this id's are not writable according to the standard */
        ret = COAP_405_METHOD_NOT_ALLOWED;
        break;
      }

      default:
      {
        ret = COAP_404_NOT_FOUND;
        break;
      }
    }
  }

  return ret;
}

uint8_t lwm2m_device_discover(uint16_t instanceId,
                                   int * numDataP,
                                   lwm2m_data_t ** dataArrayP,
                                   lwm2m_object_t * objectP)
{
    uint8_t result;
    int i;

    // this is a single instance object
    if (instanceId != 0)
    {
      return COAP_404_NOT_FOUND;
    }

    result = COAP_205_CONTENT;

    lwm2m_instance_t * inst = (lwm2m_instance_t*) LWM2M_LIST_FIND(objectP->instanceList, instanceId);
    lwm2m_device_instance_data_t * data = inst->data;
    uint8_t avlResourceIds[23] = {};

    // is the server asking for the full object ?
    if (*numDataP == 0)
    {
      *numDataP = available_resource_ids(data, avlResourceIds);
      *dataArrayP = lwm2m_data_new(*numDataP);
      if (*dataArrayP == NULL) return COAP_500_INTERNAL_SERVER_ERROR;
      for (i = 0 ; i < *numDataP ; i++)
      {
        (*dataArrayP)[i].id = avlResourceIds[i];
      }
    }
    else
    {
      uint8_t numIds = available_resource_ids(data, avlResourceIds);
      result = COAP_205_CONTENT;
      for (i = 0; i < *numDataP && result == COAP_205_CONTENT; i++)
      {
        if(! id_within_array((*dataArrayP)[i].id, avlResourceIds, numIds)){
          result = COAP_404_NOT_FOUND;
          break;
        }
      }
    }

    return result;
}

static bool id_within_array(const uint8_t id, const uint8_t ids[], const size_t numIds){
  for(int i = 0; i< numIds; i++){
    if(ids[i] == id) return true;
  }

  return false;
}

uint8_t lwm2m_device_execute(uint16_t instanceId,
                                  uint16_t resourceId,
                                  uint8_t * buffer,
                                  int length,
                                  lwm2m_object_t * objectP)
{
    // this is a single instance object
    if (instanceId != 0) return COAP_404_NOT_FOUND;
    if (length != 0) return COAP_400_BAD_REQUEST;
    if(objectP == NULL) return COAP_400_BAD_REQUEST;
    
    lwm2m_instance_t * targetP = (lwm2m_instance_t *)lwm2m_list_find(objectP->instanceList, instanceId);
    if (NULL == targetP) return COAP_404_NOT_FOUND;
    lwm2m_device_instance_data_t * data = targetP->data;

    uint8_t ret;
    switch(resourceId){
      case LWM2M_DEVICE_REBOOT_ID:
        if(data->rebootCallback != NULL){
          data->rebootCallback();
          ret = COAP_204_CHANGED;
        }else{
          ret = COAP_404_NOT_FOUND;
        }
        break;

      case LWM2M_DEVICE_FACTORY_RESET_ID:
        if(data->factoryResetCallback != NULL){
          data->factoryResetCallback();
          ret = COAP_204_CHANGED;
        }else{
          ret = COAP_404_NOT_FOUND;
        }
        break;

      default:
        ret = COAP_405_METHOD_NOT_ALLOWED;
    }

    return ret;
}